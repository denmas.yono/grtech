 <div class="col-lg-3">
    @if(Models\Post::whereHas('post_status', function($q){
        $q->where('code', 'publish');
    })->whereHas('category', function($q){
        $q->where('status', 1);
    })->count() > 0)
        @foreach(Models\Category::whereNotIn('id', [4,5])->where('status', 1)->get() as $group)
        <div class="card" style="margin-top: 10px;">
            <div class="card-header">
                <div class="card-title">
                    {{$group->name}}
                </div>
            </div>
            <div class="card-body">
                <div class="tree">
                    <ul>
                        <li class="parent_li">
                            <?php 
                            $posts = Models\Post::select('year')->where('category_id', $group->id)->whereHas('post_status', function($q){
                                $q->where('code', 'publish');
                            })->groupBy('year')->orderBy('year','desc')->get();
                            ?>
                            @if($posts->count() > 0)
                                @foreach ($posts as $post)
                                    <span title="Expand this branch"><i class="fa fa-minus"></i>
                                        {{ $post->year }}
                                    </span> <i class="fa fa-book">
                                    </i>
                                    <ul>
                                        <?php 
                                        $postes = Models\Post::where('category_id', $group->id)->whereHas('post_status', function($q){
                                            $q->where('code', 'publish');
                                        })->where('year', $post->year)->orderBy('month')->orderBy('year')->get();
                                        ?>
                                        @foreach ($postes as $poste)
                                        <li class="parent_li">
                                            <span data-model="" data-id="" data-target="" title="Expand this branch"><i
                                                    class="fa fa-plus"></i>{{ $poste->title }}</span>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <br> 
                                @endforeach
                            
                            @else 
                            {{$group->name}} Tidak Ada
                            @endif
                        </li>
                    </ul>
                </div>

            </div>
            <div class="card-footer">&nbsp;</div>
        </div> 
        @endforeach
     @endif
 </div>
