<?php

namespace Lib;

use CRYPT_SHA512;
class QRIS
{
   protected $url_dev = "https=>//dev.yokke.bankmandiri.co.id=>7778/";
   protected $url_test = "https//tst.yokke.bankmandiri.co.id=>7778/";
   protected $url_prod = "https//api.yokke.bankmandiri.co.id=>7778/";
   public $invoice;
   public $client;
   protected $accessToken;
   protected $expireIn;
   protected $tokenType;
   /*
   1 R Stands for required in data field description.
   2 C Stands for conditional in data field description.
   3 O Stands for optional in data field description.
   */

   public function __construct($invoice)
   {
      $this->invoice = $invoice;
      $this->client = $invoice->client;
   }

   protected function getSecretKey()
   {
      return "QRPay2021";
   }

   protected function getClientId()
   {
      return "";
   }

   protected function getClientSecret()
   {
      return "";
   }
 
   protected function getAccessToken()
   {
      return "";
   }

   //stringToSign = HTTPMethod+":"+EndpointUrl+":"+AccessToken+":"+Lowercase(HexEncode(SHA256(RequestBody)))+": "+TimeStamp
   protected function getStringToSign()
   {   
      return "POST:".$this->generateQRUrl().":".$this->getAccessToken().":".strtolower(HexEncode(sha_256($this->getRequestBody()))).": ".$this->getTimeStamp();
   }

   //signToString = ClientID + “|” + X-TIMESTAMP

   protected function getSignToString()
   {  
      return $this->getClientId()."|".$this->getTimeStamp();
   }

   /*
   MAC is used to verify the contents of messages to avoid changing (tempering) messages. 
   SHA512 = (secretKey,stringToSign)
   */
   protected function getMac(){
      return sha_512($this->getSecretKey(), $this->getStringToSign());
   }
   /**
    *  HMAC(SHA512):c75deb76ee85394d04c2cc6b7a8dd18f10625dd70343cf553a4eb2ddd1a4247d8c220d824907dac6c378ebe89def9804685acc14dbbe475f3346eaa836297164
    */
   protected function getComputdMac(){
      return hash_hmac(sha_512()).":".$this->getMac();
   }

   /*
   MAC is used to verify the contents of messages to avoid changing (tempering) messages.
   SHA512 = (secretKey,stringToSign)
   stringToSign = HTTPMethod+"=>"+EndpointUrl+"=>"+AccessToken+"=>"+Lowercase(HexEncode(SHA256(RequestBody)))+"=> "+TimeStamp
   */
   protected function generateQRUrl()
   {
      return ((isDev()) ? $this->url_dev : $this->url_test) . "qrispayment/generateQR";
   }
   protected function getTimeStamp()
   {
      date_default_timezone_set('Asia/Jakarta');
      return date('c', time() + 12 * 3600); 
   }
   protected function tokenUrl()
   {
   }


   protected function getSignature(){
      return "";
   }

   protected function getMTIKey(){
      return "";
   }

   protected function getQRTokenUrl()
   {
      return ((isDev()) ? $this->url_dev : $this->url_test) . "qrispayment/getToken";
   }
 
   protected function getTokenHeader(){
      return [ 
            "Accept"=>"application/json",
            "X-TIMESTAMP"=> $this->getTimeStamp(),
            "X-MTI-Key"=> $this->getMTIKey(),
            "X-SIGNATURE"=> $this->getSignature()            
      ];
   }

   protected function getQRTokenResponse($response)
   {  
         $this->accessToken = $response->accessToken;
         $this->tokenType = $response->tokenType;
         $this->expiredIn = $response->expiredIn;
         return $this;
      
   }
    
   protected function generateQRRequestBody()
   {
      return [
         "transactionDate" => "20210419",
         "transactionTime" => "101223",
         "merchantID" => "00007100010926",
         "terminalID" => "72001126",
         "channelType" => "01",
         "merchantTransactionID" => 230218123798017,
         "billReferenceNumber" => "23021812379800000001",
         "transactionAmount" => [
            "value" => "100000.00",
            "currency" => "IDR"
         ],
         "additionalAmount" => [
            "value" => "100000.00",
            "currency" => "IDR"
         ]
      ];
   }

   protected function getQRResponse($response)
   {
      return [ 
            "transactionDate"=> $response->transactionDate,
            "transactionTime"=> $response->transactionTime,
            "merchantID"=> $response->merchantID,
            "terminalID"=> $response->terminalID,
            "responseCode"=> $response->responseCode,
            "responseMessage"=>$response->responseMessage,
            "merchantTransactionID"=> $response->merchantTransactionID,
            "billRefferenceNumber"=> $response->billRefferenceNumber,
            "channelType"=> $response->channelType,
            "referenceNumber"=> $response->referenceNumber,
            "qrContent"=> $response->qrContent
           
      ];
   }

   protected function responseCode($error){
      $codes = [
         200 => [
            "2004900" => "APPROVAL"
         ],
         400 => [
            "4004901" => "CALL YOUR BANK",
            "4004902" => "INVALID CHECKSUM",
            "4004903" => "INVALID MERCHANT",
            "4004905" => "DO NOT HONOR ",
            "4004906" => "UNDEFINED ERROR CODE",
            "4004911" => "MESSAGE FORMAT ERROR",
            "4004912" => "INVALID TRANSACTION",
            "4004913" => "INVALID AMOUNT",
            "4004931" => "MESSAGE EDIT ERROR",
            "4004951" => "INSUFF. FUNDS OVER LIMIT",
            "4004961" => "EXCEED LIMIT",
            "4004988" => "DUPLICATE CANCELATION",
            "4004990" => "TIME OUT",
            "40049A5" => "TERMINAL INVALID",
            "40049A7" => "FEATURE NOT ALLOWED",
            "40049AG" => "ORIGINAL TRANSACTION NOT FOUND",
            "40049A0" => "QR PAYMENT FAIL",
            "40049AH" => "ORIGINAL TRANSACTION ALREADY CANCELED",
            "40049M3" => "MEMBER NOT ACTIVE",
            "40049QD" => "INVALID PARAMETER",
         ]
      ];

      return $codes[$error->code][$error->reason_code];
   }
}
