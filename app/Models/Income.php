<?php

namespace Models;
 
use Illuminate\Database\Eloquent\Model;

class Income extends BasedModel
{ 
    public function income_category(){
        return $this->belongsTo(IncomeCategory::class);
    }

    public function income_type(){
        return $this->belongsTo(IncomeType::class);
    }

    public function income_detail(){
        return $this->hasMany(IncomeDetail::class);
    }

    public function template(){
        return $this->belongsTo(Template::class);
    }
}
