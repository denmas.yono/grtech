<?php

namespace Models;
 
class IncomeType extends BasedModel
{ 
    public function income(){
        return $this->hasMany(Income::class);
    }
}
