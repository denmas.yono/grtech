<?php

namespace Models;

use Illuminate\Database\Eloquent\Factories\HasFactory; 

class MenuType extends BasedModel
{
    use HasFactory;
}
