<?php

namespace Models;
 
class Structure extends BasedModel
{ 
    protected $appends = ['period'];
    public function staff(){
        return $this->hasMany(Staff::class);
    }

    public function getPeriodAttribute()
    {
        return "{$this->start_date} - {$this->end_date}";
    }

    public function tree(){
        return $this->hasOne(Tree::class);
    }
}
