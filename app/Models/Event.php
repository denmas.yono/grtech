<?php

namespace Models;
 
use Illuminate\Database\Eloquent\Model;

class Event extends BasedModel
{ 
    public function event_type(){
        return $this->belongsTo(EventType::class);
    }

    public function getDateStartAttribute($value){
        return $attribute['date_start'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function getDateEndAttribute($value){
        return $attribute['date_end'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
    }

    public function template(){
        return $this->belongsTo(Template::class);
    }
}
