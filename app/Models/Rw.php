<?php

namespace Models; 

class Rw extends BasedModel
{ 
    protected $table = 'rw'; 

    public function subdistrict(){
        return $this->belongsTo(Subdistrict::class);
    } 

    public function rt(){
        return $this->hasMany(Rt::class);
    } 

    public function child(){
        return Rt::class;
    }

    public function getTitle(){
        return "Daftar RT Untuk RW ".$this->name;
    }

    public function getTemplate(){
        return "Rt::list";
    }
}
