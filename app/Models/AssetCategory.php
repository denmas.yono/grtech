<?php

namespace Models;
 
use Illuminate\Database\Eloquent\Model;

class AssetCategory extends BasedModel
{
    public function asset(){
        return $this->hasMany(Asset::class);
    } 
}
