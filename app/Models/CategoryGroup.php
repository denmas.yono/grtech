<?php 
namespace Models; 

class CategoryGroup extends BasedModel
{ 
    public function category(){
        return $this->hasMany(Category::class);
    }
}