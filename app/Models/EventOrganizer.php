<?php

namespace Models;
 
class EventOrganizer extends basedModel
{ 

    public function event(){
        return $this->belongsTo(Event::class);
    }

    public function event_section(){
        return $this->belongsTo(EventSection::class);
    }

    public function people(){
        return $this->belongsTo(People::class);
    }
}
