<?php

namespace Models;
 
class Mushola extends BasedModel
{ 
    protected $table = 'mushola';
    public function address()
    {
        return $this->hasOne(Address::class,"id","object_id");
    }
}
