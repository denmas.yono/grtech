<?php

namespace Models;
  
class EventType extends BasedModel
{  
    public function event(){
        return $this->hasMany(Event::class);
    }
}
