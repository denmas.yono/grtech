<?php 
namespace Models;

use App\Models\User;

class Post extends BasedModel
{   
    public $appends = ['slug','date']; 
    
    public function getSlugAttribute(){
        return str_replace(" ","-",strtolower($this->title));
    } 

    public function getDateAttribute(){
        return date("Ymd", strtotime($this->publish_date));
    } 

    public function tag(){
        return $this->belongsToMany(Tag::class);
    } 

    public function comment(){
        return $this->hasMany(Comment::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    } 

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function template(){
        return $this->belongsTo(Template::class);
    }

    public function page(){
        return $this->hasOne(Page::class);
    }

    public function post_status(){
        return $this->belongsTo(PostStatus::class);
    } 

    public function child(){
        return Post::class;
    }
}