<?php

namespace Models;
 
class EventDocumentation extends BasedModel
{ 
    public function event(){
        return $this->belongsTo(Event::class);
    }
}
