<?php

namespace Models; 
class Rt extends BasedModel
{ 
    protected $table = 'rt'; 

    public function rw(){
        return $this->belongsTo(Rw::class);
    } 

    public function people(){
        return $this->hasMany(People::class);
    }   

    public function child(){
        return null;
    }

    public function getTemplate(){
        return "Rt::list";
    }

    public function getTitle(){
        return "Daftar Warga Untuk RT ".$this->name;
    }

}
