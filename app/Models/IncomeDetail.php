<?php

namespace Models; 

class IncomeDetail extends BasedModel
{ 
    public function income(){
        return $this->belongsTo(Income::class);
    }
}
