<?php 
namespace Models; 

class Schedule extends BasedModel
{ 
    public function teacher(){
        return $this->belongsTo(Teacher::class);
    }

    public function subject(){
        return $this->belongsTo(Subject::class);
    }

    public function class_level(){
        return $this->belongsTo(ClassLevel::class);
    }
}