<?php

namespace Models;
 
use Illuminate\Database\Eloquent\Model;

class Expenditure extends BasedModel
{ 
    public function expenditure_category(){
        return $this->belongsTo(ExpenditureCategory::class);
    } 

    public function child(){
        return Expenditure::class;
    }

    public function template(){
        return $this->belongsTo(Template::class);
    }
}
