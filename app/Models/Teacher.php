<?php

namespace Models;
 
class Teacher extends BasedModel
{ 
    protected $appends = ['full_name'];

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function mushola()
    {
        return $this->belongsTo(Mushola::class);
    }

    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    }

    public function subjects(){
        return $this->belongsToMany(Subject::class);
    }
}
