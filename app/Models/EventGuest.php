<?php

namespace Models;
 
class EventGuest extends BasedModel
{ 
    public function event(){
        return $this->belongsTo(Event::class);
    }

    public function people(){
        return $this->belongsTo(People::class);
    }
}
