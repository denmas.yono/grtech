<?php

namespace Models;
 
class Student extends BasedModel
{ 
    protected $appends = ['full_name'];

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function mushola()
    {
        return $this->belongsTo(Mushola::class);
    }

    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    }
}
