<?php

namespace Models; 

class EventCost extends BasedModel
{ 
    public function event(){
        return $this->belongsTo(Event::class);
    }
}
