<?php

namespace Models; 
class Gallery extends BasedModel
{ 
    public function post_status(){
        return $this->belongsTo(PostStatus::class);
    } 

    public function album(){
        return $this->belongsTo(Album::class);
    } 

    public function template(){
        return $this->belongsTo(Template::class);
    }
}
