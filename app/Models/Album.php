<?php

namespace Models; 
class Album extends BasedModel
{ 
    public function post_status(){
        return $this->belongsTo(PostStatus::class);
    } 

    public function gallery(){
        return $this->hasMany(Gallery::class);
    } 

    public function child(){
        return Album::class;
    }

    public function template(){
        return $this->belongsTo(Template::class);
    }
}
