<?php

namespace Models;

use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable; 

class People extends BasedModel
{  
    use Notifiable; 
    protected $appends = ['full_name'];
    protected $table = 'people';
    
    public static $rules = array( 
        'first_name' => 'required', 
        'last_name' => 'required', 
    );

    public function validate($data)
    {
        $v = Validator::make($data, People::$rules);
        return $v;
    }

    public function mushola()
    {
        return $this->belongsTo(Mushola::class);
    }

    public function mosque()
    {
        return $this->belongsTo(Mosque::class);
    } 

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function address()
    {
        return $this->hasMany(Address::class);
    }

    public function staff()
    {
        return $this->hasMany(Staff::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
