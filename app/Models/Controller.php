<?php

namespace Models;
 
use Illuminate\Database\Eloquent\Model;

class Controller extends BasedModel
{ 
    public function roles() {
        return $this->belongsToMany(Role::class);
    }

    public function group_menu() {
        return $this->belongsTo(GroupMenu::class);
    }

    public function attribute() {
        return $this->hasMany(Attribute::class);
    }

    public function childrens() {
        return $this->belongsTo(Controller::class, 'id', 'parent_id');
    }

}
