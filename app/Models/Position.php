<?php

namespace Models;
 
use Illuminate\Database\Eloquent\Model;

class Position extends BasedModel
{ 
    public function people()
    {
        return $this->hasMany(People::class);
    }
}
