<?php 
namespace Models; 

class Comment extends BasedModel
{ 
    public function post(){
        return $this->belongsTo(Post::class);
    }
}