<?php 
namespace Models; 

class Meta extends BasedModel
{ 
    public function post(){
        return $this->belongsTo(Post::class);
    }
}