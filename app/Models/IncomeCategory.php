<?php

namespace Models;
  
class IncomeCategory extends BasedModel
{  
    public function isBankSampah(){
        return $this->code == "bs";
    }

    public function isKotakAmal(){
        return $this->code == "ka";
    }

    public function isSumbanganWarga(){
        return $this->code == "sw";
    }
}
