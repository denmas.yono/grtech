<?php

namespace Models;
 

class Address extends BasedModel
{ 
    public function rt(){
        return $this->belongsTo(Rt::class);
    }

    public function rw(){
        return $this->belongsTo(Rw::class);
    }

    public function subdistrict(){
        return $this->belongsTo(Subdistrict::class);
    }

    public function district(){
        return $this->belongsTo(District::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function region(){
        return $this->belongsTo(Region::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }

    public function mosque(){
        return $this->hasOne(Mosque::class);
    }

    public function mushola(){
        return $this->hasOne(Mushola::class);
    }
 
    public function people()
    {
        return $this->belongsTo(People::class);
    }
}
