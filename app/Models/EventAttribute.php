<?php

namespace Models; 

class EventAttribute extends BasedModel
{ 
    public function event(){
        return $this->belongsTo(Event::class);
    }
}
