<?php

namespace Models;
  
use Illuminate\Support\Facades\Validator;
use Illuminate\Notifications\Notifiable;

class Mosque extends BasedModel
{
    use Notifiable; 
     
    public static $rules = array( 
        'name' => 'required'
    );

    public function validate($data)
    {
        $v = Validator::make($data, Mosque::$rules);
        return $v;
    }

    public function people()
    {
        return $this->hasMany(People::class);
    }

    public function student()
    {
        return $this->hasMany(Student::class);
    }

    public function address()
    {
        return $this->hasOne(Address::class,"id","object_id");
    }
}
