<?php

namespace Models; 

class Subject extends BasedModel
{ 

    public function teachers(){
        return $this->belongsToMany(Teacher::class);
    }
}
