<?php

namespace Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BasedModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['id']; 
    protected $dates = ['deleted_at'];

    protected static function boot()
    { 
        parent::boot(); 
        static::saving(function($model) { 
            if($model instanceof People){
                $model->name = $model->first_name." ".$model->last_name; 
            } 

            if($model instanceof Post){
                $model->month = date('n', strtotime($model->created_at)); 
                $model->year = date('Y', strtotime($model->created_at)); 
            }
        });

        static::updating(function($model) { 
            if($model instanceof People){
                $model->name = $model->first_name." ".$model->last_name; 
            } 

            if($model instanceof Post){
                $model->month = date('n', strtotime($model->created_at)); 
                $model->year = date('Y', strtotime($model->created_at)); 
            } 
        });

        static::deleting(function($model) { 
             
        });
 
    } 

}
