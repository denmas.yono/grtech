<?php

namespace Models;
 
use Illuminate\Database\Eloquent\Model;

class ExpenditureCategory extends BasedModel
{ 
    public function expenditure(){
        return $this->hasMany(Expenditure::class);
    } 
}
