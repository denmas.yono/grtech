<?php 
namespace Models; 

class Category extends BasedModel
{ 
    public function post(){
        return $this->hasMany(Post::class);
    }

    public function category_group(){
        return $this->belongsTo(CategoryGroup::class);
    }
}