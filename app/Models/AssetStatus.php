<?php

namespace Models;
 
use Illuminate\Database\Eloquent\Model;

class AssetStatus extends BasedModel
{ 
    protected $table = 'asset_status';
}
