<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required',
            'category_group_id'=> 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'=> 'Nama Kategori Harus diisi',
            'category_group_id.required'=> 'Nama Grup Kategori Harus diisi'
        ];
    }
}
