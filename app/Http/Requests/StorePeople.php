<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePeople extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:4',
            'last_name' => 'required', 
            'email'=> 'required|unique:People'
        ];
    }

    public function messages()
    {   
        return [
            'first_name.required'=> 'Nama Depan harus diisi',
            'last_name.required'=> 'Nama Belakang harus diisi',
            'email.required'=> 'Email harus diisi',
            'email.unique'=> 'Email sudah ada di database kami',
            'first_name.min'=> 'Nama Depan Minimal 4 Karakter',
        ];
    }
}
