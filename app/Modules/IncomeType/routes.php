<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'income_type', 'namespace' => 'App\Modules\IncomeType\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'income_type.index', 'uses' => 'IncomeTypeController@index']);
    Route::get('/getListAjax', ['as' => 'income_type.getListAjax', 'uses' => 'IncomeTypeController@getListAjax']);
    Route::get('/create', ['as' => 'income_type.create', 'uses' => 'IncomeTypeController@create']);
    Route::get('/show/{id}', ['as' => 'income_type.show', 'uses' => 'IncomeTypeController@show']);    
    Route::get('/preview/{id}', ['as' => 'income_type.preview', 'uses' => 'IncomeTypeController@preview']);    
    Route::post('/store', ['as' => 'income_type.store', 'uses' => 'IncomeTypeController@store']);
    Route::get('/edit/{id}', ['as' => 'income_type.edit', 'uses' => 'IncomeTypeController@edit']);
    Route::put('/update/{id}', ['as' => 'income_type.update', 'uses' => 'IncomeTypeController@update']);
    Route::delete('/delete/{id}', ['as' => 'income_type.destroy', 'uses' => 'IncomeTypeController@destroy']);
});