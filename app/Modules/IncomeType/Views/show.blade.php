@extends('layouts.app')

@section('content')
<br>
  <div class="card card-widget widget-income_type col-md-4" style="margin: auto;padding-top: 7.5px;">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-income_type-header bg-info">
          <h3 class="widget-income_type-incomename">{{ $income_type->name }}</h3>
          <h5 class="widget-income_type-desc">Founder &amp; CEO</h5>
      </div>
      <div class="widget-income_type-image">
          <img src="{{ $income_type->photo ? $income_type->photo : asset('assets/images/income_type.png') }}"
              class="profile-income_type-img img-fluid img-circle img-responsive image-logo" data-title="{{ $income_type->name }}"
              onerror="this.src='../../dist/img/income2-160x160.jpg'">
      </div>
      <div class="card-footer">
          <div class="row">
              <div class="col-sm-12 border">
                  <div class="description-block">
                      <h5 class="description-header">3,200</h5>
                      <span class="description-text">SALES</span>
                  </div>
                  <!-- /.description-block -->
              </div>
              <!-- /.col -->
          </div>
          <!-- /.row -->
      </div>
  </div> 
@endsection
