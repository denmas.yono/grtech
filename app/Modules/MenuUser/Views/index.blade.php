@extends('adminlte::page')
@section('content')
    <div id="container">
        @include('MenuUser::list')
    </div>
    @push('js')
        <script>
            $(function() { 
                $("body").on('click', 'input[type=checkbox]', function(e) {
                    let data = {
                        field: $(this).attr('data-field'),
                        value: $(this).attr('data-value'),
                        id: $(this).attr('data-id'),
                    };
                    $.post('{{ url('menu_user/updateField') }}', data, function(res) {
                        $("button.reload").click();
                    });
                });
            }); 
        </script>
    @endpush
@stop
