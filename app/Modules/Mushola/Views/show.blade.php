@extends('adminlte::page')  
@section('content') 
<div id="container"> 
    {{ Form::model($mushola, ['method' => 'POST', 'route' => ['mushola.update', $mushola->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
    <div class="card-body">
        <div class="form-group">
            <label for="name">Nama</label>
            <input type="hidden" name="id" value="{{ $mushola->id }}">
            <input type="text" class="form-control" id="name" name="name" value="{{ $mushola->name }}"
                placeholder="Masukan Nama">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ $mushola->email }}"
                placeholder="Email">
        </div>
        <div class="form-group">
            <label for="preview"></label>
            <div class="post-review">
                <img src="{{ $mushola->logo ? asset('logo/' . $mushola->logo) : null }}" class="img-responsive"
                    onerror="this.src='{{ asset('assets/images/mushola.png') }}'" width="100px">
            </div>
        </div>
        <div class="form-group">
            <label for="logo">Logo</label>
            <input type="file" class="form-control post-input" id="logo" name="logo" value="{{ $mushola->logo }}"
                placeholder="Upload Logo">
        </div>
        <div class="form-group">
            <label for="website">Website</label>
            <input type="text" class="form-control" id="website" name="website" value="{{ $mushola->website }}"
                placeholder="Masukan website">
        </div>
        <div class="form-group">
            <label for="country_id">Negara</label> 
            {{ Form::select('country_id', \Models\Country::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker', 'placeholder'=> 'Pilih Negara']) }}
        </div>
        <div class="form-group">
            <label for="region_id">Provinsi</label> 
            {{ Form::select('region_id', \Models\Region::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker', 'placeholder'=> 'Pilih Propinsi']) }}
        </div>
        <div class="form-group">
            <label for="city_id">Kabupaten</label> 
            {{ Form::select('city_id', \Models\City::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker', 'placeholder'=> 'Pilih Kabupaten/Kota']) }}
        </div>
        <div class="form-group">
            <label for="district_id">Kecamatan</label> 
            {{ Form::select('district_id', \Models\District::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker', 'placeholder'=> 'Pilih Kecamatan']) }}
        </div>
        <div class="form-group">
            <label for="subdistrict_id">Kelurahan</label> 
            {{ Form::select('subdistrict_id', \Models\Subdistrict::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker', 'placeholder'=> 'Pilih Kelurahan']) }}
        </div>
        <div class="form-group">
            <label for="rw_id">RW</label> 
            {{ Form::select('rw_id', \Models\Rw::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker', 'placeholder'=> 'Pilih RW']) }}
        </div>
        <div class="form-group">
            <label for="rt_id">RT</label> 
            {{ Form::select('rt_id', \Models\Rt::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker', 'placeholder'=> 'Pilih RT']) }}
        </div>
        <div class="form-group">
            <label for="address_id">Alamat</label> 
            {{ Form::textarea('address_id', null, ['class' => 'form-control', 'placeholder'=> 'Alamat', 'rows'=> '3']) }}
        </div>
    </div>
    {{ Form::close() }}
    @push('js')
    <script src="{{ asset('js/core.js') }}"></script>        
    @endpush
 </div> 
@stop
  
