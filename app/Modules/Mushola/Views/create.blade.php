{{ Form::open(['method' => 'POST','route' => ['mushola.store'],'class' => 'form-horizontal','enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Nama</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nama']) }}
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) }}
    </div>
    <div class="form-group">
        <label for="preview"></label>
        <div class="post-review">
            <img src="" class="img-responsive" onerror="this.src='{{ asset('assets/images/mushola.png') }}'"
                width="100px">
        </div>
    </div>
    <div class="form-group">
        <label for="phone">Logo</label>
        <input type="file" class="form-control post-input" id="logo" name="logo" value="{{ old('logo') }}"
            placeholder="Upload Logo">
    </div>
    <div class="form-group">
        <label for="phone">Photo</label>
        <input type="file" class="form-control post-input" id="photo" name="photo" value="{{ old('logo') }}"
            placeholder="Upload Photo">
    </div>
    <div class="form-group">
        <label for="website">Website</label>
        <input type="text" class="form-control" id="website" name="website" value=""
            placeholder="Masukan website">
    </div>
    <div class="form-group">
        <label for="phone">No Telephone</label>
        {{ Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Nomor Telephone']) }}
    </div>
    <div class="form-group">
        <label for="cellphone">Handphone</label>
        {{ Form::text('cellphone', null, ['class' => 'form-control', 'placeholder' => 'Nomor Handphone']) }}
    </div>
    <div class="form-group">
        <label for="address">Alamat</label>
        {{ Form::textarea('address_detail', null, ['class' => 'form-control', 'placeholder' => 'Alamat', 'rows'=> 3]) }}
    </div>
</div>
{{ Form::close() }}
@push('js')
    <script src="{{ asset('js/core.js') }}"></script>
@endpush
