  <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Daftar Mushola</h3> 
                            {!!create(['url'=> route('mushola.create'), 'title'=> 'Tambah Mushola', 'style'=> "float: right;"]) !!}
                          </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="mushola" class="table table-bordered table-hover display standard" style="width: 100%" data-route="{{ route('mushola.getListAjax') }}">
                                <thead>
                                    <tr>
                                        <th data-name="id">No</th> 
                                        <th data-name="name">Nama</th>
                                        <th data-name="email">Email</th>
                                        <th data-name="logo">Logo</th>
                                        <th data-name="photo">Photo</th>
                                        <th data-name="phone">Telephone</th>
                                        <th data-name="cellphone">Handphone</th>
                                        <th data-name="address_detail">Alamat</th>
                                        <th data-name="action" nowrap="nowrap">Action</th> 
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content --> 