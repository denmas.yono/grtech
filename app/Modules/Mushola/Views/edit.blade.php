     {{ Form::model($mushola, ['method' => 'POST', 'route' => ['mushola.update', $mushola->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
     <div class="card-body">
         <div class="form-group">
             <label for="name">Nama</label>
             <input type="hidden" name="id" value="{{ $mushola->id }}">
             <input type="text" class="form-control" id="name" name="name" value="{{ $mushola->name }}"
                 placeholder="Masukan Nama">
         </div>
         <div class="form-group">
             <label for="email">Email</label>
             <input type="email" class="form-control" id="email" name="email" value="{{ $mushola->email }}"
                 placeholder="Email">
         </div>
         <div class="form-group">
             <label for="preview"></label>
             <div class="post-review">
                 <img src="{{ $mushola->logo ? asset('logo/' . $mushola->logo) : null }}" class="img-responsive"
                     onerror="this.src='{{ asset('assets/images/mushola.png') }}'" width="100px">
             </div>
         </div>
         <div class="form-group">
             <label for="logo">Logo</label>
             <input type="file" class="form-control post-input" id="logo" name="logo" value="{{ $mushola->logo }}"
                 placeholder="Upload Logo">
         </div>
         <div class="form-group">
             <label for="website">Website</label>
             <input type="text" class="form-control" id="website" name="website" value="{{ $mushola->website }}"
                 placeholder="Masukan website">
         </div>
         <div class="form-group">
            <label for="phone">No Telephone</label>
            {{Form::text('phone', null, ['class'=> 'form-control', 'placeholder'=> 'Nomor Telephone'])}}
        </div>
         <div class="form-group">
             <label for="cellphone">Handphone</label>
             {{Form::text('cellphone', null, ['class'=> 'form-control', 'placeholder'=> 'Nomor Handphone'])}}
         </div>
         <div class="form-group">
            <label for="address">Alamat</label>
            {{ Form::textarea('address_detail', $mushola->address_detail, ['class' => 'form-control', 'placeholder' => 'Alamat', 'rows'=> 3]) }}
        </div>
     </div>
     {{ Form::close() }}
     <script src="{{ asset('js/core.js') }}"></script>
