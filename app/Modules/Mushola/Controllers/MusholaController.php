<?php

namespace App\Modules\Mushola\Controllers;

use Models\Mushola as mushola;
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 
use Lib\File;  
use App\Http\Requests\StoreMushola;
       
class MusholaController extends MainController
{
    public function __construct() {  
        parent::__construct(new mushola(), 'Mushola');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        
        return view('Mushola::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $mushola = $this->_model::with('address')->select('*');
            return datatables()->of($mushola)
                    ->addIndexColumn() 
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('mushola.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('mushola.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('mushola.destroy',$row->id), 'preview'=> route('mushola.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    }) 
                    ->addColumn('logo', function($row){     
                        $logo = '<img src="'.($row->logo?asset('logo/'.$row->logo):(asset('assets/images/mushola.png'))).'" class="img-responsive image-logo" data-title="'.($row->name).'" width="100px">';
                         return $logo;
                    })
                    ->rawColumns(['action','logo'])
                    ->make(true);
        }
    }
 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Mushola::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMushola $request)
    {    
        try {
              
            $mushola = $this->_model::create($this->_serialize($request)); 
            if (request()->file('logo')) {                
                $this->validate($request, [
                    'name' => 'required',
                    'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]); 
                
                if (request()->file('logo')->isValid()) {
                    $file = request()->file('logo');
                    // image upload in storage/app/public/logo   
                    $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/logo')));
                    if($mushola->logo && file_exists(storage_path('app/public/logo/'.$mushola->logo))){
                        unlink(storage_path('app/public/logo/'.$mushola->logo));
                    }
                    $mushola->logo = $info->getFilename(); 
                    $mushola->save();
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                }
            } 
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Tambah Data Error '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('mushola.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Mushola  $people
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mushola = $this->_model::find($id); 
        return view('Mushola::show', ['mushola'=> $mushola]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Mushola  $people
     * @return \Illuminate\Http\Response
     */
    public function preview($id)
    {
        $mushola = $this->_model::find($id); 
        return view('Mushola::preview', ['mushola'=> $mushola]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Mushola  $mushola
     * @return \Illuminate\Http\Response
     */
    public function edit($mushola_id)
    {
        $mushola = $this->_model::find($mushola_id);
        return view('Mushola::edit', ['mushola'=> $mushola]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Mushola  $mushola
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMushola $request, $mushola_id)
    {     
        try {
            $mushola = $this->_model::find($mushola_id);
            if($mushola){  
                $mushola->update($this->_serialize($request));
                 
                if (request()->file('logo')) {                
                    $this->validate($request, [
                        'name' => 'required',
                        'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]); 
                    if (request()->file('logo')->isValid()) {
                        $file = request()->file('logo');
                        // image upload in storage/app/public/logo   
                        $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/logo')));
                        if($mushola->logo && file_exists(storage_path('app/public/logo/'.$mushola->logo))){
                            unlink(storage_path('app/public/logo/'.$mushola->logo));
                        }
                        $mushola->logo = $info->getFilename(); 
                        $mushola->save();
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                    }
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Tambah Mushola error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Data was updated.', 'redirectTo'=> route('mushola.index')], 200); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Mushola  $mushola
     * @return \Illuminate\Http\Response
     */
    public function destroy($mushola_id)
    {   
        try { 
            $mushola = $this->_model::find($mushola_id); 
            if($mushola){
                $mushola->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('mushola.index')], 200);
    }
}
