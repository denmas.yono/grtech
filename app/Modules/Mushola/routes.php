<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'mushola', 'namespace' => 'App\Modules\Mushola\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'mushola.index', 'uses' => 'MusholaController@index']);
    Route::get('/getListAjax', ['as' => 'mushola.getListAjax', 'uses' => 'MusholaController@getListAjax']);
    Route::get('/create', ['as' => 'mushola.create', 'uses' => 'MusholaController@create']);
    Route::post('/store', ['as' => 'mushola.store', 'uses' => 'MusholaController@store']);
    Route::get('/edit/{id}', ['as' => 'mushola.edit', 'uses' => 'MusholaController@edit']);
    Route::get('/show/{id}', ['as' => 'mushola.show', 'uses' => 'MusholaController@show']);
    Route::get('/preview/{id}', ['as' => 'mushola.preview', 'uses' => 'MusholaController@preview']);
    Route::put('/update/{id}', ['as' => 'mushola.update', 'uses' => 'MusholaController@update']);
    Route::delete('/destroy/{id}', ['as' => 'mushola.destroy', 'uses' => 'MusholaController@destroy']);
});