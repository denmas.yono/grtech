@extends('adminlte::page')  
@section('content') 
    <div id="container">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Data Masjid Dan Mushola</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-one-mosque-tab" data-toggle="pill"
                                            href="#custom-tabs-one-mosque" role="tab" aria-controls="custom-tabs-one-mosque"
                                            aria-selected="true">Masjid</a>
                                    </li> 
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-mushola-tab" data-toggle="pill"
                                            href="#custom-tabs-one-mushola" role="tab" aria-controls="custom-tabs-one-mushola"
                                            aria-selected="true">Mushola</a>
                                    </li>  
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-one-tabContent"> 
                                    <div class="tab-pane fade show active" id="custom-tabs-one-mosque" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-mosque-tab"> 
                                        @include('Mosque::list')
                                    </div> 
                                    <div class="tab-pane fade show" id="custom-tabs-one-mushola" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-mushola-tab"> 
                                        @include('Mushola::list')
                                    </div>   
                                </div> 
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
    </div>
    
@endsection
