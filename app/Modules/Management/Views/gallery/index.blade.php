@extends('adminlte::page')  
@section('content') 
    <div id="container">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Manajemen Gallery</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-one-gallery-tab" data-toggle="pill"
                                            href="#custom-tabs-one-gallery" role="tab" aria-controls="custom-tabs-one-gallery"
                                            aria-selected="true">Gallery</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-album-tab" data-toggle="pill"
                                            href="#custom-tabs-one-album" role="tab" aria-controls="custom-tabs-one-album"
                                            aria-selected="true">Album</a>
                                    </li>
                                     
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-one-tabContent">
                                    <div class="tab-pane fade show active" id="custom-tabs-one-gallery" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-gallery-tab"> 
                                        @include('Gallery::list')
                                    </div> 
                                    <div class="tab-pane fade show" id="custom-tabs-one-album" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-album-tab"> 
                                        @include('Album::list')
                                    </div>
                                     
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
    </div>
    
@endsection
