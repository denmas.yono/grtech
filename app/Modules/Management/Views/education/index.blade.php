@extends('adminlte::page')  
@section('content') 
    <div id="container">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Kependidikan</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-one-subject-tab" data-toggle="pill"
                                            href="#custom-tabs-one-subject" role="tab" aria-controls="custom-tabs-one-subject"
                                            aria-selected="true">Materi Pendidikan</a>
                                    </li> 
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-teacher-tab" data-toggle="pill"
                                            href="#custom-tabs-one-teacher" role="tab" aria-controls="custom-tabs-one-teacher"
                                            aria-selected="true">Daftar Ustadz Dan Ustadzah</a>
                                    </li> 
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-santri-tab" data-toggle="pill"
                                            href="#custom-tabs-one-santri" role="tab" aria-controls="custom-tabs-one-santri"
                                            aria-selected="true">Daftar Santri</a>
                                    </li>  
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-schedule-tab" data-toggle="pill"
                                            href="#custom-tabs-one-schedule" role="tab" aria-controls="custom-tabs-one-schedule"
                                            aria-selected="true">Jadwal Ngaji</a>
                                    </li>  
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-class_level-tab" data-toggle="pill"
                                            href="#custom-tabs-one-class_level" role="tab" aria-controls="custom-tabs-one-class_level"
                                            aria-selected="true">Level Kelas</a>
                                    </li> 
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-one-tabContent"> 
                                    <div class="tab-pane fade show active" id="custom-tabs-one-subject" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-subject-tab"> 
                                        @include('Subject::list')
                                    </div> 
                                    <div class="tab-pane fade show" id="custom-tabs-one-teacher" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-teacher-tab"> 
                                        @include('Teacher::list')
                                    </div>  
                                    <div class="tab-pane fade show" id="custom-tabs-one-santri" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-santri-tab"> 
                                        @include('Student::list')
                                    </div>  
                                    <div class="tab-pane fade show" id="custom-tabs-one-schedule" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-schedule-tab"> 
                                        @include('Schedule::list')
                                    </div>  
                                    <div class="tab-pane fade show" id="custom-tabs-one-class_level" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-class_level-tab"> 
                                        @include('ClassLevel::list')
                                    </div> 
                                </div> 
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
    </div>
    
@endsection
