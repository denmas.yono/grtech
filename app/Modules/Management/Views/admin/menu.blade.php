@push('css')
    <style>
        .toggle-vis {
            background-color: #6c757d;
            padding: 5px;
            border-radius: 20px;
            color: white;
            cursor: pointer;
        }

    </style>
@endpush

<div id="container">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Daftar Menu</h3>  
                        </div>

                        <!-- /.card-header -->
                        <div class="card-body"> 
                            <table id="menu" class="table table-bordered table-hover display" style="width: 100%"
                                data-route="{{ route('menu.getListAjax') }}">
                                <thead>
                                    <tr>
                                        <th data-name="id">No</th>
                                        <th data-name="name">Nama</th> 
                                        <th data-name="status">Status</th>
                                        <th class="action" data-priority="2">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->


                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

</div>
@push('js')
    <script>
        $(function() {
            loadMenus();

            $("body").on('click', 'a.show_detail', function(e) {
                e.preventDefault();
                let tr = $(this).parents('tr');
                let id = $(tr).attr('data-id');
                let is_tr_exists = $("table#menu").find("tr.detail-" + id);
                if ($("table#menu").find("tr.detail-" + id).length > 0) {
                    $("table#menu tr.detail-" + id).remove();
                } else {
                    $('<tr role="row" class="detail-' + id + '"><th colspan="7"></th></tr>').insertAfter(
                        tr);
                    $.ajax({
                        url: $(this).attr("data-href"),
                        type: "GET",
                        success: function(e) {
                            $("table#menu tr.detail-" + id + " th").html(e);
                        },
                        error: function(xhr) {

                        }
                    });
                }
            }).on('click', 'button.reload', function(e) {
                $.get('{{ url('/admin/refresh') }}', function() {});
            });
        });

        function loadMenus() {
            var selected = [];
            var table = $("#menu").DataTable({
                dom: "<'row'<'col-sm-3'l><'col-sm-4'B><'col-sm-5'f>>rtip",
                responsive: true,
                scrollY: 400,
                responsive: true,
                processing: true,
                serverSide: true,
                select: true,
                "rowCallback": function(row, data) {
                    if ($.inArray(data.DT_RowId, selected) !== -1) {
                        $(row).addClass('selected');
                    }
                },
                "rowCallback": function(row, data) {
                    if ($.inArray(data.DT_RowId, selected) !== -1) {
                        $(row).addClass('selected');
                    }
                },
                "ajax": "{{ route('menu.getListAjax') }}",
                "columns": [
                    // data:column name from server ,name:alias
                    {
                        data: 'id',
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        },
                        searchable: false
                    },
                    {
                        data: function(row, type) {
                            if (row != null) {
                                return '<a href data-href="{{ url('menu/detail') }}/' + row.id +
                                    '" class="show_detail">' + (row.url ? row.url : "No Name") + '</a>';
                            } else {
                                return "--";
                            }
                        },
                        name: 'name'
                    },   
                    {
                        data: function(row, type) {
                            if (row.is_active != null) {
                                if (row.is_active) {
                                    return '<span class="alert alert-success btn-xs">On</span>';
                                } else {
                                    return '<span class="alert alert-danger btn-xs">Off</span>';
                                }

                            } else {
                                return '<span class="alert alert-danger btn-xs">Off</span>';
                            }
                        },
                        name: 'is_active'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        printable: false,
                        exportable: false
                    },
                ],
                "buttons": [{
                        extend: 'pdf',
                        customize: function(win) {
                            console.log(win)
                            // $(win.document.body)
                            //     .css( 'font-size', '10pt' )
                            //     .prepend(
                            //         '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                            //     );

                            // let table = $(win.document.body).find( 'table' ).addClass( 'compact' ).css( 'font-size', 'inherit' );
                            // $(table).find( 'thead th:last()' ).css( 'display', 'none' );
                            // $(table).find( 'tbody td:last()' ).css( 'display', 'none' );
                        }
                    },
                    {
                        extend: 'excel',
                    },
                    {
                        extend: 'reload',
                        customize: function(e, dt, node, config) {
                            console.log(dt);
                            dt.ajax.reload();
                        }
                    },
                    {
                        extend: 'print',
                        //autoPrint: false,
                        customize: function(win) {

                            $(win.document.body)
                                .css('font-size', '10pt')
                                .prepend(
                                    '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                                );

                            let table = $(win.document.body).find('table').addClass('compact').css(
                                'font-size', 'inherit');
                            $(table).find('thead th:last()').css('display', 'none');
                            $(table).find('tbody td:last()').css('display', 'none');
                        }
                    },

                ]
            });
            $('a.toggle-vis').on('click', function(e) {
                e.preventDefault(); 
                // Get the column API object
                var column = table.column($(this).attr('data-column')); 
                // Toggle the visibility
                column.visible(!column.visible());
            });
        }
    </script>
@endpush
