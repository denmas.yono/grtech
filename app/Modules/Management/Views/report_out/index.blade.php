@extends('adminlte::page')
@section('content')
    <div id="container">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Laporan Pengeluaran Masjid</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-one-allreport-tab" data-toggle="pill"
                                            href="#custom-tabs-one-allreport" role="tab"
                                            aria-controls="custom-tabs-one-allreport" aria-selected="true">Keseluruhan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-daily-tab" data-toggle="pill"
                                            href="#custom-tabs-one-daily" role="tab"
                                            aria-controls="custom-tabs-one-daily" aria-selected="true">Harian</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-weekly-tab" data-toggle="pill"
                                            href="#custom-tabs-one-weekly" role="tab" aria-controls="custom-tabs-one-weekly"
                                            aria-selected="true">Mingguan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-monthly-tab" data-toggle="pill"
                                            href="#custom-tabs-one-monthly" role="tab" aria-controls="custom-tabs-one-monthly"
                                            aria-selected="true">Bulanan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-yearly-tab" data-toggle="pill"
                                            href="#custom-tabs-one-yearly" role="tab" aria-controls="custom-tabs-one-yearly"
                                            aria-selected="true">Tahunan</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-one-tabContent">

                                    <div class="tab-pane fade show active" id="custom-tabs-one-allreport" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-allreport-tab">
                                        @include('Management::report_out.allreport')
                                    </div>
                                    <div class="tab-pane fade show" id="custom-tabs-one-daily" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-daily-tab">
                                        @include('Management::report_out.daily')
                                    </div>
                                    <div class="tab-pane fade show" id="custom-tabs-one-weekly" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-weekly-tab">
                                        @include('Management::report_out.weekly')
                                    </div>
                                    <div class="tab-pane fade show" id="custom-tabs-one-monthly" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-monthly-tab">
                                        @include('Management::report_out.monthly')
                                    </div>
                                    <div class="tab-pane fade show" id="custom-tabs-one-yearly" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-yearly-tab">
                                        @include('Management::report_out.yearly')
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
    </div>
@endsection
