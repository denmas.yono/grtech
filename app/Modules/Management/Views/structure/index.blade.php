@extends('adminlte::page')  
@section('content') 
    <div id="container">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Data Jamaah dan Pengurus</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-one-structure-tab" data-toggle="pill"
                                            href="#custom-tabs-one-structure" role="tab" aria-controls="custom-tabs-one-structure"
                                            aria-selected="true">Struktur Kepengurusan</a>
                                    </li> 
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-section-tab" data-toggle="pill"
                                            href="#custom-tabs-one-section" role="tab" aria-controls="custom-tabs-one-section"
                                            aria-selected="true">Seksi Kepengurusan</a>
                                    </li> 
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-people-tab" data-toggle="pill"
                                            href="#custom-tabs-one-people" role="tab" aria-controls="custom-tabs-one-people"
                                            aria-selected="true">Jamaah</a>
                                    </li>
                                    
                                   
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-one-tabContent">
                                    
                                    <div class="tab-pane fade show active" id="custom-tabs-one-structure" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-structure-tab"> 
                                        @include('Structure::list')
                                    </div> 
                                    <div class="tab-pane fade show" id="custom-tabs-one-section" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-section-tab"> 
                                        @include('Position::list')
                                    </div>  
                                    <div class="tab-pane fade show" id="custom-tabs-one-people" role="tabpanel"
                                    aria-labelledby="custom-tabs-one-people-tab"> 
                                    @include('People::list')
                                </div> 
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
    </div>
    
@endsection
