{{ Form::model($income_detail, ['method' => 'POST', 'route' => ['income_detail.update', $income_detail->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}

<div class="card-body">
  <div class="form-group">
    <label for="name">Nama Kategori</label>
    <input type="text" class="form-control" id="name" name="name" value="{{$income_detail->name}}" placeholder="Nama">
  </div>
</div>

{{ Form::close() }}