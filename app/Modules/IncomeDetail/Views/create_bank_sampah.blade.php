{{ Form::open(['method' => 'POST', 'route' => ['income_detail.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
{{Form::hidden('income_id', $income->id)}}
<div class="card-body"> 
    <div class="form-group">
        <label for="trash_category_id">Jenis Sampah</label> 
        {{ Form::select('trash_category_id', \Models\TrashCategory::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker']) }}
    </div>
    <div class="form-group">
        <label for="weight">Jumlah Timbangan</label>
        {{ Form::text('weight', old('weight'), ['class' => 'form-control', 'id' => 'weight',"placeholder"=>"Jumlah Timbangan"]) }}
    </div>
    <div class="form-group">
        <label for="price">Jumlah/Harga</label>
        {{ Form::text('price', old('price'), ['class' => 'form-control', 'id' => 'price',"placeholder"=>"Jumlah/Harga"]) }}
    </div>
</div>
{{ Form::close() }}
