{{ Form::open(['method' => 'POST', 'route' => ['income_detail.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
{{Form::hidden('income_id', $income->id)}}
<div class="card-body">
    <div class="form-group">
        <label for="name">Kategori Pendapatan</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"
            placeholder="Nama Pendapatan">
    </div>
</div>
{{ Form::close() }}
