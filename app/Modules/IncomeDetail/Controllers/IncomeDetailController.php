<?php

namespace App\Modules\IncomeDetail\Controllers;

use Models\IncomeDetail as income_detail; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreIncomeDetail;
       
class IncomeDetailController extends MainController
{
    public function __construct() { 
        parent::__construct(new income_detail(), 'income_detail');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('IncomeDetail::index');
    }

    public function getListAjax( $income_id){
        if (request()->ajax()) {
            $income_categories = $this->_model::where('income_id', $income_id)->select('*');
            return datatables()->of($income_categories)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('income_detail.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('income_detail.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('income_detail.destroy',$row->id), 'preview'=> route('income_detail.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $income_id)
    {
        $income = \Models\Income::find($income_id);
        if($income){ 
            if($income->income_category->isBankSampah()){
                return view('IncomeDetail::create_bank_sampah', ['income'=> $income]);
            }else if($income->income_category->isSumbanganWarga()){
                return view('IncomeDetail::create_sumbangan_warga', ['income'=> $income]);
            }else{
                return view('IncomeDetail::create_kotak_amal', ['income'=> $income]);
            }
        }
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreIncomeDetail $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('income_detail.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\IncomeDetail  $income_detail
     * @return \Illuminate\Http\Response
     */
    public function show($income_id)
    {
        $income_detail = $this->_model::find($income_id);
        return view('IncomeDetail::show', ['income_detail'=> $income_detail]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\IncomeDetail  $income_detail
     * @return \Illuminate\Http\Response
     */
    public function preview($income_id)
    {
        $income_detail = $this->_model::find($income_id);
        return view('IncomeDetail::preview', ['income_detail'=> $income_detail]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\IncomeDetail  $income_detail
     * @return \Illuminate\Http\Response
     */
    public function edit($income_id)
    {
        $income_detail = $this->_model::find($income_id);
        return view('IncomeDetail::edit', ['income_detail'=> $income_detail]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\IncomeDetail  $income_detail
     * @return \Illuminate\Http\Response
     */
    public function update( $income_id)
    {  
        try {
            $income_detail = $this->_model::find($income_id);
            if($income_detail){                
                $income_detail->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('income_detail.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\IncomeDetail  $income_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy($income_id)
    {
        try {
            $income_detail = $this->_model::find($income_id);
            if($income_detail){
                $income_detail->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('income_detail.index')], 200);
    }
}
