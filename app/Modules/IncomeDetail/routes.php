<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'income_detail', 'namespace' => 'App\Modules\IncomeDetail\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'income_detail.index', 'uses' => 'IncomeDetailController@index']);
    Route::get('/getListAjax/{income_id}', ['as' => 'income_detail.getListAjax', 'uses' => 'IncomeDetailController@getListAjax']);
    Route::get('/create/{income_id}', ['as' => 'income_detail.create', 'uses' => 'IncomeDetailController@create']);
    Route::get('/show/{id}', ['as' => 'income_detail.show', 'uses' => 'IncomeDetailController@show']);    
    Route::get('/preview/{id}', ['as' => 'income_detail.preview', 'uses' => 'IncomeDetailController@preview']);    
    Route::post('/store', ['as' => 'income_detail.store', 'uses' => 'IncomeDetailController@store']);
    Route::get('/edit/{id}', ['as' => 'income_detail.edit', 'uses' => 'IncomeDetailController@edit']);
    Route::put('/update/{id}', ['as' => 'income_detail.update', 'uses' => 'IncomeDetailController@update']);
    Route::delete('/delete/{id}', ['as' => 'income_detail.destroy', 'uses' => 'IncomeDetailController@destroy']);
});