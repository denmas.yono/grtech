{{ Form::model($income, ['method' => 'POST', 'route' => ['income.update', $income->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Nama Pendapatan</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name',"placeholder"=>"Nama pendapatan"]) }}
    </div>
    <div class="form-group">
        <label for="amount">Jumlah / Nominal Uang</label>
        {{ Form::text('amount', null, ['class' => 'form-control', 'id' => 'amount',"placeholder"=>"Nama pendapatan"]) }}
    </div>
    <div class="form-group">
        <label for="date">Tanggal</label>
        {{ Form::date('date', null, ['class' => 'form-control', 'id' => 'date',"placeholder"=>"Nama pendapatan"]) }}
    </div>
    <div class="form-group">
        <label for="income_type_id">Tipe</label> 
        {{ Form::select('income_type_id', \Models\IncomeType::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker']) }}
    </div>
    <div class="form-group">
        <label for="income_category_id">Kategori</label> 
        {{ Form::select('income_category_id', \Models\IncomeCategory::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker']) }}
    </div>
    <div class="form-group">
        <label for="template_id">Template</label>
        {{ Form::select('template_id', \Models\Template::pluck('name','id')->all(), null, ['class' => 'form-control','id' => 'template_id','placeholder' => '--Pilih Template--']) }}                    
    </div>
    <div class="form-group">
        <label for="description">Keterangan</label>
        {{ Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description',"placeholder"=>"Nama pendapatan", 'rows'=> '3']) }}
    </div>
</div>

{{ Form::close() }}
<script src="{{ asset('js/apps.js') }}"></script>