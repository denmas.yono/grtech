<?php

namespace App\Modules\Event\Controllers;

use Models\Event as event; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreEvent;
       
class EventController extends MainController
{
    public function __construct() { 
        parent::__construct(new event(), 'event');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Event::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $events = $this->_model::with(['event_type'])->select('*');
            return datatables()->of($events)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('event.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('event.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('event.destroy',$row->id), 'preview'=> route('event.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Event::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEvent $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('event.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($event_id)
    {
        $event = $this->_model::find($event_id);
        return view('Event::show', ['event'=> $event]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function preview($event_id)
    {
        $event = $this->_model::find($event_id);
        return view('Event::preview', ['event'=> $event]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit($event_id)
    {
        $event = $this->_model::find($event_id);
        return view('Event::edit', ['event'=> $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update( $event_id)
    {  
        try {
            $event = $this->_model::find($event_id);
            if($event){                
                $event->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('event.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_id)
    {
        try {
            $event = $this->_model::find($event_id);
            if($event){
                $event->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('event.index')], 200);
    }
}
