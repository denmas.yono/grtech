{{ Form::open(['method' => 'POST','route'=> ['event.store'], 'class'=> 'form-horizontal', 'enctype'=> 'multipart/form-data']) }}
    <div class="card-body"> 
        <div class="form-group">
            <label for="name">Nama Kegiatan</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"
                placeholder="Nama Kegiatan">
        </div>
        <div class="form-group">
            <label for="parent">Kategori Kegiatan</label> 
            {{Form::select('event_type_id', \Models\EventType::pluck('name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "type", "data-live-search"=> "true", 'data-style'=> 'btn-success'])}}
        </div> 
        <div class="form-group">
          <label for="start_date">Mulai</label> 
          {{Form::date('date_start', null, ['class'=> 'form-control'])}}
        </div> 
        <div class="form-group">
          <label for="end_date">Berakhir</label> 
          {{Form::date('date_end', null, ['class'=> 'form-control'])}}
        </div>
        <div class="form-group">
          <label for="template_id">Template</label>
          {{ Form::select('template_id', \Models\Template::pluck('name','id')->all(), null, ['class' => 'form-control','id' => 'template_id','placeholder' => '--Pilih Template--']) }}                    
      </div>
        <div class="form-group">
          <label for="name">Tujuan</label>
          {{Form::textarea('description', null, ["class"=> "form-control", 'rows'=> 3, "placeholder"=> "Isi Tujuan Kegiatan"])}}
        </div>
    </div>
    <!-- /.card-body -->
{{Form::close()}}
@push('js')
    <script>
      $(function(){
        $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });
      })
      </script>
@endpush