@extends('adminlte::page')

@push('css')
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/ekko-lightbox/ekko-lightbox.css') }}">
    
@endpush
@section('plugins.Select2', true)
@section('content') 
    <div id="container">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Detail Kegiatan</h1>
                        {!! kembali(['url'=> route('event.index'), 'title'=> 'Kembali']);!!}
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content-header">
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill"
                                            href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home"
                                            aria-selected="true">Panitia</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-cost-tab" data-toggle="pill"
                                            href="#custom-tabs-one-cost" role="tab"
                                            aria-controls="custom-tabs-one-cost" aria-selected="false">Rancangan Biaya</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill"
                                            href="#custom-tabs-one-profile" role="tab"
                                            aria-controls="custom-tabs-one-profile" aria-selected="false">Susunan Acara</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-location-tab" data-toggle="pill"
                                            href="#custom-tabs-one-location" role="tab"
                                            aria-controls="custom-tabs-one-settings" aria-selected="false">Lokasi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-attributes-tab" data-toggle="pill"
                                            href="#custom-tabs-one-attributes" role="tab"
                                            aria-controls="custom-tabs-one-settings" aria-selected="false">Perlengkapan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-guest-tab" data-toggle="pill"
                                            href="#custom-tabs-one-guest" role="tab"
                                            aria-controls="custom-tabs-one-messages" aria-selected="false">Tamu Undangan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-documentation-tab" data-toggle="pill"
                                            href="#custom-tabs-one-documentation" role="tab"
                                            aria-controls="custom-tabs-one-settings" aria-selected="false">Dokumentasi</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-one-tabContent">
                                    <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-home-tab"> 
                                        @include('Event::slices.organizer')
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-cost" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-cost-tab">
                                        @include('Event::slices.cost')
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-profile-tab">
                                        @include('Event::slices.rundown')
                                    </div>

                                    <div class="tab-pane fade" id="custom-tabs-one-location" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-location-tab">
                                        @include('Event::slices.location')
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-attributes" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-attributes-tab">
                                        @include('Event::slices.attributes')
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-guest" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-guest-tab">
                                        @include('Event::slices.guest')
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-one-documentation" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-documentation-tab">
                                        @include('Event::slices.documentation')
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
         
        </section>
    </div>
    @push('js')
        <script src="{{ asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
        <script src="{{ asset('plugins/filterizr/jquery.filterizr.min.js') }}"></script>
        <script src="{{ asset('js/demo.js') }}"></script>
        <script>
            $(function() {
                $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                    event.preventDefault();
                    $(this).ekkoLightbox({
                        alwaysShowClose: true
                    });
                });

                $('.filter-container').filterizr({
                    gutterPixels: 3
                });
                $('.btn[data-filter]').on('click', function() {
                    $('.btn[data-filter]').removeClass('active');
                    $(this).addClass('active');
                });
            })
        </script>
    @endpush
@endsection
