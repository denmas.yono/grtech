<section class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><b>Rancangan Biaya</b></h3> 
              {!!create(['url'=> route('event_cost.create', ['event_id'=> $event->id]), 'title'=> 'Tambah Kegiatan', 'style'=> "float: right;"]) !!}
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="event_cost" class="table table-bordered table-hover display standard" data-route="{{ route('event_cost.getListAjax', ['event_id'=> $event->id]) }}">
                <thead>
                <tr>
                  <th data-name="id">No</th> 
                  <th data-name="name">Keperluan</th>  
                  <th data-name="amount">Jumlah</th>  
                  <th data-name="subtotal" data-format="money">Nominal</th>  
                  <th data-name="description">Tujuan</th>  
                  <th data-name="action" nowrap="nowrap">Action</th>  
                </tr>
                </thead>
                <tbody>
                   
                </tbody> 
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
    </div><!-- /.container-fluid -->
  </section>