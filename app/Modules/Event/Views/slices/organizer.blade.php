<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-2">
          <b>Ketua</b>
        </div> 
        <div class="col-sm-3">
          <b>: Taryono</b>
        </div> 
      </div>
      <div class="row mb-2">
        <div class="col-sm-2">
          <b>Wakil Ketua</b>
        </div> 
        <div class="col-sm-3">
          <b>: Santoso Muhammad Nur</b>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  @foreach (\Models\EventSection::where('type', 2)->get() as $section) 
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"><b>{{strtoupper($section->name)}}</b></h3> 
                {!!create(['url'=> route('event_organizer.create', ['event_id'=> $event->id, 'event_section_id'=> $section->id]), 'title'=> 'Tambah Kegiatan', 'style'=> "float: right;"]) !!}
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="{{$section->name}}" class="table table-bordered table-hover display standard" style="width: 100%!important" data-route="{{ route('event_organizer.getListAjaxEventSection', ['event_id'=> $event->id, 'event_section_id'=> $section->id]) }}">
                  <thead>
                  <tr>
                    <th data-name="id">No</th> 
                    <th data-name="people.first_name">Nama</th>  
                    <th data-name="action" nowrap="nowrap">Action</th> 
                  </tr>
                  </thead>
                  <tbody>
                    
                  </tbody> 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
  @endforeach
  