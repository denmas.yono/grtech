 <section class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><b>Susunan Acara</b></h3> 
              {!!create(['url'=> route('event_rundown.create', ['event_id'=> $event->id]), 'title'=> 'Tambah Kegiatan', 'style'=> "float: right;"]) !!}
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="event_rundown" class="table table-bordered table-hover display standard" data-route="{{ route('event_rundown.getListAjax', ['event_id'=> $event->id]) }}">
                <thead>
                <tr>
                  <th data-name="id">No</th> 
                  <th data-name="sequence">Urutan Acara</th>  
                  <th data-name="name">Nama Acara</th>  
                  <th data-name="date_start">Mulai</th>  
                  <th data-name="date_end">Selesai</th>  
                  <th data-name="by_name">Di isi oleh</th>  
                  <th data-name="description">Tujuan</th> 
                  <th data-name="action" nowrap="nowrap">Action</th>  
                </tr>
                </thead>
                <tbody>
                   
                </tbody> 
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
    </div><!-- /.container-fluid -->
  </section>