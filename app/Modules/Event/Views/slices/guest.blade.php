<section class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><b>Jamaah Atau Tamu Undangan</b></h3> 
              {!!create(['url'=> route('event_guest.create', ['event_id'=> $event->id]), 'title'=> 'Tambah Undangan', 'style'=> "float: right;"]) !!}               
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="event_guest" class="table table-bordered table-hover display standard" data-route="{{ route('event_guest.getListAjax', ['event_id'=> $event->id]) }}">
                <thead>
                <tr>
                  <th data-name="id">No</th> 
                  <th data-name="people.full_name">Nama</th>  
                  <th data-name="as">Sebagai</th>  
                  <th data-name="address">Alamat</th>  
                  <th data-name="description">Tujuan</th>  
                  <th data-name="action" nowrap="nowrap">Action</th> 
                </tr>
                </thead>
                <tbody>
                   
                </tbody> 
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
    </div><!-- /.container-fluid -->
  </section>