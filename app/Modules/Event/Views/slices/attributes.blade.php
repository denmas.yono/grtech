<section class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><b>Perlengkapan Yang Digunakan</b></h3> 
              {!!create(['url'=> route('event_attribute.create', ['event_id'=> $event->id]), 'title'=> 'Tambah Kegiatan', 'style'=> "float: right;"]) !!}
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="event_attribute" class="table table-bordered table-hover display standard" style="width: 100%" data-route="{{ route('event_attribute.getListAjax', ['event_id'=> $event->id]) }}">
                <thead>
                <tr>
                  <th data-name="id">No</th> 
                  <th data-name="name">Nama Barang</th>  
                  <th data-name="amount">Jumlah</th>  
                  <th data-name="subtotal">Biaya</th> 
                  <th data-name="description">Tujuan</th> 
                  <th data-name="action" nowrap="nowrap">Action</th> 
                </tr>
                </thead>
                <tbody>
                   
                </tbody> 
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
    </div><!-- /.container-fluid -->
  </section>