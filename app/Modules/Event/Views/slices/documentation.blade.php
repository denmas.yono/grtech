<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card-body">
                    <div>
                        <!--div class="btn-group w-100 mb-2">
                            <a class="btn btn-info active" href="javascript:void(0)" data-filter="all"> All items </a>
                            <a class="btn btn-info" href="javascript:void(0)" data-filter="1"> (WHITE) </a>
                            <a class="btn btn-info" href="javascript:void(0)" data-filter="2"> (BLACK) </a>
                            <a class="btn btn-info" href="javascript:void(0)" data-filter="3"> (COLORED) </a>
                            <a class="btn btn-info" href="javascript:void(0)" data-filter="4"> (COLORED, BLACK) </a>
                        </div-->
                        <div class="mb-2 mb-12">
                            <a class="btn btn-secondary" href="javascript:void(0)"
                                data-shuffle> Shuffle items </a>
                            <div class="float-right">
                                <select class="custom-select" style="width: auto;"
                                    data-sortOrder>
                                    <option value="index"> Sort by Position
                                    </option>
                                    <option value="sortData"> Sort by Custom Data
                                    </option>
                                </select>
                                <div class="btn-group">
                                    <a class="btn btn-default"
                                        href="javascript:void(0)" data-sortAsc>
                                        Ascending </a>
                                    <a class="btn btn-default"
                                        href="javascript:void(0)" data-sortDesc>
                                        Descending </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="filter-container p-0 row">
                            <div class="filtr-item col-sm-2" data-category="1"
                                data-sort="white sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 1 - white">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="white sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="2, 4"
                                data-sort="black sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 2 - black">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="black sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="3, 4"
                                data-sort="red sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 3 - red">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="red sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="3, 4"
                                data-sort="red sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 4 - red">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="red sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="2, 4"
                                data-sort="black sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 5 - black">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="black sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="1"
                                data-sort="white sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 6 - white">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="white sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="1"
                                data-sort="white sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 7 - white">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="white sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="2, 4"
                                data-sort="black sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 8 - black">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="black sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="3, 4"
                                data-sort="red sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 9 - red">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="red sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="1"
                                data-sort="white sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 10 - white">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="white sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="1"
                                data-sort="white sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="sample 11 - white">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="white sample" />
                                </a>
                            </div>
                            <div class="filtr-item col-sm-2" data-category="2, 4"
                                data-sort="black sample">
                                <a href="{{ asset('assets/images/company.png') }}"
                                    data-toggle="lightbox"
                                    data-title="Album Sampul">
                                    <img src="{{ asset('assets/images/company.png') }}"
                                        class="img-fluid mb-2" alt="black sample" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>