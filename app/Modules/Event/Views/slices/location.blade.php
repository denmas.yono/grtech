{{ Form::open(['method' => 'POST','route'=> ['event.store'], 'class'=> 'form-horizontal', 'enctype'=> 'multipart/form-data']) }}
<section class="content-header">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><b>Lokasi Acara</b></h3>  
            </div>
            <!-- /.card-header -->
            <div class="card-body"> 
               {{Form::textarea('location', null, ["class"=>'form-control','placeholder'=> "lokasi Acara Diselenggarakan", "rows"=> 3])}}
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
    </div><!-- /.container-fluid -->
  </section>
  {{Form::close()}}
@push('js')
    <script>
      $(function(){
        $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });
      })
      </script>
@endpush