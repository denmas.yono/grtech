<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'event', 'namespace' => 'App\Modules\Event\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'event.index', 'uses' => 'EventController@index']);
    Route::get('/getListAjax', ['as' => 'event.getListAjax', 'uses' => 'EventController@getListAjax']);
    Route::get('/create', ['as' => 'event.create', 'uses' => 'EventController@create']);
    Route::get('/show/{id}', ['as' => 'event.show', 'uses' => 'EventController@show']);    
    Route::get('/preview/{id}', ['as' => 'event.preview', 'uses' => 'EventController@preview']);    
    Route::post('/store', ['as' => 'event.store', 'uses' => 'EventController@store']);
    Route::get('/edit/{id}', ['as' => 'event.edit', 'uses' => 'EventController@edit']);
    Route::put('/update/{id}', ['as' => 'event.update', 'uses' => 'EventController@update']);
    Route::delete('/delete/{id}', ['as' => 'event.destroy', 'uses' => 'EventController@destroy']);
});