  {{ Form::model($asset_category, ['method' => 'POST', 'route' => ['asset_category.update', $asset_category->id], 'class' => 'form-horizontal']) }}
  <div class="card-body">
      <div class="form-group">
          <label for="name">Nama</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ $asset_category->name }}"
              placeholder="Nama">
      </div>
      <div class="form-group"> 
        <label for="description">Keterangan</label>
        {{Form::textarea('description', null, ['class'=> 'form-control', 'placeholder'=> "keterangan", "rows"=> 3])}}                 
    </div>
  </div>
  {{ Form::close() }}
