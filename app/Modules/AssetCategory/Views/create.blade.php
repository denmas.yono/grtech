{{ Form::open(['method' => 'POST', 'route' => ['asset_category.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Nama</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Nama">
    </div>
    <div class="form-group"> 
        <label for="description">Keterangan</label>
        {{Form::textarea('description', old('description'), ['class'=> 'form-control', 'placeholder'=> "keterangan", "rows"=> 3])}}                 
    </div>
</div>
{{ Form::close() }}
