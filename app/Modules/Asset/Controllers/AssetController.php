<?php

namespace App\Modules\Asset\Controllers;

use Models\Asset as asset; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreAsset;
use Illuminate\Support\Facades\Auth;

class AssetController extends MainController
{
    public function __construct() { 
        parent::__construct(new Asset(), 'asset');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        return view('Asset::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $assets = $this->_model::with(['asset_type','asset_status', 'asset_category']);
             
            return datatables()->of($assets)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){ 
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('asset.edit',$row->id), 'title'=> $row->name]); 
                            //$btn .= show(['url'=> route('asset.show',$row->id), 'title'=> $row->name, 'popup'=> true]);
                            $btn .= hapus(['url'=> route('asset.destroy',$row->id), 'preview'=> route('asset.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
                        }
                    })->filterColumn('subtotal', function ($row, $search) {
                         return $row->where("amount", 'Like', "%{$search}%")->orWhere("amount", 'Like', "%{$search}%"); 
                    })->orderColumn('subtotal', function ($row) {
                        return $row->orderBy("amount",  'desc'); 
                   })
                    ->rawColumns(['action','subtotal'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Asset::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAsset $request)
    {   
        try { 
            $asset = $this->_model::create($this->_serialize($request));
            if (request()->file('photo')) {                
                $this->validate($request, [ 
                    'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]); 
                
                if (request()->file('photo')->isValid()) {
                    $file = request()->file('photo');
                    // image upload in storage/app/public/photo   
                    $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/assets')));
                    if($asset->photo && file_exists(storage_path('app/public/assets/'.$asset->photo))){
                        unlink(storage_path('app/public/photos/'.$asset->photo));
                    }
                    $asset->photo = $info->getFilename(); 
                    $asset->save();
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                }
            } 
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('asset.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show($asset_id)
    {
        $asset = $this->_model::find($asset_id);
        return view('Asset::show', ['asset'=> $asset]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function preview($asset_id)
    {
        $asset = $this->_model::find($asset_id);
        return view('Asset::preview', ['asset'=> $asset]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit($asset_id)
    {
        $asset = $this->_model::find($asset_id);
        return view('Asset::edit', ['asset'=> $asset]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update( $asset_id)
    {  
        try {
            $asset = $this->_model::find($asset_id);
            if($asset){                
                $asset->update($this->_serialize(request()));

                if (request()->file('photo')) {                
                    $this->validate(request(), [ 
                        'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]); 
                    
                    if (request()->file('photo')->isValid()) {
                        $file = request()->file('photo');
                        // image upload in storage/app/public/photo   
                        $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/assets')));
                        if($asset->photo && file_exists(storage_path('app/public/assets/'.$asset->photo))){
                            unlink(storage_path('app/public/photos/'.$asset->photo));
                        }
                        $asset->photo = $info->getFilename(); 
                        $asset->save();
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                    }
                }
                
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('asset.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy($asset_id)
    {
        try {
            $asset = $this->_model::find($asset_id);
            if($asset){
                $asset->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('asset.index')], 200);
    }
}
