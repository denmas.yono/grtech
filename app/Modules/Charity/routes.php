<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'charity', 'namespace' => 'App\Modules\Charity\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'charity.index', 'uses' => 'CharityController@index']);
    Route::get('/getListAjax', ['as' => 'charity.getListAjax', 'uses' => 'CharityController@getListAjax']);
    Route::get('/create', ['as' => 'charity.create', 'uses' => 'CharityController@create']);
    Route::get('/show/{id}', ['as' => 'charity.show', 'uses' => 'CharityController@show']);    
    Route::get('/preview/{id}', ['as' => 'charity.preview', 'uses' => 'CharityController@preview']);    
    Route::post('/store', ['as' => 'charity.store', 'uses' => 'CharityController@store']);
    Route::get('/edit/{id}', ['as' => 'charity.edit', 'uses' => 'CharityController@edit']);
    Route::put('/update/{id}', ['as' => 'charity.update', 'uses' => 'CharityController@update']);
    Route::delete('/delete/{id}', ['as' => 'charity.destroy', 'uses' => 'CharityController@destroy']);
});