{{ Form::model($charity, ['method' => 'POST', 'route' => ['charity.update', $charity->id], 'class' => 'form-horizontal']) }}
<div class="card-body">
  <div class="form-group">
    <label for="name">Nama</label>
    <input type="text" class="form-control" id="name" name="name" value="{{$charity->name}}" placeholder="Email">
</div>
  <div class="form-group">
      <label for="email">Email</label>
      <input type="email" class="form-control" id="email" name="email" value="{{$charity->email}}" placeholder="Email">
  </div>
  <div class="form-group">
    <label for="cellphone">No Hanphone</label>
    <input type="text" class="form-control" id="cellphone" name="cellphone" value="{{$charity->cellphone}}" placeholder="Handphone">
  </div>
  <div class="form-group">
    <label for="age">Usia</label>
    <input type="number" class="form-control" id="age" name="age" value="{{$charity->age}}" placeholder="Usia">
  </div>
</div>
{{ Form::close() }}