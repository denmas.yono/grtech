{{ Form::open(['method' => 'POST', 'route' => ['charity.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body"> 
  <div class="form-group">
    <label for="name">Nama</label>
    <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Nama">
</div> 
  <div class="form-group">
      <label for="email">Email</label>
      <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}" placeholder="Email">
  </div> 
  <div class="form-group">
    <label for="cellphone">No Handphone</label>
    <input type="text" class="form-control" id="cellphone" name="cellphone" value="{{old('cellphone')}}" placeholder="No Handphone">
  </div> 
  <div class="form-group">
    <label for="age">Usia</label>
    <input type="text" class="form-control" id="age" name="age" value="{{old('age')}}" placeholder="Usia">
  </div> 
</div>
{{ Form::close() }}