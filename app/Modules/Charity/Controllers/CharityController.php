<?php

namespace App\Modules\Charity\Controllers;

use Models\Charity as charity; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreCharity;
       
class CharityController extends MainController
{
    public function __construct() { 
        parent::__construct(new charity(), 'charity');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Charity::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $charitys = $this->_model::select('*');
            return datatables()->of($charitys)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('charity.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= hapus(['url'=> route('charity.destroy',$row->id), 'preview'=> route('charity.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Charity::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCharity $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('charity.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Charity  $charity
     * @return \Illuminate\Http\Response
     */
    public function show($charity_id)
    {
        $charity = $this->_model::find($charity_id);
        return view('Charity::show', ['charity'=> $charity]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Charity  $charity
     * @return \Illuminate\Http\Response
     */
    public function preview($charity_id)
    {
        $charity = $this->_model::find($charity_id);
        return view('Charity::preview', ['charity'=> $charity]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Charity  $charity
     * @return \Illuminate\Http\Response
     */
    public function edit($charity_id)
    {
        $charity = $this->_model::find($charity_id);
        return view('Charity::edit', ['charity'=> $charity]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Charity  $charity
     * @return \Illuminate\Http\Response
     */
    public function update( $charity_id)
    {  
        try {
            $charity = $this->_model::find($charity_id);
            if($charity){                
                $charity->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('charity.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Charity  $charity
     * @return \Illuminate\Http\Response
     */
    public function destroy($charity_id)
    {
        try {
            $charity = $this->_model::find($charity_id);
            if($charity){
                $charity->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('charity.index')], 200);
    }
}
