{{ Form::model($people, ['method' => 'PUT', 'route' => ['people.update', $people->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group"> 
        {{ Form::text('first_name', $people->first_name, ['class' => 'form-control ucword', 'id' => 'first_name', 'placeholder' => 'Nama Depan']) }}
    </div>
    <div class="form-group"> 
        {{ Form::text('last_name', $people->last_name, ['class' => 'form-control ucword', 'id' => 'last_name', 'placeholder' => 'Nama Belakang']) }}
    </div>
    <div class="form-group"> 
        {{ Form::text('pob', $people->pob, ['class' => 'form-control', 'id' => 'pob', 'placeholder' => 'Kota Kelahiran']) }}
    </div>
    <div class="form-group"> 
        {{ Form::date('dob', $people->dob, ['class' => 'form-control', 'id' => 'dob', 'placeholder' => 'Tgl lahir']) }}
    </div>
    <div class="form-group"> 
        {{ Form::radio('sex', 'Laki - laki', ['class' => 'form-control', 'id' => 'l',$people->sex== "l"?"checked=checked":'']) }} Laki - laki
        {{ Form::radio('sex', 'Perempuan', ['class' => 'form-control', 'id' => 'p',$people->sex== "p"?"checked=checked":'']) }} Perempuan
    </div>
    <div class="form-group"> 
        {{ Form::radio('marital_status', 'Single', ['class' => 'form-control', 'id' => 'Single']) }} Single
        {{ Form::radio('marital_status', 'Menikah', ['class' => 'form-control', 'id' => 'Menikah']) }} Menikah
        {{ Form::radio('marital_status', 'Duda', ['class' => 'form-control', 'id' => 'Duda']) }} Duda
        {{ Form::radio('marital_status', 'Janda', ['class' => 'form-control', 'id' => 'Janda']) }} Janda
    </div>
    <div class="form-group"> 
        {{ Form::text('parent_name', $people->parent_name, ['class' => 'form-control', 'id' => 'parent_name', 'placeholder' => 'Nama Orang Tua']) }}
    </div>
    <div class="form-group"> 
        {{ Form::text('cellphone', $people->cellphone, ['class' => 'form-control', 'id' => 'cellphone', 'placeholder' => 'No Handphone']) }}
    </div>
    <div class="form-group"> 
        {{ Form::text('email', $people->email, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email']) }}
    </div>
    <div class="form-group"> 
        {{ Form::select('mosque_id', \Models\Mosque::pluck('name', 'id')->all(), null, ['class' => 'form-control selectpicker', 'placeholder' => 'Masjid']) }}
    </div>
    <div class="form-group"> 
        {{ Form::select('mushola_id', \Models\Mushola::pluck('name', 'id')->all(), null, ['class' => 'form-control selectpicker', 'placeholder' => 'Mushola']) }}
    </div> 
    <div class="form-group">
        <label for="rt_id">RT</label>
        {{ Form::select('rt_id', \Models\Rt::pluck('name', 'id')->all(), null, ['class' => 'form-control selectpicker']) }}
    </div>
    <div class="form-group">
        <label for="name">Detail Alamat</label>
        {{ Form::textarea('name', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Detail alamat e.g: nama jalan dan nomor rumah']) }}
    </div>
</div>
{{ Form::close() }}

@section('adminlte_js')
@stop
