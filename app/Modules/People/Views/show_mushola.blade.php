<div class="card card-primary card-outline">
    <div class="card-body box-profile">
      <div class="text-center"> 
        <img src="{{$mushola->logo?$mushola->logo:asset('assets/images/mosque.png')}}" class="profile-user-img img-fluid img-circle img-responsive image-logo" data-title="{{$mushola->name}}">
      </div>

      <h3 class="profile-username text-center">{{$mushola->name}}</h3>

      <p class="text-muted text-center">Website: {{$mushola->website}}</p> 
    </div>
    <!-- /.card-body -->
  </div>