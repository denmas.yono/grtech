{{ Form::open(['method' => 'POST','route' => ['people.store'],'class' => 'form-horizontal','enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group"> 
        {{ Form::text('first_name', old('first_name'), ['class' => 'form-control ucword','id' => 'first_name','placeholder' => 'Nama Depan']) }}
    </div>
    <div class="form-group"> 
        {{ Form::text('last_name', old('last_name'), ['class' => 'form-control ucword','id' => 'last_name','placeholder' => 'Nama Belakang']) }}
    </div>
    <div class="form-group"> 
      {{ Form::text('pob', old('pob'), ['class' => 'form-control', 'id' => 'pob', 'placeholder' => 'Kota Kelahiran']) }}
    </div>
    <div class="form-group"> 
      {{ Form::date('dob', old('dob'), ['class' => 'form-control', 'id' => 'dob', 'placeholder' => 'Tgl lahir']) }}
    </div>
    <div class="form-group"> 
      {{ Form::radio('sex', 'Laki - laki', ['class' => 'form-control', 'id' => 'l']) }} Laki - laki
      {{ Form::radio('sex', 'Perempuan', ['class' => 'form-control', 'id' => 'p']) }} Perempuan
    </div>
    <div class="form-group"> 
      {{ Form::radio('marital_status', 'Single', ['class' => 'form-control', 'id' => 'l']) }} Single
      {{ Form::radio('marital_status', 'Menikah', ['class' => 'form-control', 'id' => 'l']) }} Menikah
      {{ Form::radio('marital_status', 'Duda', ['class' => 'form-control', 'id' => 'l']) }} Duda
      {{ Form::radio('marital_status', 'Janda', ['class' => 'form-control', 'id' => 'l']) }} Janda'       
    </div>
    <div class="form-group"> 
      {{ Form::text('parent_name', old('parent_name'), ['class' => 'form-control', 'id' => 'parent_name', 'placeholder' => 'Nama Orang Tua']) }}
    </div> 
    <div class="form-group"> 
      {{ Form::text('cellphone', old('cellphone'), ['class' => 'form-control', 'id' => 'cellphone', 'placeholder' => 'No Handphone']) }}
    </div>
    <div class="form-group"> 
        {{ Form::text('email', old('email'), ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email']) }}
    </div>
    <div class="form-group"> 
        {{ Form::select('mosque_id', \Models\Mosque::pluck('name', 'id')->all(), null, ['class' => 'form-control selectpicker', 'placeholder' => 'Masjid']) }}
    </div>
    <div class="form-group"> 
        {{ Form::select('mushola_id', \Models\Mushola::pluck('name', 'id')->all(), null, ['class' => 'form-control selectpicker', 'placeholder' => 'Mushola']) }}
    </div>
     
    <div class="form-group"> 
        {{ Form::textarea('name',  null, ['class' => 'form-control', 'rows'=> 3, 'placeholder'=> 'Detail alamat e.g: nama jalan dan nomor rumah']) }}
    </div>
</div>
{{ Form::close() }}
