@extends('adminlte::page')
@section('content')
    <div id="container">
        {{ Form::model($people, ['method' => 'PUT', 'route' => ['people.update', $people->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
        <div class="card-body">
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                  <label for="first_name">Nama Depan</label>
                  {{ Form::text('first_name', null, ['class' => 'form-control', 'id' => 'first_name', 'placeholder' => 'Nama Depan']) }}
              </div>
              <div class="form-group">
                  <label for="last_name">Nama Belakang</label>
                  {{ Form::text('last_name', null, ['class' => 'form-control', 'id' => 'last_name', 'placeholder' => 'Nama Belakang']) }}
              </div>
              <div class="form-group">
                  <label for="email">Email</label>
                  {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email']) }}
              </div>
              <div class="form-group">
                  <label for="cellphone">Handphone</label>
                  {{ Form::text('cellphone', null, ['class' => 'form-control', 'id' => 'cellphone', 'placeholder' => 'Handphone']) }}
              </div>
              <div class="form-group">
                <label for="mushola_id">Mushola</label>
                {{ Form::select('mushola_id', \Models\Mushola::pluck('name', 'id')->all(), null, ['class' => 'form-control selectpicker']) }}
              </div> 
              <div class="form-group">
                  <label for="mosque_id">Masjid</label>
                  {{ Form::select('mosque_id', \Models\Mosque::pluck('name', 'id')->all(), null, ['class' => 'form-control selectpicker']) }}
              </div> 
            </div> 
            <div class="col-6">
              <div class="form-group"> 
                  <div class="post-review"  style="text-align: center; float: left;">
                      <img src="{{url('photos/'.$people->photo)}}" class="img-responsive" onerror="this.src='{{ asset('assets/images/mushola.png') }}'" width="40%" style="min-width: 200px;">
                      <i class="fa fa-2x fa-camera upload-photo" style="margin-left: -40px; vertical-align: top; position: absolute;"></i>
                    </div>
                    <input type="file" class="post-input d-none" id="photo" name="photo">
              </div> 
            </div>
          </div>  
          <div class="form-group">
            {!! submit(['title'=>'Submit', 'style' =>'float:right']) !!}
            {!! kembali(['url'=> url('people/'),'title'=>'Kembali', 'style' =>'float:left']) !!}
        </div>

    </div>
    {{ Form::close() }}
        <div class="row">
          <div class="col-12"> 
            <div class="form-group">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Alamat</h3>  
                      {!!create(['url'=> route('address.create',['people_id'=> $people->id]), 'title' => 'Tambah Alamat', 'style'=> "float: right;"]) !!}
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                      <table id="asset" class="table table-bordered table-hover display standard" style="width: 100%" data-route="{{ route('address.getListAjax',['people_id'=> $people->id]) }}">
                        <thead>
                          <tr>
                            <th data-name="id">No</th> 
                            <th data-name="name">Detail Alamat</th> 
                            <th data-name="rt.name">RT</th>   
                            <th data-name="rw.name">RW</th>   
                            <th data-name="subdistrict.name">Kelurahan</th>   
                            <th data-name="district.name">Kecamatan</th>   
                            <th data-name="city.name">Kabupaten</th>   
                            <th data-name="region.name">Provinsi</th>   
                            <th data-name="action">Action</th> 
                          </tr>
                        </thead>
                        <tbody>
                           
                        </tbody> 
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
      
                  
                  <!-- /.card -->
                </div>
                <!-- /.col -->
              </div>
            </div>
          </div>
        </div>

    </div>
    @push('js')
      <script type="text/javascript">
        $(function(){
          $(document).on('click', ".upload-photo", function(e){
              $("input.post-input").click();
          })
        })
      </script>    
    @endpush
@stop
