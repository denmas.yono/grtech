<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'people', 'namespace' => 'App\Modules\People\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'people.index', 'uses' => 'PeopleController@index']);
    Route::get('/getListAjax', ['as' => 'people.getListAjax', 'uses' => 'PeopleController@getListAjax']);
    Route::get('/create', ['as' => 'people.create', 'uses' => 'PeopleController@create']);
    Route::get('/show/{id}', ['as' => 'people.show', 'uses' => 'PeopleController@show']);    
    Route::get('/preview/{id}', ['as' => 'people.preview', 'uses' => 'PeopleController@preview']);    
    Route::get('/show_mosque/{id}', ['as' => 'people.show_mosque', 'uses' => 'PeopleController@show_mosque']);    
    Route::get('/show_mushola/{id}', ['as' => 'people.show_mushola', 'uses' => 'PeopleController@show_mushola']);    
    Route::post('/store', ['as' => 'people.store', 'uses' => 'PeopleController@store']);
    Route::get('/edit/{id}', ['as' => 'people.edit', 'uses' => 'PeopleController@edit']);
    Route::get('/getName/{id}', ['as' => 'people.getName', 'uses' => 'PeopleController@getName']);
    Route::put('/update/{id}', ['as' => 'people.update', 'uses' => 'PeopleController@update']);
    Route::delete('/delete/{id}', ['as' => 'people.destroy', 'uses' => 'PeopleController@destroy']);
});