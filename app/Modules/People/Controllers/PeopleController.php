<?php

namespace App\Modules\People\Controllers;

use Models\People as People;  
use App\Http\Controllers\MainController;  
use App\Http\Requests\StorePeople; 
use Lib\File;  

       
class PeopleController extends MainController
{
    public function __construct() { 
        parent::__construct(new People(), 'people');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     
        return view('People::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $peoples = $this->_model::with(['mosque','mushola'])->select('*');
            if(request()->has('rt_id')){
                $rt_id = request()->input('rt_id');
                $peoples->whereHas('address', function($q)use($rt_id){
                    $q->where('rt_id', $rt_id);
                });
            }
            return datatables()->of($peoples)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('people.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('people.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('people.destroy',$row->id), 'preview'=> route('people.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    }) 
                    ->addColumn('mosque', function($row){ 
                        if($row->mosque_id){
                            $link = '<a href data-href="'.(route('people.show_mosque',$row->mosque_id)).'" class="show_popup" data-toggle="modal" data-target="#modalPopupDetail" data-title="'.(isset($row->mosque)?$row->mosque->name:"Noname").'">'.(isset($row->mosque)?$row->mosque->name:"Noname").'</a>';
                            return $link;
                        }
                        return null;
                    }) 
                    ->addColumn('mushola', function($row){ 
                        if($row->mushola_id){
                            $link = '<a href data-href="'.(route('people.show_mushola',$row->mushola_id)).'" class="show_popup" data-toggle="modal" data-target="#modalPopupDetail" data-title="'.(isset($row->mosque)?$row->mushola->name:"Noname").'">'.(isset($row->mushola)?$row->mushola->name:"Noname").'</a>';
                            return $link;
                        }
                        return null;
                    }) ->filterColumn('mosque', function ($row, $search) {
                        return $row->whereHas('mosque', function($q)use($search){
                            $q->where("name", 'Like', "%{$search}%"); 
                        });
                    }) ->filterColumn('mushola', function ($row, $search) {
                        return $row->whereHas('mushola', function($q)use($search){
                            $q->where("name", 'Like', "%{$search}%"); 
                        });
                    }) ->filterColumn('full_name', function ($row, $search) {
                        return $row->where("first_name", 'Like', "%{$search}%")->orWhere("last_name", 'Like', "%{$search}%"); 
                    })
                    ->rawColumns(['action','mosque','mushola'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $rt = request()->has('rt_id')?\Models\Rt::find(request()->input('rt_id')):null;
        return view('People::create',['rt'=> $rt]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePeople $request)
    {   
        try { 
            $people = $this->_model::create($this->_serialize($request));   
            if (request()->file('photo')) {                
                $this->validate($request, [ 
                    'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]); 
                
                if (request()->file('photo')->isValid()) {
                    $file = request()->file('photo');
                    // image upload in storage/app/public/photo   
                    $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/photo')));
                    if($people->photo && file_exists(storage_path('app/public/photos/'.$people->photo))){
                        unlink(storage_path('app/public/photos/'.$people->photo));
                    }
                    $people->photo =  is_object($info)?$info->getFilename():$info; 
                    $people->save(); 
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                }
            } 
            // Notification::route('mail', 'taylor@example.com')
            // ->route('nexmo', '5555555555')
            // ->route('slack', 'https://hooks.slack.com/services/...')
            // ->notify(new MosqueCreated($this->_model));

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('people.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $people = $this->_model::find($id); 
        return view('People::show', ['people'=> $people]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function preview($id)
    {
        $people = $this->_model::find($id); 
        return view('People::preview', ['people'=> $people]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function getName($id)
    {
        $people = $this->_model::find($id);  
        return $people->name;
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function show_mosque($mosque_id)
    {   $mosque = \Models\Mosque::find($mosque_id);
        return view('People::show_mosque', ['mosque'=> $mosque]);
    }

     /**
     * Display the specified resource.
     *
     * @param  \Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function show_mushola($mushola_id)
    {   $mushola = \Models\Mushola::find($mushola_id);
        return view('People::show_mushola', ['mushola'=> $mushola]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function edit($people_id)
    {
        $people = $this->_model::find($people_id);
        return view('People::edit', ['people'=> $people]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function update( $people_id)
    {  
        try {
            $people = $this->_model::find($people_id);
            if($people){        
                $old_photo = $people->photo;        
                $people->update($this->_serialize(request()));
                if (request()->file('photo')) {                
                    $this->validate(request(), [ 
                        'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]); 
                    
                    if (request()->file('photo')->isValid()) {
                        $file = request()->file('photo');
                        // image upload in storage/app/public/photo   
                         
                        $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/photos')));
                        if($old_photo && file_exists(storage_path('app/public/photos/'.$old_photo))){
                            unlink(storage_path('app/public/photos/'.$old_photo));
                        }
                        $people->photo =  is_object($info)?$info->getFilename():$info; 
                        $people->save();
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                    }
                }
            }
        } catch (\Exception $e) {
            dump($e->getTraceAsString());
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('people.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\People  $people
     * @return \Illuminate\Http\Response
     */
    public function destroy($people_id)
    {
        try {
            $people = $this->_model::find($people_id);
            if($people){
                $people->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('people.index')], 200);
    }
}
