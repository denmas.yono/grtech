{{ Form::model($asset_type, ['method' => 'POST', 'route' => ['asset_type.update', $asset_type->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
  <div class="form-group">
    <label for="name">Nama</label>
    <input type="text" class="form-control" id="name" name="name" value="{{$asset_type->name}}" placeholder="Nama">
  </div> 
</div>
{{ Form::close() }}