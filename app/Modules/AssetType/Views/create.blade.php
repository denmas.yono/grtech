{{ Form::open(['method' => 'POST', 'route' => ['asset_type.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Nama</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Nama" required title="Nama Harus Diisi" minlength="3" maxlength="30">
    </div>
</div>
{{ Form::close() }}
