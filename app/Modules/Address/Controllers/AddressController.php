<?php

namespace App\Modules\Address\Controllers;

use App\Http\Controllers\MainController;
use App\Http\Requests\StoreAddress;
use Form;
use Models\Address as address;

class AddressController extends MainController
{
    public function __construct()
    {
        parent::__construct(new address(), 'address');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Address::index');
    }

    public function getListAjax()
    {
        if (request()->ajax()) {
            $events = $this->_model::with(['region', 'city', 'district', 'subdistrict', 'rw', 'rt'])->select('*');
            if (request()->has('people_id')) {
                $events->where('people_id', request()->input('people_id'));
            }
            return datatables()->of($events)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    if ($row) {
                        $btn = '<div class="justify-content-between">';
                        $btn .= edit(['url' => route('address.edit', $row->id), 'title' => $row->name]);
                        $btn .= hapus(['url' => route('address.destroy', $row->id), 'preview' => route('address.preview', $row->id), 'title' => $row->name]);
                        $btn .= '</div>';
                        return $btn;

                    }
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $people_id = request()->has('people_id') ? request()->input('people_id') : null;
        return view('Address::create', ['people_id' => $people_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAddress $request)
    {
        try {
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => ' . $e->getMessage()], 400);
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo' => route('address.index')], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show($event_id)
    {
        $address = $this->_model::find($event_id);
        return view('Address::show', ['address' => $address]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function preview($event_id)
    {
        $address = $this->_model::find($event_id);
        return view('Address::preview', ['address' => $address]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function edit($event_id)
    {
        $address = $this->_model::find($event_id);
        return view('Address::edit', ['address' => $address]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function update($event_id)
    {
        try {
            $address = $this->_model::find($event_id);
            if ($address) {
                $address->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Update Data Error ' . $e->getMessage()], 400);
        }
        return response()->json(['status' => 'success', 'message' => 'Update Data Berhasil.', 'redirectTo' => route('address.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_id)
    {
        try {
            $address = $this->_model::find($event_id);
            if ($address) {
                $address->delete();
            }
        } catch (\Throwable $e) {
            return response()->json(['status' => 'error', 'message' => 'Data Error ' . $e->getMessage()], 400);
        }
        return response()->json(['status' => 'success', 'message' => 'Hapus Data Berhasil.', 'redirectTo' => route('address.index')], 200);
    }

    public function filterArea()
    {
        $id = request()->input('id');
        $field = request()->input('field');
        $instance = request()->input('model');
        try {
            $model = new $instance;
            if (request()->has('index')) {
                $field = str_replace(request()->input('index'), "", $field);
            }
            $data = $model::where($field, $id)->pluck('name', 'id')->all();

            return Form::select($model->getTable() . "_id", $data, null, ['class' => 'form-control selectpicker', 'id' => $model->getTable() . "_id", 'placeholder' => say('--Pilih--'), 'data-live-search' => 'true']);
        } catch (\Throwable $th) {
            //throw $th;
        }

    }
}
