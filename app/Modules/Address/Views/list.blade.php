
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Kota</h3>
                {!!create(['url'=> route('address.create'), 'title'=> 'Tambah Kota', 'style'=> "float: right;"]) !!}
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="address" class="table table-bordered table-hover display standard" style="width: 100%" data-route="{{ route('address.getListAjax') }}">
                  <thead>
                  <tr>
                    <th data-name="id">No</th> 
                    <th data-name="name">Detail Alamat</th> 
                    <th data-name="rt.name">RT</th>   
                    <th data-name="rw.name">RW</th>   
                    <th data-name="subdistrict.name">Kelurahan</th>   
                    <th data-name="district.name">Kecamatan</th>   
                    <th data-name="city.name">Kabupaten</th>   
                    <th data-name="region.name">Provinsi</th>   
                    <th data-name="action">Action</th> 
                  </tr>
                  </thead>
                  <tbody>
                     
                  </tbody> 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content --> 