{{ Form::open(['method' => 'POST', 'route' => ['address.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
{{Form::hidden('people_id', $people_id)}}
<div class="card-body"> 
    
    <div class="form-group">
        <label for="region_id">Provinsi</label>
        {{ Form::select('region_id', [''=> say('-- Pilih Kota Provinsi --')]+Models\Region::where('country_id', 1)->pluck('name', 'id')->all(), null, ['class' => 'form-control area selectpicker', 'id'=> 'region_id','model'=> get_class(new Models\City),'data-target'=> 'city_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="city_id">Kota/Kabupaten</label>
        {{ Form::select('city_id', [''=> say('-- Pilih Kota Kabupaten --')], null, ['class' => 'form-control area selectpicker', 'id'=> 'city_id','model'=> get_class(new Models\District),'data-target'=> 'district_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="district_id">Kecamatan</label>
        {{ Form::select('district_id', [''=> say('-- Pilih Kota Kecamatan --')], null, ['class' => 'form-control area selectpicker', 'id'=> 'district_id','model'=> get_class(new Models\Subdistrict),'data-target'=> 'subdistrict_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="subdistrict_id">Kelurahan</label>
        {{ Form::select('subdistrict_id', [''=> say('-- Pilih Kelurahan --')], null, ['class' => 'form-control area selectpicker', 'id'=> 'subdistrict_id','model'=> get_class(new Models\Rw),'data-target'=> 'rw_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="rw_id">RW</label>
        {{ Form::select('rw_id', [''=> say('-- Pilih RW --')], null, ['class' => 'form-control area selectpicker', 'id'=> 'rw_id','model'=> get_class(new Models\Rt),'data-target'=> 'rt_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="rt_id">RT</label>
        {{ Form::select('rt_id', [''=> say('-- Pilih RT --')], null, ['class' => 'form-control selectpicker', 'id'=> 'rt_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="name">Detail Alamat</label>
        {{ Form::textarea('name', null, ['class' => 'form-control', 'id' => 'name','rows'=> 3, 'placeholder'=> 'No Rumah, Nama Jalan dll']) }}
    </div>
</div>
<!-- /.card-body -->
{{ Form::close() }}
@push('js')
    <script>
        $(function() {
            $('#reservationdatetime').datetimepicker({
                icons: {
                    time: 'far fa-clock'
                }
            });
        })
    </script>
@endpush
