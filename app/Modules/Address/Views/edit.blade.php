{{ Form::model($address, ['method' => 'POST', 'route' => ['address.update', $address->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="region_id">Provinsi</label>
        {{ Form::select('region_id', [''=> say('-- Pilih Kota Provinsi --')]+Models\Region::pluck('name', 'id')->all(), null, ['class' => 'form-control area selectpicker', 'id'=> 'region_id','model'=> get_class(new Models\City),'data-target'=> 'city_id', 'data-live-search' => 'true']) }}
    </div>
    <div class="form-group">
        <label for="city_id">Kota/Kabupaten</label>
        {{ Form::select('city_id', [''=> say('-- Pilih Kota Kabupaten --')]+Models\City::pluck('name', 'id')->all(), null, ['class' => 'form-control area selectpicker', 'id'=> 'city_id','model'=> get_class(new Models\District),'data-target'=> 'district_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="region_id">Kecamatan</label>
        {{ Form::select('district_id', [''=> say('-- Pilih Kota Kecamatan --')]+Models\District::pluck('name', 'id')->all(), null, ['class' => 'form-control area selectpicker', 'id'=> 'district_id','model'=> get_class(new Models\Subdistrict),'data-target'=> 'subdistrict_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="region_id">Kelurahan</label>
        {{ Form::select('subdistrict_id', [''=> say('-- Pilih Kelurahan --')]+Models\Subdistrict::pluck('name', 'id')->all(), null, ['class' => 'form-control area selectpicker', 'id'=> 'subdistrict_id','model'=> get_class(new Models\Rw),'data-target'=> 'rw_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="region_id">RW</label>
        {{ Form::select('rw_id', [''=> say('-- Pilih RW --')]+Models\Rw::pluck('name', 'id')->all(), null, ['class' => 'form-control area selectpicker', 'id'=> 'rw_id','model'=> get_class(new Models\Rt),'data-target'=> 'rt_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="region_id">RT</label>
        {{ Form::select('rt_id', [''=> say('-- Pilih RT --')]+Models\Rt::pluck('name', 'id')->all(), null, ['class' => 'form-control selectpicker', 'id'=> 'rt_id', 'data-live-search' => 'true','data-url'=> route('address.filterArea')]) }}
    </div>
    <div class="form-group">
        <label for="name">Detail Alamat</label>
        {{ Form::textarea('name', null, ['class' => 'form-control', 'id' => 'name','rows'=> "3", 'placeholder'=> 'No Rumah, Nama Jalan dll']) }}
    </div>
</div>
{{ Form::close() }}
