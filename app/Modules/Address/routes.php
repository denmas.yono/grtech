<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'address', 'namespace' => 'App\Modules\Address\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'address.index', 'uses' => 'AddressController@index']);
    Route::get('/getListAjax', ['as' => 'address.getListAjax', 'uses' => 'AddressController@getListAjax']);
    Route::get('/create', ['as' => 'address.create', 'uses' => 'AddressController@create']);
    Route::get('/filterArea', ['as' => 'address.filterArea', 'uses' => 'AddressController@filterArea']);
    Route::get('/show/{id}', ['as' => 'address.show', 'uses' => 'AddressController@show']);    
    Route::get('/preview/{id}', ['as' => 'address.preview', 'uses' => 'AddressController@preview']);    
    Route::post('/store', ['as' => 'address.store', 'uses' => 'AddressController@store']);
    Route::get('/edit/{id}', ['as' => 'address.edit', 'uses' => 'AddressController@edit']);
    Route::put('/update/{id}', ['as' => 'address.update', 'uses' => 'AddressController@update']);
    Route::delete('/delete/{id}', ['as' => 'address.destroy', 'uses' => 'AddressController@destroy']);
});