{{ Form::open(['method' => 'POST', 'route' => ['trash_category.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="code">Kode</label>
        {{ Form::text('code', old('code'), ['class' => 'form-control', 'id' => 'code',"placeholder"=>"Kode"]) }}
    </div>
    <div class="form-group">
        <label for="name">Jenis Sampah</label>
        {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name',"placeholder"=>"Nama"]) }}
    </div>
    <div class="form-group">
        <label for="description">Keterangan</label>
        {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'id' => 'description',"placeholder"=>"Keterangan", 'rows'=> '3']) }}
    </div>
</div>
{{ Form::close() }}
