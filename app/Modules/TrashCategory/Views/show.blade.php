@extends('layouts.app')

@section('content')
<br>
  <div class="card card-widget widget-trash_category col-md-4" style="margin: auto;padding-top: 7.5px;">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-trash_category-header bg-info">
          <h3 class="widget-trash_category-asset_typename">{{ $trash_category->name }}</h3>
          <h5 class="widget-trash_category-desc">Founder &amp; CEO</h5>
      </div>
      <div class="widget-trash_category-image">
          <img src="{{ $trash_category->photo ? $trash_category->photo : trash_category('asset_types/images/trash_category.png') }}"
              class="profile-trash_category-img img-fluid img-circle img-responsive image-logo" data-title="{{ $trash_category->name }}"
              onerror="this.src='{{asset('assets/images/company.png') }}'">
      </div>
      <div class="card-footer">
          <div class="row">
              <div class="col-sm-12 border">
                  <div class="description-block">
                      <h5 class="description-header">3,200</h5>
                      <span class="description-text">SALES</span>
                  </div>
                  <!-- /.description-block -->
              </div>
              <!-- /.col -->
          </div>
          <!-- /.row -->
      </div>
  </div> 
@endsection
