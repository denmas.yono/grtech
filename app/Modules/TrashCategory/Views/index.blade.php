@extends('adminlte::page') 
@section('content')
    <div id="container">
        @include('TrashCategory::list')
    </div> 
    @section('adminlte_js')
        <script>
            $(function() {
                loadTrashCategories();
            });

            function loadTrashCategories() {
                var selected = [];
                $("#trash_category").DataTable({
                    dom:"<'row'<'col-sm-3'l><'col-sm-4'B><'col-sm-5'f>>rtip",
                    responsive: true,
                    scrollY: 400,
                    responsive: true, 
                    processing: true,
                    serverSide: true,  
                    select: true,
                    "rowCallback": function( row, data ) {
                        if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                            $(row).addClass('selected');
                        }
                    },
                    "ajax": "{{ route('trash_category.getListAjax') }}",
                    "columns": [
                        // data:column name from server ,name:alias
                        {
                            data: 'DT_RowIndex',
                            name: 'id'
                        },
                        {
                            data: 'code',
                            name: 'code'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'description',
                            name: 'description'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        },
                    ]
                });
            }
        </script>
     @stop
@stop
