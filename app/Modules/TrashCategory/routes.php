<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'trash_category', 'namespace' => 'App\Modules\TrashCategory\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'trash_category.index', 'uses' => 'TrashCategoryController@index']);
    Route::get('/getListAjax', ['as' => 'trash_category.getListAjax', 'uses' => 'TrashCategoryController@getListAjax']);
    Route::get('/create', ['as' => 'trash_category.create', 'uses' => 'TrashCategoryController@create']);
    Route::get('/show/{id}', ['as' => 'trash_category.show', 'uses' => 'TrashCategoryController@show']);    
    Route::get('/preview/{id}', ['as' => 'trash_category.preview', 'uses' => 'TrashCategoryController@preview']);    
    Route::post('/store', ['as' => 'trash_category.store', 'uses' => 'TrashCategoryController@store']);
    Route::get('/edit/{id}', ['as' => 'trash_category.edit', 'uses' => 'TrashCategoryController@edit']);
    Route::put('/update/{id}', ['as' => 'trash_category.update', 'uses' => 'TrashCategoryController@update']);
    Route::delete('/delete/{id}', ['as' => 'trash_category.destroy', 'uses' => 'TrashCategoryController@destroy']);
});