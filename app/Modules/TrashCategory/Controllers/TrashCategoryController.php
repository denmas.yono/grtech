<?php

namespace App\Modules\TrashCategory\Controllers;

use Models\TrashCategory as trash_category; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreTrashCategory;
       
class TrashCategoryController extends MainController
{
    public function __construct() { 
        parent::__construct(new trash_category(), 'trash_category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('TrashCategory::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $trash_categories = $this->_model::select('*');
            return datatables()->of($trash_categories)
                    ->addIndexColumn() 
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('trash_category.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('trash_category.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('trash_category.destroy',$row->id), 'preview'=> route('trash_category.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('TrashCategory::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTrashCategory $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('trash_category.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\TrashCategory  $trash_category
     * @return \Illuminate\Http\Response
     */
    public function show($trash_category_id)
    {
        $trash_category = $this->_model::find($trash_category_id);
        return view('TrashCategory::show', ['trash_category'=> $trash_category]);
    }
 
    /**
     * Display the specified resource.
     *
     * @param  \Models\TrashCategory  $trash_category
     * @return \Illuminate\Http\Response
     */
    public function preview($trash_category_id)
    {
        $trash_category = $this->_model::find($trash_category_id);
        return view('TrashCategory::preview', ['trash_category'=> $trash_category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\TrashCategory  $trash_category
     * @return \Illuminate\Http\Response
     */
    public function edit($trash_category_id)
    {
        $trash_category = $this->_model::find($trash_category_id);
        return view('TrashCategory::edit', ['trash_category'=> $trash_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\TrashCategory  $trash_category
     * @return \Illuminate\Http\Response
     */
    public function update( $trash_category_id)
    {  
        try {
            $trash_category = $this->_model::find($trash_category_id);
            if($trash_category){                
                $trash_category->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('trash_category.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\TrashCategory  $trash_category
     * @return \Illuminate\Http\Response
     */
    public function destroy($trash_category_id)
    {
        try {
            $trash_category = $this->_model::find($trash_category_id);
            if($trash_category){
                $trash_category->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('trash_category.index')], 200);
    }
}
