<?php

namespace App\Modules\Subject\Controllers;

use Models\Subject as Subject; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController;  
use App\Http\Requests\StoreSubject; 

       
class SubjectController extends MainController
{
    public function __construct() { 
        parent::__construct(new Subject(), 'subject');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     
        return view('Subject::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $subjects = $this->_model::select('*');
            return datatables()->of($subjects)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('subject.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('subject.show',$row->id), 'title'=> $row->name, 'popup'=> true]);
                            $btn .= hapus(['url'=> route('subject.destroy',$row->id), 'preview'=> route('subject.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })
                    ->addColumn('mosque', function($row){ 
                        if($row->mosque_id){
                            $link = '<a href data-href="'.(route('subject.show_mosque',$row->mosque_id)).'" class="detail" data-toggle="modal" data-target="#modalMin" data-title="'.(isset($row->mosque)?$row->mosque->name:"Noname").'">'.(isset($row->mosque)?$row->mosque->name:"Noname").'</a>';
                            return $link;
                        }
                        return null;
                    }) 
                    ->addColumn('mushola', function($row){ 
                        if($row->mushola_id){
                            $link = '<a href data-href="'.(route('subject.show_mushola',$row->mushola_id)).'" class="detail" data-toggle="modal" data-target="#modalMin" data-title="'.(isset($row->mosque)?$row->mushola->name:"Noname").'">'.(isset($row->mushola)?$row->mushola->name:"Noname").'</a>';
                            return $link;
                        }
                        return null;
                    }) 
                    ->rawColumns(['action','mosque','mushola'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Subject::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubject $request)
    {   
        try { 
            $subject = $this->_model::create($this->_serialize($request));   

            // Notification::route('mail', 'taylor@example.com')
            // ->route('nexmo', '5555555555')
            // ->route('slack', 'https://hooks.slack.com/services/...')
            // ->notify(new MosqueCreated($this->_model));

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('subject.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = $this->_model::find($id); 
        return view('Subject::show', ['subject'=> $subject]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function preview($id)
    {
        $subject = $this->_model::find($id); 
        return view('Subject::preview', ['subject'=> $subject]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show_mosque($mosque_id)
    {   $mosque = \Models\Mosque::find($mosque_id);
        return view('Subject::show_mosque', ['mosque'=> $mosque]);
    }

     /**
     * Display the specified resource.
     *
     * @param  \Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show_mushola($mushola_id)
    {   $mushola = \Models\Mushola::find($mushola_id);
        return view('Subject::show_mushola', ['mushola'=> $mushola]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit($subject_id)
    {
        $subject = $this->_model::find($subject_id);
        return view('Subject::edit', ['subject'=> $subject]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update( $subject_id)
    {  
        try {
            $subject = $this->_model::find($subject_id);
            if($subject){                
                $subject->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('subject.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy($subject_id)
    {
        try {
            $subject = $this->_model::find($subject_id);
            if($subject){
                $subject->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('subject.index')], 200);
    }
}
