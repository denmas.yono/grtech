<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'subject', 'namespace' => 'App\Modules\Subject\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'subject.index', 'uses' => 'SubjectController@index']);
    Route::get('/getListAjax', ['as' => 'subject.getListAjax', 'uses' => 'SubjectController@getListAjax']);
    Route::get('/create', ['as' => 'subject.create', 'uses' => 'SubjectController@create']);
    Route::get('/show/{id}', ['as' => 'subject.show', 'uses' => 'SubjectController@show']);    
    Route::get('/preview/{id}', ['as' => 'subject.preview', 'uses' => 'SubjectController@preview']);    
    Route::get('/show_mosque/{id}', ['as' => 'subject.show_mosque', 'uses' => 'SubjectController@show_mosque']);    
    Route::get('/show_mushola/{id}', ['as' => 'subject.show_mushola', 'uses' => 'SubjectController@show_mushola']);    
    Route::post('/store', ['as' => 'subject.store', 'uses' => 'SubjectController@store']);
    Route::get('/edit/{id}', ['as' => 'subject.edit', 'uses' => 'SubjectController@edit']);
    Route::put('/update/{id}', ['as' => 'subject.update', 'uses' => 'SubjectController@update']);
    Route::delete('/delete/{id}', ['as' => 'subject.destroy', 'uses' => 'SubjectController@destroy']);
});