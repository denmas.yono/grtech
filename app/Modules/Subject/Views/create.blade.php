
{{ Form::open(['method' => 'POST','route'=> ['subject.store'], 'class'=> 'form-horizontal', 'enctype'=> 'multipart/form-data']) }}
<div class="card-body">
  <div class="form-group">
    <label for="name">Nama Materi</label> 
    {{ Form::text('name', old('name'), ['class' => 'form-control ucword', 'id' => 'name',"placeholder"=>"Nama Materi", "required"]) }}
  </div>
   
</div>
{{ Form::close() }}