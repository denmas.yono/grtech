{{ Form::model($subject, ['method' => 'POST', 'route' => ['subject.update', $subject->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Nama Materi</label>
        {{ Form::text('name', null, ['class' => 'form-control ucword', 'id' => 'name', 'placeholder' => 'Nama Materi', "required"]) }}
    </div>
</div>
{{ Form::close() }}
