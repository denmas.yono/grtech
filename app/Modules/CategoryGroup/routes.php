<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'category_group', 'namespace' => 'App\Modules\CategoryGroup\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'category_group.index', 'uses' => 'CategoryGroupController@index']);
    Route::get('/getListAjax', ['as' => 'category_group.getListAjax', 'uses' => 'CategoryGroupController@getListAjax']);
    Route::get('/create', ['as' => 'category_group.create', 'uses' => 'CategoryGroupController@create']);
    Route::get('/show/{id}', ['as' => 'category_group.show', 'uses' => 'CategoryGroupController@show']);    
    Route::get('/preview/{id}', ['as' => 'category_group.preview', 'uses' => 'CategoryGroupController@preview']);    
    Route::post('/store', ['as' => 'category_group.store', 'uses' => 'CategoryGroupController@store']);
    Route::get('/edit/{id}', ['as' => 'category_group.edit', 'uses' => 'CategoryGroupController@edit']);
    Route::put('/update/{id}', ['as' => 'category_group.update', 'uses' => 'CategoryGroupController@update']);
    Route::delete('/delete/{id}', ['as' => 'category_group.destroy', 'uses' => 'CategoryGroupController@destroy']);
});