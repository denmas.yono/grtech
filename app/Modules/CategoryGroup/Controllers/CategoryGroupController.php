<?php

namespace App\Modules\CategoryGroup\Controllers;

use Models\CategoryGroup as category_group; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreCategoryGroup;
       
class CategoryGroupController extends MainController
{
    public function __construct() { 
        parent::__construct(new category_group(), 'category_group');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('CategoryGroup::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $category_groups = $this->_model::select('*');
            return datatables()->of($category_groups)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('category_group.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= hapus(['url'=> route('category_group.destroy',$row->id), 'preview'=> route('category_group.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->addColumn('parent', function($row){  
                        return $row->parent?$row->parent->name:"";
                    })->addColumn('status', function($row){  
                        return $row->status == 1 ?"Aktif":"Tidak Aktif";
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('CategoryGroup::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryGroup $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('category_group.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\CategoryGroup  $category_group
     * @return \Illuminate\Http\Response
     */
    public function show($category_group_id)
    {
        $category_group = $this->_model::find($category_group_id);
        return view('CategoryGroup::show', ['category_group'=> $category_group]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\CategoryGroup  $category_group
     * @return \Illuminate\Http\Response
     */
    public function preview($category_group_id)
    {
        $category_group = $this->_model::find($category_group_id);
        return view('CategoryGroup::preview', ['category_group'=> $category_group]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\CategoryGroup  $category_group
     * @return \Illuminate\Http\Response
     */
    public function edit($category_group_id)
    {
        $category_group = $this->_model::find($category_group_id);
        return view('CategoryGroup::edit', ['category_group'=> $category_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\CategoryGroup  $category_group
     * @return \Illuminate\Http\Response
     */
    public function update( $category_group_id)
    {  
        try {
            $category_group = $this->_model::find($category_group_id);
            if($category_group){                
                $category_group->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('category_group.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\CategoryGroup  $category_group
     * @return \Illuminate\Http\Response
     */
    public function destroy($category_group_id)
    {
        try {
            $category_group = $this->_model::find($category_group_id);
            if($category_group){
                $category_group->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('category_group.index')], 200);
    }
}
