 {{ Form::model($category_group, ['method' => 'PUT', 'route' => ['category_group.update', $category_group->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
 <div class="card-body">
     <div class="form-group">
         <label for="name">Nama Grup Kategori</label>
         {{Form::text('name', null, ['class'=> 'form-control', 'placeholder'=> "Nama Grup Kategori"])}}
     </div>  
     <div class="form-group">
        <label for="sequence">Urutan</label> 
       {{Form::select('sequence',range(1,10), $category_group->sequence, ['class'=> 'form-control', 'placeholder'=> "Urutan Grup Kategori"])}}        
    </div>  
        <label for="post_status_id">Status</label>
        {{ Form::select('status', [0=> 'Tidak Aktif', 1=> 'Aktif'], null, ['class' => 'form-control','id' => 'post_status_id','placeholder' => '--Pilih Status Publish--']) }}                    
    </div>
 </div>

 {{ Form::close() }}
