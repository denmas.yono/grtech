<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'report', 'namespace' => 'App\Modules\Report\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'report.index', 'uses' => 'ReportController@index']);
    Route::get('/getListAjax', ['as' => 'report.getListAjax', 'uses' => 'ReportController@getListAjax']);
    Route::get('/create', ['as' => 'report.create', 'uses' => 'ReportController@create']);
    Route::get('/daily', ['as' => 'report.daily', 'uses' => 'ReportController@daily']);
    Route::get('/weekly', ['as' => 'report.weekly', 'uses' => 'ReportController@weekly']);
    Route::get('/monthly', ['as' => 'report.monthly', 'uses' => 'ReportController@monthly']);
    Route::get('/yearly', ['as' => 'report.yearly', 'uses' => 'ReportController@yearly']);
    Route::get('/show/{id}', ['as' => 'report.show', 'uses' => 'ReportController@show']);    
    Route::post('/store', ['as' => 'report.store', 'uses' => 'ReportController@store']);
    Route::get('/edit/{id}', ['as' => 'report.edit', 'uses' => 'ReportController@edit']);
    Route::put('/update/{id}', ['as' => 'report.update', 'uses' => 'ReportController@update']);
    Route::delete('/delete/{id}', ['as' => 'report.destroy', 'uses' => 'ReportController@destroy']);
});