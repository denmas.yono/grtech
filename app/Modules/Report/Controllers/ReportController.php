<?php

namespace App\Modules\Report\Controllers;

use Models\Report as report; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController;  
       
class ReportController extends MainController
{
    public function __construct() { 
        parent::__construct(new report(), 'report');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Report::index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function monthly()
    { 
        return view('Report::monthly');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function yearly()
    { 
        return view('Report::yearly');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function daily()
    { 
        return view('Report::daily');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function weekly()
    { 
        return view('Report::weekly');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $roles = $this->_model::select('*');
            return datatables()->of($roles)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('region.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('region.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('region.destroy',$row->id), 'preview'=> route('region.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Report::create');
    }
 

    /**
     * Display the specified resource.
     *
     * @param  \Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show($role_id)
    {
        $report = $this->_model::find($role_id);
        return view('Report::show', ['report'=> $report]);
    }
 
     /**
     * Display the specified resource.
     *
     * @param  \Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function preview($role_id)
    {
        $report = $this->_model::find($role_id);
        return view('Report::preview', ['report'=> $report]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit($role_id)
    {
        $report = $this->_model::find($role_id);
        return view('Report::edit', ['report'=> $report]);
    }
  
}
