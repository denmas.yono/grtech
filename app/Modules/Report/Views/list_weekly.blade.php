
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Laporan</h3> 
                {!!create(route('report.create'), 'Tambah Asset', "float: right;") !!}
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="report" class="table table-bordered table-hover display" style="width: 100%">
                  <thead>
                  <tr>
                    <th>No</th> 
                    <th>Nama</th>   
                    <th data-name="action" nowrap="nowrap">Action</th>  
                  </tr>
                  </thead>
                  <tbody>
                     
                  </tbody> 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content --> 