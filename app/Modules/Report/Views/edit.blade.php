 {{ Form::model($report, ['method' => 'POST', 'route' => ['report.update', $report->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
 <div class="card-body">
     <div class="form-group">
         <label for="name">Nama</label>
         <input type="text" class="form-control" id="name" name="name" value="{{ $report->name }}" placeholder="Nama">
     </div>
 </div>
 {{ Form::close() }}
