@extends('adminlte::page') 
@section('content')
    <div id="container">
        @include('Report::list')
    </div>
    @push('js')

        <script>
            $(function() {
                loadLaporan();
            });

            function loadLaporan() {
                var selected = [];
                $("#report").DataTable({
                    dom:"<'row'<'col-sm-3'l><'col-sm-4'B><'col-sm-5'f>>rtip",
                    responsive: true,
                    scrollY: 400,
                    responsive: true, 
                    processing: true,
                    serverSide: true,  
                    select: true,
                    "rowCallback": function( row, data ) {
                        if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                            $(row).addClass('selected');
                        }
                    },
                    "ajax": "{{ route('report.getListAjax') }}",
                    "columns": [
                        // data:column name from server ,name:alias
                        {
                            data: 'DT_RowIndex',
                            name: 'id'
                        },
                        {
                            data: 'name',
                            name: 'name'
                        },
                        {
                            data: 'action',
                            name: 'action',
                            orderable: false,
                            searchable: false
                        },
                    ],
                    "buttons": ["copy", "csv", "excel", "pdf", "print"]
                }).buttons().container().appendTo('#menu_wrapper .col-md-6:eq(0)');
            }
        </script>
    @endpush
@stop
