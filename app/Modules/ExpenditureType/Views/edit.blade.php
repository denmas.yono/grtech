{{ Form::model($income_type, ['method' => 'POST', 'route' => ['income_type.update', $income_type->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}

<div class="card-body">
  <div class="form-group">
    <label for="name">Nama Kategori</label>
    <input type="text" class="form-control" id="name" name="name" value="{{$income_type->name}}" placeholder="Nama">
  </div>
</div>

{{ Form::close() }}