{{ Form::open(['method' => 'POST', 'route' => ['income_type.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Tipe Pendapatan</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"
            placeholder="Nama Pendapatan" required title="Harus diisi" minlength="3" data-minlength="minimal 3 karakter">
    </div>
</div>
{{ Form::close() }}
