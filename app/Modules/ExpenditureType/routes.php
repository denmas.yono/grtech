<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'expenditure_type', 'namespace' => 'App\Modules\ExpenditureType\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'expenditure_type.index', 'uses' => 'ExpenditureTypeController@index']);
    Route::get('/getListAjax', ['as' => 'expenditure_type.getListAjax', 'uses' => 'ExpenditureTypeController@getListAjax']);
    Route::get('/create', ['as' => 'expenditure_type.create', 'uses' => 'ExpenditureTypeController@create']);
    Route::get('/show/{id}', ['as' => 'expenditure_type.show', 'uses' => 'ExpenditureTypeController@show']);    
    Route::get('/preview/{id}', ['as' => 'expenditure_type.preview', 'uses' => 'ExpenditureTypeController@preview']);    
    Route::post('/store', ['as' => 'expenditure_type.store', 'uses' => 'ExpenditureTypeController@store']);
    Route::get('/edit/{id}', ['as' => 'expenditure_type.edit', 'uses' => 'ExpenditureTypeController@edit']);
    Route::put('/update/{id}', ['as' => 'expenditure_type.update', 'uses' => 'ExpenditureTypeController@update']);
    Route::delete('/delete/{id}', ['as' => 'expenditure_type.destroy', 'uses' => 'ExpenditureTypeController@destroy']);
});