<?php

namespace App\Modules\ExpenditureType\Controllers;

use Models\ExpenditureType as expenditure_type; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreExpenditureType;
       
class ExpenditureTypeController extends MainController
{
    public function __construct() { 
        parent::__construct(new expenditure_type(), 'expenditure_type');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('ExpenditureType::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $income_categories = $this->_model::select('*');
            return datatables()->of($income_categories)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('expenditure_type.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('expenditure_type.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('expenditure_type.destroy',$row->id), 'preview'=> route('expenditure_type.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ExpenditureType::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExpenditureType $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('expenditure_type.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\ExpenditureType  $expenditure_type
     * @return \Illuminate\Http\Response
     */
    public function show($income_id)
    {
        $expenditure_type = $this->_model::find($income_id);
        return view('ExpenditureType::show', ['expenditure_type'=> $expenditure_type]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\ExpenditureType  $expenditure_type
     * @return \Illuminate\Http\Response
     */
    public function preview($income_id)
    {
        $expenditure_type = $this->_model::find($income_id);
        return view('ExpenditureType::preview', ['expenditure_type'=> $expenditure_type]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\ExpenditureType  $expenditure_type
     * @return \Illuminate\Http\Response
     */
    public function edit($income_id)
    {
        $expenditure_type = $this->_model::find($income_id);
        return view('ExpenditureType::edit', ['expenditure_type'=> $expenditure_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\ExpenditureType  $expenditure_type
     * @return \Illuminate\Http\Response
     */
    public function update( $income_id)
    {  
        try {
            $expenditure_type = $this->_model::find($income_id);
            if($expenditure_type){                
                $expenditure_type->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('expenditure_type.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\ExpenditureType  $expenditure_type
     * @return \Illuminate\Http\Response
     */
    public function destroy($income_id)
    {
        try {
            $expenditure_type = $this->_model::find($income_id);
            if($expenditure_type){
                $expenditure_type->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('expenditure_type.index')], 200);
    }
}
