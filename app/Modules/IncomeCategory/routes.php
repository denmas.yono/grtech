<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'income_category', 'namespace' => 'App\Modules\IncomeCategory\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'income_category.index', 'uses' => 'IncomeCategoryController@index']);
    Route::get('/getListAjax', ['as' => 'income_category.getListAjax', 'uses' => 'IncomeCategoryController@getListAjax']);
    Route::get('/create', ['as' => 'income_category.create', 'uses' => 'IncomeCategoryController@create']);
    Route::get('/show/{id}', ['as' => 'income_category.show', 'uses' => 'IncomeCategoryController@show']);    
    Route::get('/preview/{id}', ['as' => 'income_category.preview', 'uses' => 'IncomeCategoryController@preview']);    
    Route::post('/store', ['as' => 'income_category.store', 'uses' => 'IncomeCategoryController@store']);
    Route::get('/edit/{id}', ['as' => 'income_category.edit', 'uses' => 'IncomeCategoryController@edit']);
    Route::put('/update/{id}', ['as' => 'income_category.update', 'uses' => 'IncomeCategoryController@update']);
    Route::delete('/delete/{id}', ['as' => 'income_category.destroy', 'uses' => 'IncomeCategoryController@destroy']);
});