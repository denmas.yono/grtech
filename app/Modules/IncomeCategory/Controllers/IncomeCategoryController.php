<?php

namespace App\Modules\IncomeCategory\Controllers;

use Models\IncomeCategory as income_category; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreIncomeCategory;
       
class IncomeCategoryController extends MainController
{
    public function __construct() { 
        parent::__construct(new income_category(), 'income_category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('IncomeCategory::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $income_categories = $this->_model::select('*');
            return datatables()->of($income_categories)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('income_category.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('income_category.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('income_category.destroy',$row->id), 'preview'=> route('income_category.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('IncomeCategory::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreIncomeCategory $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('income_category.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\IncomeCategory  $income_category
     * @return \Illuminate\Http\Response
     */
    public function show($income_id)
    {
        $income_category = $this->_model::find($income_id);
        return view('IncomeCategory::show', ['income_category'=> $income_category]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\IncomeCategory  $income_category
     * @return \Illuminate\Http\Response
     */
    public function preview($income_id)
    {
        $income_category = $this->_model::find($income_id);
        return view('IncomeCategory::preview', ['income_category'=> $income_category]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\IncomeCategory  $income_category
     * @return \Illuminate\Http\Response
     */
    public function edit($income_id)
    {
        $income_category = $this->_model::find($income_id);
        return view('IncomeCategory::edit', ['income_category'=> $income_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\IncomeCategory  $income_category
     * @return \Illuminate\Http\Response
     */
    public function update( $income_id)
    {  
        try {
            $income_category = $this->_model::find($income_id);
            if($income_category){                
                $income_category->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('income_category.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\IncomeCategory  $income_category
     * @return \Illuminate\Http\Response
     */
    public function destroy($income_id)
    {
        try {
            $income_category = $this->_model::find($income_id);
            if($income_category){
                $income_category->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('income_category.index')], 200);
    }
}
