{{ Form::model($income_category, ['method' => 'POST', 'route' => ['income_category.update', $income_category->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
  <div class="form-group">
    <label for="code">Kode</label>
    {{ Form::text('code', null, ['class' => 'form-control', 'id' => 'code', "placeholder"=> "Kode Pendapatan", "readonly"=> "readonly"]) }}    
  </div>
  <div class="form-group">
    <label for="name">Nama Kategori</label>
    {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name', "placeholder"=> "Nama Pendapatan"]) }}   
  </div> 
</div>
{{ Form::close() }}