<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'category', 'namespace' => 'App\Modules\Category\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'category.index', 'uses' => 'CategoryController@index']);
    Route::get('/getListAjax', ['as' => 'category.getListAjax', 'uses' => 'CategoryController@getListAjax']);
    Route::get('/create', ['as' => 'category.create', 'uses' => 'CategoryController@create']);
    Route::get('/show/{id}', ['as' => 'category.show', 'uses' => 'CategoryController@show']);    
    Route::get('/preview/{id}', ['as' => 'category.preview', 'uses' => 'CategoryController@preview']);    
    Route::post('/store', ['as' => 'category.store', 'uses' => 'CategoryController@store']);
    Route::get('/edit/{id}', ['as' => 'category.edit', 'uses' => 'CategoryController@edit']);
    Route::put('/update/{id}', ['as' => 'category.update', 'uses' => 'CategoryController@update']);
    Route::delete('/delete/{id}', ['as' => 'category.destroy', 'uses' => 'CategoryController@destroy']);
});