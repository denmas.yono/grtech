<?php

namespace App\Modules\Category\Controllers;

use Models\Category as category; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreCategory;
       
class CategoryController extends MainController
{
    public function __construct() { 
        parent::__construct(new category(), 'category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Category::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $group_menus = $this->_model::with(['category_group'])->select('*');
            return datatables()->of($group_menus)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('category.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= hapus(['url'=> route('category.destroy',$row->id), 'preview'=> route('category.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->addColumn('parent', function($row){  
                        return isset($row->parent)?$row->parent->name:"";
                    })
                    ->addColumn('group', function($row){  
                        return isset($row->category_group)?$row->category_group->name:"";
                    })
                    ->addColumn('status', function($row){  
                        return $row->status == 1 ?"Aktif":"Tidak Aktif";
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Category::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategory $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('category.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($category_id)
    {
        $category = $this->_model::find($category_id);
        return view('Category::show', ['category'=> $category]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function preview($category_id)
    {
        $category = $this->_model::find($category_id);
        return view('Category::preview', ['category'=> $category]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($category_id)
    {
        $category = $this->_model::find($category_id);
        return view('Category::edit', ['category'=> $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update( $category_id)
    {  
        try {
            $category = $this->_model::find($category_id);
            if($category){                
                $category->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('category.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($category_id)
    {
        try {
            $category = $this->_model::find($category_id);
            if($category){
                if(isset($category->post)){
                    return response()->json(['status'=> 'error', 'message'=> 'Data kategori tidak bisa dihapus '], 200);
                }else{
                    $category->delete(); 
                }
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('category.index')], 200);
    }
}
