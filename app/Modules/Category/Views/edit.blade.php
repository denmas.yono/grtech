 {{ Form::model($category, ['method' => 'PUT', 'route' => ['category.update', $category->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
 <div class="card-body">
     <div class="form-group">
         <label for="name">Nama Kategori</label>
         {{Form::text('name', $category->name, ['class'=> 'form-control', 'placeholder'=> "Nama Kategori"])}}
     </div> 
     <div class="form-group">
        <label for="post_status_id">Status</label>
        {{ Form::select('status', [0=> 'Tidak Aktif', 1=> 'Aktif'], null, ['class' => 'form-control','id' => 'post_status_id','placeholder' => '--Pilih Status Publish--']) }}                    
    </div>
     <div class="form-group">
        <label for="category_group_id">Grup Kategori</label>
        {{Form::select('category_group_id', \Models\CategoryGroup::pluck('name', 'id')->all(), null, ['class' => 'form-control'])}}
    </div>
    <div class="form-group">
        <label for="name">Keterangan</label> 
       {{Form::textarea('description', $category->description, ['class'=> 'form-control', 'placeholder'=> "Keterangan Kategori"])}}        
    </div> 
 </div>

 {{ Form::close() }}
