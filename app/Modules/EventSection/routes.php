<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'event_section', 'namespace' => 'App\Modules\EventSection\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'event_section.index', 'uses' => 'EventSectionController@index']);
    Route::get('/getListAjax', ['as' => 'event_section.getListAjax', 'uses' => 'EventSectionController@getListAjax']);
    Route::get('/create', ['as' => 'event_section.create', 'uses' => 'EventSectionController@create']);
    Route::get('/show/{id}', ['as' => 'event_section.show', 'uses' => 'EventSectionController@show']);    
    Route::get('/preview/{id}', ['as' => 'event_section.preview', 'uses' => 'EventSectionController@preview']);    
    Route::post('/store', ['as' => 'event_section.store', 'uses' => 'EventSectionController@store']);
    Route::get('/edit/{id}', ['as' => 'event_section.edit', 'uses' => 'EventSectionController@edit']);
    Route::put('/update/{id}', ['as' => 'event_section.update', 'uses' => 'EventSectionController@update']);
    Route::delete('/delete/{id}', ['as' => 'event_section.destroy', 'uses' => 'EventSectionController@destroy']);
});