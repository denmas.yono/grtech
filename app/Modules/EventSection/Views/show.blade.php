<br>
  <div class="card card-widget widget-event_section col-md-4" style="margin: auto;padding-top: 7.5px;">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-event_section-header bg-info">
          <h3 class="widget-event_section-event_sectionname">{{ $event_section->name }}</h3>
          <h5 class="widget-event_section-desc">Founder &amp; CEO</h5>
      </div>
      <div class="widget-event_section-image">
          <img src="{{ $event_section->photo ? $event_section->photo : asset('assets/images/company.png') }}"
              class="profile-event_section-img img-fluid img-circle img-responsive image-logo" data-title="{{ $event_section->name }}"
              onerror="this.src='{{asset('assets/images/company.png')}}">
      </div>
      <div class="card-footer">
          <div class="row">
              <div class="col-sm-12 border">
                  <div class="description-block">
                      <h5 class="description-header">3,200</h5>
                      <span class="description-text">SALES</span>
                  </div>
                  <!-- /.description-block -->
              </div>
              <!-- /.col -->
          </div>
          <!-- /.row -->
      </div>
  </div> 