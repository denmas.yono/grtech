<?php

namespace App\Modules\EventSection\Controllers;

use Models\EventSection as event_section; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreEventSection;
       
class EventSectionController extends MainController
{
    public function __construct() { 
        parent::__construct(new event_section(), 'event_section');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('EventSection::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $event_sections = $this->_model::select('*');
            
            return datatables()->of($event_sections)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('event_section.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('event_section.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('event_section.destroy',$row->id), 'preview'=> route('event_section.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('EventSection::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventSection $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('event_section.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventSection  $event_section
     * @return \Illuminate\Http\Response
     */
    public function show($event_section_id)
    {
        $event_section = $this->_model::find($event_section_id);
        return view('EventSection::show', ['event_section'=> $event_section]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventSection  $event_section
     * @return \Illuminate\Http\Response
     */
    public function preview($event_section_id)
    {
        $event_section = $this->_model::find($event_section_id);
        return view('EventSection::preview', ['event_section'=> $event_section]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\EventSection  $event_section
     * @return \Illuminate\Http\Response
     */
    public function edit($event_section_id)
    {
        $event_section = $this->_model::find($event_section_id);
        return view('EventSection::edit', ['event_section'=> $event_section]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\EventSection  $event_section
     * @return \Illuminate\Http\Response
     */
    public function update( $event_section_id)
    {  
        try {
            $event_section = $this->_model::find($event_section_id);
            if($event_section){                
                $event_section->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('event_section.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\EventSection  $event_section
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_section_id)
    {
        try {
            $event_section = $this->_model::find($event_section_id);
            if($event_section){
                $event_section->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('event_section.index')], 200);
    }
}
