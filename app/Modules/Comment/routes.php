<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'comment', 'namespace' => 'App\Modules\Comment\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'comment.index', 'uses' => 'CommentController@index']);
    Route::get('/getListAjax', ['as' => 'comment.getListAjax', 'uses' => 'CommentController@getListAjax']);
    Route::get('/create', ['as' => 'comment.create', 'uses' => 'CommentController@create']);
    Route::get('/show/{id}', ['as' => 'comment.show', 'uses' => 'CommentController@show']);    
    Route::get('/preview/{id}', ['as' => 'comment.preview', 'uses' => 'CommentController@preview']);    
    Route::post('/store', ['as' => 'comment.store', 'uses' => 'CommentController@store']);
    Route::get('/edit/{id}', ['as' => 'comment.edit', 'uses' => 'CommentController@edit']);
    Route::put('/update/{id}', ['as' => 'comment.update', 'uses' => 'CommentController@update']);
    Route::delete('/delete/{id}', ['as' => 'comment.destroy', 'uses' => 'CommentController@destroy']);
});