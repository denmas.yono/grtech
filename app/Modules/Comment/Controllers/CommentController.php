<?php

namespace App\Modules\Comment\Controllers;

use Models\Comment as comment; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreComment;
       
class CommentController extends MainController
{
    public function __construct() { 
        parent::__construct(new comment(), 'comment');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Comment::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $group_menus = $this->_model::select('*');
            return datatables()->of($group_menus)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('comment.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= hapus(['url'=> route('comment.destroy',$row->id), 'preview'=> route('comment.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->addColumn('parent', function($row){  
                        return $row->parent?$row->parent->name:"";
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Comment::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComment $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('comment.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show($group_menu_id)
    {
        $comment = $this->_model::find($group_menu_id);
        return view('Comment::show', ['comment'=> $comment]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit($group_menu_id)
    {
        $comment = $this->_model::find($group_menu_id);
        return view('Comment::edit', ['comment'=> $comment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update( $group_menu_id)
    {  
        try {
            $comment = $this->_model::find($group_menu_id);
            if($comment){                
                $comment->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('comment.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($group_menu_id)
    {
        try {
            $comment = $this->_model::find($group_menu_id);
            if($comment){
                $comment->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('comment.index')], 200);
    }
}
