@extends('layouts.app')

@section('content')
<br>
  <div class="card card-widget widget-comment col-md-4" style="margin: auto;padding-top: 7.5px;">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-comment-header bg-info">
          <h3 class="widget-comment-group_menuname">{{ $comment->name }}</h3>
          <h5 class="widget-comment-desc">Founder &amp; CEO</h5>
      </div>
      <div class="widget-comment-image">
          <img src="{{ $comment->photo ? $comment->photo : asset('assets/images/comment.png') }}"
              class="profile-comment-img img-fluid img-circle img-responsive image-logo" data-title="{{ $comment->name }}"
              onerror="this.src='../../dist/img/group_menu2-160x160.jpg'">
      </div>
      <div class="card-footer">
          <div class="row">
              <div class="col-sm-12 border">
                  <div class="description-block">
                      <h5 class="description-header">3,200</h5>
                      <span class="description-text">SALES</span>
                  </div>
                  <!-- /.description-block -->
              </div>
              <!-- /.col -->
          </div>
          <!-- /.row -->
      </div>
  </div> 
@endsection
