{{ Form::open(['method' => 'POST', 'route' => ['class_level.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body"> 
    <div class="form-group">
        <label for="name">Level Kelas</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
    </div> 
</div>
<!-- /.card-body -->
{{ Form::close() }}
@push('js')
    <script>
        $(function() {
            $('#reservationdatetime').datetimepicker({
                icons: {
                    time: 'far fa-clock'
                }
            });
        })
    </script>
@endpush
