{{ Form::model($class_level, ['method' => 'POST', 'route' => ['class_level.update', $class_level->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Level Kelas</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
    </div> 
</div>
{{ Form::close() }}
