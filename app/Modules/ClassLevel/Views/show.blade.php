@extends('layouts.app')

@section('content')
<br>
  <div class="card card-widget widget-class_level col-md-4" style="margin: auto;padding-top: 7.5px;">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-class_level-header bg-info">
          <h3 class="widget-class_level-eventname">{{ $class_level->name }}</h3>
          <h5 class="widget-class_level-desc">Founder &amp; CEO</h5>
      </div>
      <div class="widget-class_level-image">
          <img src="{{ $class_level->photo ? $class_level->photo : class_level('events/images/class_level.png') }}"
              class="profile-class_level-img img-fluid img-circle img-responsive image-logo" data-title="{{ $class_level->name }}"
              onerror="this.src='../../dist/img/event2-160x160.jpg'">
      </div>
      <div class="card-footer">
          <div class="row">
              <div class="col-sm-12 border">
                  <div class="description-block">
                      <h5 class="description-header">3,200</h5>
                      <span class="description-text">SALES</span>
                  </div>
                  <!-- /.description-block -->
              </div>
              <!-- /.col -->
          </div>
          <!-- /.row -->
      </div>
  </div> 
@endsection
