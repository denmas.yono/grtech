<?php

namespace App\Modules\ClassLevel\Controllers;

use Models\ClassLevel as class_level; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreClassLevel;
       
class ClassLevelController extends MainController
{
    public function __construct() { 
        parent::__construct(new class_level(), 'class_level');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('ClassLevel::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $class_levels = $this->_model::select('*');
             
            return datatables()->of($class_levels)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('class_level.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= hapus(['url'=> route('class_level.destroy',$row->id), 'preview'=> route('class_level.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        return view('ClassLevel::create');  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClassLevel $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('class_level.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\ClassLevel  $class_level
     * @return \Illuminate\Http\Response
     */
    public function show($class_level_id)
    {
        $class_level = $this->_model::find($class_level_id);
        return view('ClassLevel::show', ['class_level'=> $class_level]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\ClassLevel  $class_level
     * @return \Illuminate\Http\Response
     */
    public function preview($class_level_id)
    {
        $class_level = $this->_model::find($class_level_id);
        return view('ClassLevel::preview', ['class_level'=> $class_level]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\ClassLevel  $class_level
     * @return \Illuminate\Http\Response
     */
    public function edit($class_level_id)
    {
        $class_level = $this->_model::find($class_level_id);
        return view('ClassLevel::edit', ['class_level'=> $class_level]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\ClassLevel  $class_level
     * @return \Illuminate\Http\Response
     */
    public function update( $class_level_id)
    {  
        try {
            $class_level = $this->_model::find($class_level_id);
            if($class_level){                
                $class_level->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('class_level.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\ClassLevel  $class_level
     * @return \Illuminate\Http\Response
     */
    public function destroy($class_level_id)
    {
        try {
            $class_level = $this->_model::find($class_level_id);
            if($class_level){
                $class_level->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('class_level.index')], 200);
    }
}
