
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Jadwal</h3> 
                {!! create(['url'=> route('schedule.create'),'title'=> 'Tambah Jadwal', 'style'=> "float: right;"])!!}
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="schedule" class="table table-bordered table-hover display standard" data-route="{{route('schedule.getListAjax')}}">
                  <thead>
                  <tr>
                    <th data-name="id">No</th> 
                    <th data-name="name">Nama</th>
                    <th data-name="class_level.name">Kelas Level</th>
                    <th data-name="day">Hari</th>  
                    <th data-name="hour">Jam</th>
                    <th data-name="teacher.name">Ustadz / Ustadzah</th>
                    <th data-name="subject.name">Materi</th>
                    <th data-name="action">Action</th> 
                  </tr>
                  </thead>
                  <tbody>
                     
                  </tbody> 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content --> 