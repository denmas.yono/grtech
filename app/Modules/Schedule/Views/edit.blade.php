 {{ Form::model($schedule, ['method' => 'POST', 'route' => ['schedule.update', $schedule->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
 <div class="card-body">
    <div class="form-group">
        <label for="name">Nama Jadwal</label>
        {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
    </div>
    <div class="form-group">
       <label for="class_level_id">Kelas Level</label>
       {{ Form::select('class_level_id',\Models\ClassLevel::pluck('name', 'id')->all(), null, ['class' => 'form-control selectpicker', 'id' => 'class_level_id', "placeholder"=>"--Pilih Kelas--"]) }}
   </div>
   <div class="form-group">
    <label for="class_level_id">Hari</label>
    {{ Form::select('day',['Ahad'=>'Ahad', 'Senin'=>'Senin','Selasa'=>'Selasa', 'Rabu'=>'Rabu', 'Kamis'=>'Kamis', 'Jumat'=>'Jumat', 'Sabtu'=>'Sabtu'], null, ['class' => 'form-control selectpicker', 'id' => 'class_level_id', "placeholder"=>"--Pilih Kelas--"]) }}
</div>
   <div class="form-group">
       <label for="hour">Jam</label>
       {{ Form::time('hour', null, ['class' => 'form-control', 'id' => 'hour']) }}
   </div>
    <div class="form-group">
        <label for="teacher">Ustadz</label>
        {{Form::select('teacher_id', \Models\Teacher::pluck('name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "teacher_id", "placeholder"=>"--Pilih Ustadz--"])}}                  
    </div>
    <div class="form-group">
       <label for="subject">Materi</label>
       {{Form::select('subject_id', \Models\Subject::pluck('name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "subject_id", "placeholder"=>"--Pilih Materi--"])}}                  
   </div>
</div>

 {{ Form::close() }}
