<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'schedule', 'namespace' => 'App\Modules\Schedule\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'schedule.index', 'uses' => 'ScheduleController@index']);
    Route::get('/getListAjax', ['as' => 'schedule.getListAjax', 'uses' => 'ScheduleController@getListAjax']);
    Route::get('/create', ['as' => 'schedule.create', 'uses' => 'ScheduleController@create']);
    Route::get('/show/{id}', ['as' => 'schedule.show', 'uses' => 'ScheduleController@show']);    
    Route::get('/preview/{id}', ['as' => 'schedule.preview', 'uses' => 'ScheduleController@preview']);    
    Route::post('/store', ['as' => 'schedule.store', 'uses' => 'ScheduleController@store']);
    Route::get('/edit/{id}', ['as' => 'schedule.edit', 'uses' => 'ScheduleController@edit']);
    Route::put('/update/{id}', ['as' => 'schedule.update', 'uses' => 'ScheduleController@update']);
    Route::delete('/delete/{id}', ['as' => 'schedule.destroy', 'uses' => 'ScheduleController@destroy']);
});