<?php

namespace App\Modules\Schedule\Controllers;

use Models\Schedule as schedule; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreSchedule;
       
class ScheduleController extends MainController
{
    public function __construct() { 
        parent::__construct(new schedule(), 'schedule');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Schedule::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $group_menus = $this->_model::with(['class_level','teacher','subject'])->select('*');
            return datatables()->of($group_menus)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('schedule.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= hapus(['url'=> route('schedule.destroy',$row->id), 'preview'=> route('schedule.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })   
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Schedule::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSchedule $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('schedule.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show($group_menu_id)
    {
        $schedule = $this->_model::find($group_menu_id);
        return view('Schedule::show', ['schedule'=> $schedule]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit($group_menu_id)
    {
        $schedule = $this->_model::find($group_menu_id);
        return view('Schedule::edit', ['schedule'=> $schedule]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update( $group_menu_id)
    {  
        try {
            $schedule = $this->_model::find($group_menu_id);
            if($schedule){                
                $schedule->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('schedule.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy($group_menu_id)
    {
        try {
            $schedule = $this->_model::find($group_menu_id);
            if($schedule){
                $schedule->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('schedule.index')], 200);
    }
}
