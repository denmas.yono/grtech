<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'gallery', 'namespace' => 'App\Modules\Gallery\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'gallery.index', 'uses' => 'GalleryController@index']);
    Route::get('/getListAjax', ['as' => 'gallery.getListAjax', 'uses' => 'GalleryController@getListAjax']);
    Route::get('/create', ['as' => 'gallery.create', 'uses' => 'GalleryController@create']);
    Route::get('/show/{id}', ['as' => 'gallery.show', 'uses' => 'GalleryController@show']);    
    Route::get('/preview/{id}', ['as' => 'gallery.preview', 'uses' => 'GalleryController@preview']);    
    Route::post('/store', ['as' => 'gallery.store', 'uses' => 'GalleryController@store']);
    Route::get('/edit/{id}', ['as' => 'gallery.edit', 'uses' => 'GalleryController@edit']);
    Route::put('/update/{id}', ['as' => 'gallery.update', 'uses' => 'GalleryController@update']);
    Route::delete('/delete/{id}', ['as' => 'gallery.destroy', 'uses' => 'GalleryController@destroy']);
   
});