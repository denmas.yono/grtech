<?php

namespace App\Modules\Gallery\Controllers;

use Models\Gallery as gallery; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 
use Lib\File;  

use App\Http\Requests\StoreGallery;
       
class GalleryController extends MainController
{
    public function __construct() { 
        parent::__construct(new gallery(), 'gallery');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Gallery::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $galleries = $this->_model::with(['post_status','album'])->select(['*']);
            return datatables()->of($galleries)
                    ->addIndexColumn() 
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('gallery.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('gallery.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('gallery.destroy',$row->id), 'preview'=> route('gallery.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn; 
                        }
                    })->addColumn('status', function($row){ 
                        if($row && $row->post_status){
                            return '<span style="background-color:'.($row->post_status?$row->post_status->bg_color:"red").'; padding:5px">'.$row->post_status?$row->post_status->name:"Draft".'</span>';
                        }else{
                            return '<span style="padding:5px">Draft</span>';
                        }
                    })->addColumn('image', function($row){ 
                        if($row){
                            if($row->image && file_exists(storage_path('app/public/gallery/'.$row->image))){
                                return '<img class="img-responsive image" style="padding:5px" src="'.asset('galleries/'.$row->image).'" onerror="this.src='.asset('assets/images/mushola.png').'" width="100px" height="100px">';
                            }
                            return '<img class="img-responsive image" style="padding:5px" src="'.asset('assets/images/mushola.png').'"  width="100px" height="100px">';
                        }
                    })->addColumn('album', function($row){ 
                        if($row){
                            return $row->album?$row->album->title:"NA";
                        }
                    })
                    ->rawColumns(['action','content', 'status','image'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $album_id = request()->input('album_id');
        return view('Gallery::create',['album_id'=> $album_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreGallery $request)
    {   
        try { 
            $gallery = $this->_model::create($this->_serialize($request)); 
            if (request()->file('image')) {                
                $this->validate($request, [ 
                    'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]); 
                
                if (request()->file('image')->isValid()) {
                    $file = request()->file('image');
                    // image upload in storage/app/public/image   
                    $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/gallery')));
                    if($gallery->image && file_exists(storage_path('app/public/gallery/'.$gallery->image))){
                        unlink(storage_path('app/public/gallery/'.$gallery->image));
                    }
                    $gallery->image = is_object($info)?$info->getFilename():$info; 
                    $gallery->save();
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                }
            } 
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('gallery.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show($gallery_id)
    {
        $gallery = $this->_model::find($gallery_id);
        return view('Gallery::show', ['gallery'=> $gallery]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function preview($gallery_id)
    {
        $gallery = $this->_model::find($gallery_id);
        return view('Gallery::preview', ['gallery'=> $gallery]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit($gallery_id)
    {
        $gallery = $this->_model::find($gallery_id);
        return view('Gallery::edit', ['gallery'=> $gallery]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update( $gallery_id)
    {  
        try {
            $gallery = $this->_model::find($gallery_id);
            if($gallery){     
                           
                $gallery->update($this->_serialize(request())); 
                if (request()->file('image')) {                
                    $this->validate(request(), [ 
                        'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]); 
                    
                    if (request()->file('image')->isValid()) {
                        $file = request()->file('image');
                        // image upload in storage/app/public/image   
                        $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/gallery')));
                        if($gallery->image && file_exists(storage_path('app/public/gallery/'.$gallery->image))){
                            unlink(storage_path('app/public/gallery/'.$gallery->image));
                        }
                        $gallery->image = is_object($info)?$info->getFilename():$info; 
                        $gallery->save();
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                    }
                } 
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('gallery.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy($gallery_id)
    {
        try {
            $gallery = $this->_model::find($gallery_id);
            if($gallery){
                $gallery->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('gallery.index')], 200);
    } 
}
