{{ Form::open(['method' => 'POST','route' => ['gallery.store'],'class' => 'form-horizontal inline','enctype' => 'multipart/form-data']) }}
    <div class="card-body"> 
        <div class="form-group">
            <label for="title">Title</label>
            {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Judul']) }}
        </div> 
        <div class="form-group">
            <label for="post_status_id">Status</label>
            {{ Form::select('post_status_id', \Models\PostStatus::pluck('name','id')->all(), null, ['class' => 'form-control','id' => 'post_status_id','placeholder' => '--Pilih Status Publish--']) }}                    
        </div>
        <div class="form-group">
            <label for="publish_date">Tanggal Publish</label>
            {{ Form::date('publish_date', null, ['class' => 'form-control datepicker','id' => 'publish_date','placeholder' => 'Tanggal Publish', 'data-language'=> 'en', 'data-multiple-tables'=> 3, 'data-multiple-tables-separator'=> ',', 'data-position'=> 'top left' ]) }}
        </div>  
        <div class="form-group">
            <label for="image">Gambar</label><br>
            {{ Form::file('image', null, ['class' => 'form-control','id' => 'image','placeholder' => 'Upload Gambar']) }}
        </div>
        @if(isset($album_id) && $album_id)
        {{ Form::hidden('album_id', $album_id) }}
        @else 
        <div class="form-group">
            <label for="album_id">Album</label>
            {{ Form::select('album_id', \Models\Album::pluck('title','id')->all(), null, ['class' => 'form-control','id' => 'album_id','placeholder' => '--Pilih Album--']) }}                    
        </div>
        @endif 
        <div class="form-group">
            <label for="template_id">Template</label>
            {{ Form::select('template_id', \Models\Template::pluck('name','id')->all(), null, ['class' => 'form-control','id' => 'template_id','placeholder' => '--Pilih Template--']) }}                    
        </div>
    </div>
    
    {{ Form::close() }}