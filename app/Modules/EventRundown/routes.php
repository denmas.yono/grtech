<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'event_rundown', 'namespace' => 'App\Modules\EventRundown\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'event_rundown.index', 'uses' => 'EventRundownController@index']);
    Route::get('/getListAjax', ['as' => 'event_rundown.getListAjax', 'uses' => 'EventRundownController@getListAjax']);
    Route::get('/create', ['as' => 'event_rundown.create', 'uses' => 'EventRundownController@create']);
    Route::get('/show/{id}', ['as' => 'event_rundown.show', 'uses' => 'EventRundownController@show']);    
    Route::get('/preview/{id}', ['as' => 'event_rundown.preview', 'uses' => 'EventRundownController@preview']);    
    Route::post('/store', ['as' => 'event_rundown.store', 'uses' => 'EventRundownController@store']);
    Route::get('/edit/{id}', ['as' => 'event_rundown.edit', 'uses' => 'EventRundownController@edit']);
    Route::put('/update/{id}', ['as' => 'event_rundown.update', 'uses' => 'EventRundownController@update']);
    Route::delete('/delete/{id}', ['as' => 'event_rundown.destroy', 'uses' => 'EventRundownController@destroy']);
});