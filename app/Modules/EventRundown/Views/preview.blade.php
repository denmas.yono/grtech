<div class="card-body"> 
    <div class="form-group">
        <label for="name">Nama Kegiatan</label>
        {{$event->name}}
    </div>
    <div class="form-group">
      <label for="name">Nama Acara</label> 
      {{$event_rundown->name}}
    </div>
    <div class="form-group">
      <label for="start_date">Mulai</label> 
      {{$event_rundown->date_start}}
    </div> 
    <div class="form-group">
      <label for="end_date">Berakhir</label> 
      {{$event_rundown->date_end}}
    </div>
    <div class="form-group">
      <label for="by_name">Nama Pengisi Acara</label> 
      {{$event_rundown->by_name}}
    </div>
    <div class="form-group">
      <label for="description">Keterangan</label>
      {{$event_rundown->description}}
    </div>
  </div>
  <!-- /.card-body -->