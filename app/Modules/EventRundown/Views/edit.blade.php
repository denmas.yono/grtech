{{ Form::model($event_rundown, ['method' => 'POST', 'route' => ['event_rundown.update', $event_rundown->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body"> 
  <div class="form-group">
      <label for="name">Nama Kegiatan</label>
      @if($event)
        {{$event->name}}
        {{Form::hidden('event_id', $event->id)}}
      @else 
      {{Form::select('event_id', \Models\Event::pluck('name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "type", "data-live-search"=> "true", 'data-style'=> 'btn-success'])}}
      @endif
  </div>
  <div class="form-group">
    <label for="name">Urutan Ke - </label> 
    {{Form::select('sequence',range(0, 30), null, ['class'=> 'form-control', 'required'])}}
  </div>
  <div class="form-group">
    <label for="name">Nama Acara</label> 
    {{Form::text('name', null, ['class'=> 'form-control ucword', 'required'])}}
  </div>
  <div class="form-group">
    <label for="start_date">Mulai</label> 
    {{Form::date('date_start', null, ['class'=> 'form-control','date-format'=> 'Y-m-d H:i:s'])}}
  </div> 
  <div class="form-group">
    <label for="end_date">Berakhir</label> 
    {{Form::date('date_end', null, ['class'=> 'form-control','date-format'=> 'Y-m-d H:i:s'])}}
  </div>
  <div class="form-group">
    <label for="by_name">Nama Pengisi Acara</label> 
    {{Form::text('by_name', null, ['class'=> 'form-control ucword', 'required'])}}
  </div>
  <div class="form-group">
    <label for="description">Keterangan</label>
    {{Form::textarea('description', null, ["class"=> "form-control", 'rows'=> 3, "placeholder"=> "Isi Keterangan Keperluan"])}}
  </div>
</div>
<!-- /.card-body -->
{{ Form::close() }}
