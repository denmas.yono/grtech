
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Kegiatan</h3> 
                {!!create(['url'=> route('event_rundown.create'), 'title'=> 'Tambah Biaya Kegiatan', 'style'=> "float: right;"]) !!}
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="event_rundown" class="table table-bordered table-hover display standard" style="width: 100%" data-route="{{ route('event_rundown.getListAjax') }}">
                  <thead>
                  <tr>
                    <th data-name="id">No</th> 
                    <th data-name="sequence">Urutan Acara</th>  
                    <th data-name="name">Nama Acara</th>  
                    <th data-name="event_type.name">Tipe Kegiatan</th>  
                    <th data-name="date_start">Mulai</th>
                    <th data-name="date_end">Berakhir</th>
                    <th data-name="description">Keterangan</th> 
                    <th data-name="action">Action</th> 
                  </tr>
                  </thead>
                  <tbody>
                     
                  </tbody> 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content --> 