<?php

namespace App\Modules\EventRundown\Controllers;

use Models\EventRundown as EventRundown; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreEventRundown;
       
class EventRundownController extends MainController
{
    public function __construct() { 
        parent::__construct(new EventRundown(), 'EventRundown');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('EventRundown::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $event_rundowns = $this->_model::with(['event'])->select('*');
            if( request()->has('event_id')){
                $event_rundowns->where('event_id', request()->input('event_id'));
            }
            return datatables()->of($event_rundowns)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('event_rundown.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= preview(['url'=> route('event_rundown.preview',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('event_rundown.destroy',$row->id), 'preview'=> route('event_rundown.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $event = null;
        if(request()->has('event_id')){
            $event = \Models\Event::find(request()->input('event_id'));
        }
        return view('EventRundown::create', ['event'=> $event]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventRundown $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.'], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventRundown  $EventRundown
     * @return \Illuminate\Http\Response
     */
    public function show($event_rundown_id)
    {
        $event_rundown = $this->_model::find($event_rundown_id);
        return view('EventRundown::show', ['event_rundown'=> $event_rundown]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventRundown  $EventRundown
     * @return \Illuminate\Http\Response
     */
    public function preview($event_rundown_id)
    {
        $event_rundown = $this->_model::find($event_rundown_id);
        $event = $event_rundown->event;
        return view('EventRundown::preview', ['event'=> $event, 'event_rundown'=> $event_rundown]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\EventRundown  $EventRundown
     * @return \Illuminate\Http\Response
     */
    public function edit($event_rundown_id)
    {
        $event_rundown = $this->_model::find($event_rundown_id);
        $event = $event_rundown->event;
        return view('EventRundown::edit', ['event_rundown'=> $event_rundown, 'event'=> $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\EventRundown  $EventRundown
     * @return \Illuminate\Http\Response
     */
    public function update( $event_rundown_id)
    {  
        try {
            $EventRundown = $this->_model::find($event_rundown_id);
            if($EventRundown){                
                $EventRundown->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\EventRundown  $EventRundown
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_rundown_id)
    {
        try {
            $EventRundown = $this->_model::find($event_rundown_id);
            if($EventRundown){
                $EventRundown->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('event_rundown.index')], 200);
    }
}
