 {{ Form::model($menu_type, ['method' => 'PUT', 'route' => ['menu_type.update', $menu_type->id], 'class' => 'form-horizontal']) }}
 <div class="card-body">
     <div class="form-group">
         <label for="name">Nama</label>
         <input type="text" class="form-control" id="name" name="name" value="{{ $menu_type->name }}"
             placeholder="Nama">
     </div>
 </div>
 <!-- /.card-body -->
 {{ Form::close() }}
