 
  {{ Form::open(['method' => 'POST','route'=> ['menu_type.store'], 'class'=> 'form-horizontal']) }}
<div class="card-body"> 
  <div class="form-group">
    <label for="name">Nama</label>
    <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}" placeholder="Nama" required title="Nama Harus diisi" range minlength="3" maxlength="30">
  </div>   
</div>
<!-- /.card-body --> 
{{Form::close()}}