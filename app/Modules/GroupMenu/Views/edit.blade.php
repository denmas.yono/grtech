 {{ Form::model($group_menu, ['method' => 'POST', 'route' => ['group_menu.update', $group_menu->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
 <div class="card-body">
     <div class="form-group">
         <label for="name">Nama Grup Menu</label>
         <input type="text" class="form-control" id="name" name="name" value="{{ $group_menu->name }}"
             placeholder="Nama Grup Menu">
     </div>
     <div class="form-group"> 
         {{Form::select('group_menu_id', \Models\GroupMenu::pluck('name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "group_menu_id", "placeholder"=>"--Pilih Parent Menu--"])}}                  
     </div>
 </div>

 {{ Form::close() }}
