<?php

namespace App\Modules\Teacher\Controllers;

use Models\Teacher as Teacher; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController;  
use App\Http\Requests\StoreTeacher; 

       
class TeacherController extends MainController
{
    public function __construct() { 
        parent::__construct(new Teacher(), 'teacher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     
        return view('Teacher::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $teachers = $this->_model::select('*');
            return datatables()->of($teachers)
                    ->addIndexColumn() 
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('teacher.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('teacher.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('teacher.destroy',$row->id), 'preview'=> route('teacher.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })
                    ->addColumn('mosque', function($row){ 
                        if($row->mosque_id){
                            $link = '<a href data-href="'.(route('people.show_mosque',$row->mosque_id)).'" class="show_popup" data-toggle="modal" data-target="#modalPopupDetail" data-title="'.(isset($row->mosque)?$row->mosque->name:"Noname").'">'.(isset($row->mosque)?$row->mosque->name:"Noname").'</a>';
                            return $link;
                        }
                        return null;
                    }) 
                    ->addColumn('mushola', function($row){ 
                        if($row->mushola_id){
                            $link = '<a href data-href="'.(route('people.show_mushola',$row->mushola_id)).'" class="show_popup" data-toggle="modal" data-target="#modalPopupDetail" data-title="'.(isset($row->mosque)?$row->mushola->name:"Noname").'">'.(isset($row->mushola)?$row->mushola->name:"Noname").'</a>';
                            return $link;
                        }
                        return null;
                    }) 
                    ->addColumn('subject', function($row){ 
                        if($row->subjects->count() > 0){
                            $html = "<ul class='subject'>";
                            foreach($row->subjects as $subject){ 
                                $html .= "<li>". $subject->name."</li>";
                            }
                            $html .= "</ul>";
                            return $html;
                        }
                        return null;
                    }) 
                    ->rawColumns(['action','mosque','mushola','subject'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Teacher::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeacher $request)
    {   
        try { 
            $teacher = $this->_model::create($this->_serialize($request));   
            if(request()->has('subject_id')){
                $subjects = \Models\Subject::find(request()->input('subject_id'));
                $teacher->subjects()->attach($subjects);
            } 
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('teacher.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = $this->_model::find($id); 
        return view('Teacher::show', ['teacher'=> $teacher]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function preview($id)
    {
        $teacher = $this->_model::find($id); 
        return view('Teacher::preview', ['teacher'=> $teacher]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show_mosque($mosque_id)
    {   $mosque = \Models\Mosque::find($mosque_id);
        return view('Teacher::show_mosque', ['mosque'=> $mosque]);
    }

     /**
     * Display the specified resource.
     *
     * @param  \Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show_mushola($mushola_id)
    {   $mushola = \Models\Mushola::find($mushola_id);
        return view('Teacher::show_mushola', ['mushola'=> $mushola]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit($teacher_id)
    {
        $teacher = $this->_model::find($teacher_id);
        return view('Teacher::edit', ['teacher'=> $teacher]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update( $teacher_id)
    {  
        try {
            $teacher = $this->_model::find($teacher_id);
            if($teacher){                
                $teacher->update($this->_serialize(request()));
                if(request()->has('subject_id')){
                    $teacher->subjects()->detach();
                    $subjects = \Models\Subject::find(request()->input('subject_id'));
                    $teacher->subjects()->attach($subjects);
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('teacher.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy($teacher_id)
    {
        try {
            $teacher = $this->_model::find($teacher_id);
            if($teacher){
                $teacher->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('teacher.index')], 200);
    }
}
