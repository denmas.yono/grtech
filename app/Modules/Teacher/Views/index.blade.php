@extends('adminlte::page')
 
@push('css')
<style>
    ul.subject > li {
    background-color: #f0ecc6;
    border: 1px #e4d7d7 solid;
    margin: 2px;
    border-radius: 5px;
    padding: 1px 3px;
    color: saddlebrown;
    font-size: smaller;
    font-weight: bold;
}
    </style>
    
@endpush
@section('content')
<div id="container">
      @include('Teacher::list')
  </div> 
  @stop
  
