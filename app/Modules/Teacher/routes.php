<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'teacher', 'namespace' => 'App\Modules\Teacher\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'teacher.index', 'uses' => 'TeacherController@index']);
    Route::get('/getListAjax', ['as' => 'teacher.getListAjax', 'uses' => 'TeacherController@getListAjax']);
    Route::get('/create', ['as' => 'teacher.create', 'uses' => 'TeacherController@create']);
    Route::get('/show/{id}', ['as' => 'teacher.show', 'uses' => 'TeacherController@show']);    
    Route::get('/preview/{id}', ['as' => 'teacher.preview', 'uses' => 'TeacherController@preview']);    
    Route::get('/show_mosque/{id}', ['as' => 'teacher.show_mosque', 'uses' => 'TeacherController@show_mosque']);    
    Route::get('/show_mushola/{id}', ['as' => 'teacher.show_mushola', 'uses' => 'TeacherController@show_mushola']);    
    Route::post('/store', ['as' => 'teacher.store', 'uses' => 'TeacherController@store']);
    Route::get('/edit/{id}', ['as' => 'teacher.edit', 'uses' => 'TeacherController@edit']);
    Route::put('/update/{id}', ['as' => 'teacher.update', 'uses' => 'TeacherController@update']);
    Route::delete('/delete/{id}', ['as' => 'teacher.destroy', 'uses' => 'TeacherController@destroy']);
});