<?php

namespace App\Modules\Student\Controllers;

use Models\Student as Student; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController;  
use App\Http\Requests\StoreStudent; 

       
class StudentController extends MainController
{
    public function __construct() { 
        parent::__construct(new Student(), 'student');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {     
        return view('Student::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $students = $this->_model::with(['mosque','mushola'])->select('*');
            return datatables()->of($students)
                    ->addIndexColumn() 
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('student.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('student.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('student.destroy',$row->id), 'preview'=> route('student.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })
                    ->addColumn('mosque', function($row){ 
                        if($row->mosque_id){
                            $link = '<a href data-href="'.(route('people.show_mosque',$row->mosque_id)).'" class="show_popup" data-toggle="modal" data-target="#modalPopupDetail" data-title="'.(isset($row->mosque)?$row->mosque->name:"Noname").'">'.(isset($row->mosque)?$row->mosque->name:"Noname").'</a>';
                            return $link;
                        }
                        return null;
                    }) 
                    ->addColumn('mushola', function($row){ 
                        if($row->mushola_id){
                            $link = '<a href data-href="'.(route('people.show_mushola',$row->mushola_id)).'" class="show_popup" data-toggle="modal" data-target="#modalPopupDetail" data-title="'.(isset($row->mosque)?$row->mushola->name:"Noname").'">'.(isset($row->mushola)?$row->mushola->name:"Noname").'</a>';
                            return $link;
                        }
                        return null;
                    }) ->filterColumn('full_name', function ($row, $search) {
                        return $row->where("first_name", 'Like', "%{$search}%")->orWhere("last_name", 'Like', "%{$search}%"); 
                    })
                    ->rawColumns(['action','mosque','mushola'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Student::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudent $request)
    {   
        try { 
            $student = $this->_model::create($this->_serialize($request));   

            // Notification::route('mail', 'taylor@example.com')
            // ->route('nexmo', '5555555555')
            // ->route('slack', 'https://hooks.slack.com/services/...')
            // ->notify(new MosqueCreated($this->_model));

        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('student.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = $this->_model::find($id); 
        return view('Student::show', ['student'=> $student]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function preview($id)
    {
        $student = $this->_model::find($id); 
        return view('Student::preview', ['student'=> $student]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show_mosque($mosque_id)
    {   $mosque = \Models\Mosque::find($mosque_id);
        return view('Student::show_mosque', ['mosque'=> $mosque]);
    }

     /**
     * Display the specified resource.
     *
     * @param  \Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show_mushola($mushola_id)
    {   $mushola = \Models\Mushola::find($mushola_id);
        return view('Student::show_mushola', ['mushola'=> $mushola]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit($student_id)
    {
        $student = $this->_model::find($student_id);
        return view('Student::edit', ['student'=> $student]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update( $student_id)
    {  
        try {
            $student = $this->_model::find($student_id);
            if($student){                
                $student->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('student.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy($student_id)
    {
        try {
            $student = $this->_model::find($student_id);
            if($student){
                $student->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('student.index')], 200);
    }
}
