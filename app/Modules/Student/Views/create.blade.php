
{{ Form::open(['method' => 'POST','route'=> ['student.store'], 'class'=> 'form-horizontal', 'enctype'=> 'multipart/form-data']) }}
<div class="card-body">
  <div class="form-group">
    <label for="first_name">Nama Depan</label> 
    {{ Form::text('first_name', old('first_name'), ['class' => 'form-control', 'id' => 'first_name',"placeholder"=>"Nama Depan"]) }}
  </div>
  <div class="form-group">
    <label for="last_name">Nama Belakang</label>
    {{ Form::text('last_name', old('last_name'), ['class' => 'form-control', 'id' => 'last_name',"placeholder"=>"Nama Belakang"]) }}
  </div>
  <div class="form-group">
      <label for="email">Email</label>
      {{ Form::text('email', old('email'), ['class' => 'form-control', 'id' => 'email',"placeholder"=>"Email"]) }}
  </div>
  <div class="form-group">
  <label for="cellphone">Handphone</label>
  {{ Form::text('cellphone', old('cellphone'), ['class' => 'form-control', 'id' => 'phone',"placeholder"=>"Handphone"]) }}  
  </div>
  <div class="form-group">
      <label for="mosque_id">Masjid</label> 
      {{ Form::select('mosque_id', \Models\Mosque::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker', 'multiple']) }}
    </div>
    <div class="form-group">
      <label for="mushola_id">Mushola</label> 
      {{ Form::select('mushola_id', \Models\Mushola::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker', 'multiple']) }}
    </div>
</div>
{{ Form::close() }}