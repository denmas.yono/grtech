     <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Santri Masjid Mujahiddin</h3>
                {!!create(['url'=> route('student.create'), 'title'=> 'Tambah Santri', 'style'=> "float: right;"]) !!}
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="student" class="table table-bordered table-hover display standard" style="width: 100%" data-route="{{ route('student.getListAjax') }}">
                  <thead>
                  <tr>
                    <th data-name="id">No</th> 
                    <th data-name="full_name">Nama</th> 
                    <th data-name="email">Email</th>
                    <th data-name="phone">Handphone</th>
                    <th data-name="mosque">Masjid</th>
                    <th data-name="mushola">Mushola</th>
                    <th data-name="action" nowrap="nowrap">Action</th> 
                  </tr>
                  </thead>
                  <tbody>
                     
                  </tbody> 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content --> 