<?php

namespace App\Modules\EventAttribute\Controllers;

use Models\EventAttribute as EventAttribute; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreEventAttribute;
       
class EventAttributeController extends MainController
{
    public function __construct() { 
        parent::__construct(new EventAttribute(), 'EventAttribute');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('EventAttribute::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $event_attributes = $this->_model::with(['event'])->select('*');
            if( request()->has('event_id')){
                $event_attributes->where('event_id', request()->input('event_id'));
            } 
            return datatables()->of($event_attributes)
                    ->addIndexColumn() 
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('event_attribute.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('event_attribute.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('event_attribute.destroy',$row->id), 'preview'=> route('event_attribute.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $event_id = request()->input('event_id');
        return view('EventAttribute::create',['event_id'=> $event_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventAttribute $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('event_attribute.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventAttribute  $EventAttribute
     * @return \Illuminate\Http\Response
     */
    public function show($event_attribute_id)
    {
        $event_attribute = $this->_model::find($event_attribute_id);
        return view('EventAttribute::show', ['event_attribute'=> $event_attribute]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventAttribute  $EventAttribute
     * @return \Illuminate\Http\Response
     */
    public function preview($event_attribute_id)
    {
        $event_attribute = $this->_model::find($event_attribute_id);
        return view('EventAttribute::preview', ['event_attribute'=> $event_attribute]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\EventAttribute  $EventAttribute
     * @return \Illuminate\Http\Response
     */
    public function edit($event_attribute_id)
    {
        $event_attribute = $this->_model::find($event_attribute_id);
        return view('EventAttribute::edit', ['event_attribute'=> $event_attribute]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\EventAttribute  $EventAttribute
     * @return \Illuminate\Http\Response
     */
    public function update( $event_attribute_id)
    {  
        try {
            $EventAttribute = $this->_model::find($event_attribute_id);
            if($EventAttribute){                
                $EventAttribute->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('event_attribute.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\EventAttribute  $EventAttribute
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_attribute_id)
    {
        try {
            $EventAttribute = $this->_model::find($event_attribute_id);
            if($EventAttribute){
                $EventAttribute->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('event_attribute.index')], 200);
    }
}
