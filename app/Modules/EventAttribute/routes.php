<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'event_attribute', 'namespace' => 'App\Modules\EventAttribute\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'event_attribute.index', 'uses' => 'EventAttributeController@index']);
    Route::get('/getListAjax', ['as' => 'event_attribute.getListAjax', 'uses' => 'EventAttributeController@getListAjax']);
    Route::get('/create', ['as' => 'event_attribute.create', 'uses' => 'EventAttributeController@create']);
    Route::get('/show/{id}', ['as' => 'event_attribute.show', 'uses' => 'EventAttributeController@show']);    
    Route::get('/preview/{id}', ['as' => 'event_attribute.preview', 'uses' => 'EventAttributeController@preview']);    
    Route::post('/store', ['as' => 'event_attribute.store', 'uses' => 'EventAttributeController@store']);
    Route::get('/edit/{id}', ['as' => 'event_attribute.edit', 'uses' => 'EventAttributeController@edit']);
    Route::put('/update/{id}', ['as' => 'event_attribute.update', 'uses' => 'EventAttributeController@update']);
    Route::delete('/delete/{id}', ['as' => 'event_attribute.destroy', 'uses' => 'EventAttributeController@destroy']);
});