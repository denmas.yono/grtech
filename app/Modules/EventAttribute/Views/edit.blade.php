{{ Form::model($EventAttribute, ['method' => 'POST', 'route' => ['event_attribute.update', $EventAttribute->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Nama Kegiatan</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $EventAttribute->name }}"
            placeholder="Nama Kegiatan">
    </div>
    <div class="form-group">
        <label for="parent">Kategori Kegiatan</label> 
        {{Form::select('EventAttribute_type_id', \Models\EventAttributeType::pluck('name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "type", "data-live-search"=> "true", 'data-style'=> 'btn-success'])}}
    </div> 
    <div class="form-group">
      <label for="start_date">Mulai</label> 
      {{Form::date('date_start', null, ['class'=> 'form-control'])}}
    </div> 
    <div class="form-group">
      <label for="end_date">Berakhir</label> 
      {{Form::date('date_end', null, ['class'=> 'form-control'])}}
    </div>
    <div class="form-group">
        <label for="name">Keterangan</label>
        {{ Form::textarea('description', null, ["class"=> "form-control", 'rows' => 3, 'placeholder' => 'Isi Keterangan Kegiatan']) }}
    </div>
</div>
{{ Form::close() }}
