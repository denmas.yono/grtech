
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Perlengkapan Acara</h3>
                {!!create(['url'=> route('event_attribute.create'), 'title'=> 'Tambah Perlengkapan', 'style'=> "float: right;"]) !!}
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="event_attribute" class="table table-bordered table-hover display standard" style="width: 100%" data-route="{{ route('event_attribute.getListAjax') }}">
                  <thead>
                  <tr>
                    <th data-name="id">No</th> 
                    <th data-name="name">Nama</th>  
                    <th data-name="event_type.name">Tipe Kegiatan</th> 
                    <th data-name="start_date">Mulai</th>
                    <th data-name="end_date">Berakhir</th>
                    <th data-name="description">Keterangan</th>
                    <th data-name="action">Action</th> 
                  </tr>
                   
                  </thead>
                  <tbody>
                     
                  </tbody> 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content --> 