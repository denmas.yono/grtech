{{ Form::open(['method' => 'POST','route' => ['event_attribute.store'],'class' => 'form-horizontal','enctype' => 'multipart/form-data']) }}
{{ Form::hidden('event_id', $event_id) }}
<div class="card-body">
    <div class="form-group">
        <label for="parent">Nama</label>
        {{ Form::select('people_id', \Models\People::pluck('first_name', 'id')->all(), null, ['class' => 'form-control selectpicker','id' => 'type','data-live-search' => 'true','data-style' => 'btn-success']) }}
    </div>

    <div class="form-group">
        <label for="name">Keterangan</label>
        {{ Form::textarea('description', null, ['class' => 'form-control','rows' => 3,'placeholder' => 'Isi Keterangan Kegiatan']) }}
    </div>
</div>
<!-- /.card-body -->
{{ Form::close() }}
@push('js')
    <script>
        $(function() {
            $('#reservationdatetime').datetimepicker({
                icons: {
                    time: 'far fa-clock'
                }
            });
        })
    </script>
@endpush
