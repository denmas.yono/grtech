<?php

namespace App\Modules\EventType\Controllers;

use Models\EventType as event_type; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreEventType;
       
class EventTypeController extends MainController
{
    public function __construct() { 
        parent::__construct(new event_type(), 'event_type');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('EventType::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $event_types = $this->_model::select('*');
            return datatables()->of($event_types)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('event_type.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('event_type.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('event_type.destroy',$row->id), 'preview'=> route('event_type.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('EventType::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventType $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('event_type.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventType  $event_type
     * @return \Illuminate\Http\Response
     */
    public function show($event_type_id)
    {
        $event_type = $this->_model::find($event_type_id);
        return view('EventType::show', ['event_type'=> $event_type]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventType  $event_type
     * @return \Illuminate\Http\Response
     */
    public function preview($event_type_id)
    {
        $event_type = $this->_model::find($event_type_id);
        return view('EventType::preview', ['event_type'=> $event_type]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\EventType  $event_type
     * @return \Illuminate\Http\Response
     */
    public function edit($event_type_id)
    {
        $event_type = $this->_model::find($event_type_id);
        return view('EventType::edit', ['event_type'=> $event_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\EventType  $event_type
     * @return \Illuminate\Http\Response
     */
    public function update( $event_type_id)
    {  
        try {
            $event_type = $this->_model::find($event_type_id);
            if($event_type){                
                $event_type->update($this->_serialize($request));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('event_type.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\EventType  $event_type
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_type_id)
    {
        try {
            $event_type = $this->_model::find($event_type_id);
            if($event_type){
                $event_type->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('event_type.index')], 200);
    }
}
