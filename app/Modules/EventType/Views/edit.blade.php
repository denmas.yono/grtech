{{ Form::model($event_type, ['method' => 'PUT', 'route' => ['event_type.update', $event_type->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Jenis Kegiatan</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $event_type->name }}"
            placeholder="Jenis Kegiatan">
    </div>
    <div class="form-group">
        <label for="name">Keterangan</label>
        {{ Form::textarea('description', null, ["class"=> "form-control", 'rows' => 3, 'placeholder' => 'Isi Keterangan Kegiatan']) }}
    </div>
</div>
{{ Form::close() }}
