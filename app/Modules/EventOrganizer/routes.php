<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'event_organizer', 'namespace' => 'App\Modules\EventOrganizer\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'event_organizer.index', 'uses' => 'EventOrganizerController@index']);
    Route::get('/getListAjax', ['as' => 'event_organizer.getListAjax', 'uses' => 'EventOrganizerController@getListAjax']);
    Route::get('/getListAjaxEventSection', ['as' => 'event_organizer.getListAjaxEventSection', 'uses' => 'EventOrganizerController@getListAjaxEventSection']);
    Route::get('/create', ['as' => 'event_organizer.create', 'uses' => 'EventOrganizerController@create']);
    Route::get('/show/{id}', ['as' => 'event_organizer.show', 'uses' => 'EventOrganizerController@show']);    
    Route::get('/preview/{id}', ['as' => 'event_organizer.preview', 'uses' => 'EventOrganizerController@preview']);    
    Route::post('/store', ['as' => 'event_organizer.store', 'uses' => 'EventOrganizerController@store']);
    Route::get('/edit/{id}', ['as' => 'event_organizer.edit', 'uses' => 'EventOrganizerController@edit']);
    Route::put('/update/{id}', ['as' => 'event_organizer.update', 'uses' => 'EventOrganizerController@update']);
    Route::delete('/delete/{id}', ['as' => 'event_organizer.destroy', 'uses' => 'EventOrganizerController@destroy']);
});