<?php

namespace App\Modules\EventOrganizer\Controllers;

use Models\EventOrganizer as EventOrganizer; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreEventOrganizer;
       
class EventOrganizerController extends MainController
{
    public function __construct() { 
        parent::__construct(new EventOrganizer(), 'EventOrganizer');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('EventOrganizer::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $event_organizers = $this->_model::with(['event'])->select('*');
            if( request()->has('event_id')){
                $event_organizers->where('event_id', request()->input('event_id'));
            }
            return datatables()->of($event_organizers)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('event_organizer.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('event_organizer.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('event_organizer.destroy',$row->id), 'preview'=> route('event_organizer.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })   
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    public function getListAjaxEventSection(){
        if (request()->ajax()) {
            $event_organizers = $this->_model::with(['event','people','event_section'])->select('*')->where([
                'event_id'=> request()->input('event_id'),
                'event_section_id'=> request()->input('event_section_id'),
            ]);
            return datatables()->of($event_organizers)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('event_organizer.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('event_organizer.show',$row->id), 'title'=> $row->name, 'popup'=> true]);
                            $btn .= hapus(['url'=> route('event_organizer.destroy',$row->id), 'preview'=> route('event_organizer.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })   
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $event = null;
        $event_section = null;
        if(request()->has('event_id') && request()->has('event_section_id')){
            $event = \Models\Event::find(request()->input('event_id'));
            $event_section = \Models\EventSection::find(request()->input('event_section_id'));
        }
        return view('EventOrganizer::create',['event'=> $event, 'event_section'=> $event_section]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventOrganizer $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.'], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventOrganizer  $EventOrganizer
     * @return \Illuminate\Http\Response
     */
    public function show($event_organizer_id)
    {
        $event_organizer = $this->_model::find($event_organizer_id);
        return view('EventOrganizer::show', ['event_organizer'=> $event_organizer]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventOrganizer  $EventOrganizer
     * @return \Illuminate\Http\Response
     */
    public function preview($event_organizer_id)
    {
        $event_organizer = $this->_model::find($event_organizer_id);
        return view('EventOrganizer::preview', ['event_organizer'=> $event_organizer]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\EventOrganizer  $EventOrganizer
     * @return \Illuminate\Http\Response
     */ 
    public function edit( $event_organizer_id)
    {   $event_organizer = $this->_model::find($event_organizer_id);
        $event = null;
        $event_section = null;
        if(request()->has('event_id') && request()->has('event_section_id')){
            $event = \Models\Event::find(request()->input('event_id'));
            $event_section = \Models\EventSection::find(request()->input('event_section_id'));
        }
        return view('EventOrganizer::edit',['event'=> $event, 'event_section'=> $event_section, 'event_organizer'=> $event_organizer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\EventOrganizer  $EventOrganizer
     * @return \Illuminate\Http\Response
     */
    public function update( $event_organizer_id)
    {  
        try {
            $EventOrganizer = $this->_model::find($event_organizer_id);
            if($EventOrganizer){                
                $EventOrganizer->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\EventOrganizer  $EventOrganizer
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_organizer_id)
    {
        try {
            $EventOrganizer = $this->_model::find($event_organizer_id);
            if($EventOrganizer){
                $EventOrganizer->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('event_organizer.index')], 200);
    }
}
