{{ Form::open(['method' => 'POST','route'=> ['event_organizer.store'], 'class'=> 'form-horizontal', 'enctype'=> 'multipart/form-data']) }}
    <div class="card-body"> 
        <div class="form-group">
            <label for="name">Nama Kegiatan</label> 
            @if($event)
              {{$event->name}}
              {{Form::hidden('event_id', $event->id)}}
            @else 
            {{Form::select('event_id', \Models\Event::pluck('name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "type", "data-live-search"=> "true", 'data-style'=> 'btn-success'])}}
            @endif
        </div>
        <div class="form-group">
            <label for="parent">Seksi</label> 
            @if($event_section)
              {{$event_section->name}}
              {{Form::hidden('event_section_id', $event_section->id)}}
            @else 
            {{Form::select('event_section_id', \Models\EventSection::pluck('name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "type", "data-live-search"=> "true", 'data-style'=> 'btn-success'])}}
            @endif
        </div> 
        <div class="form-group">
          <label for="start_date">Nama Anggota</label> 
          {{Form::select('people_id', \Models\People::pluck('first_name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "type", "data-live-search"=> "true", 'data-style'=> 'btn-success'])}}
        </div>  
    </div>
    <!-- /.card-body -->
{{Form::close()}} 