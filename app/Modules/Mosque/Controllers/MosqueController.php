<?php

namespace App\Modules\Mosque\Controllers;

use Models\Mosque as mosque;
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 
use Lib\File;
use App\Notifications\MosqueCreated; 
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreMosque;
       
class MosqueController extends MainController
{
    public function __construct() {  
        parent::__construct(new mosque(), 'Mosque');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        
        return view('Mosque::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $companies = $this->_model::select('*');
            return datatables()->of($companies)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('mosque.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('mosque.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('mosque.destroy',$row->id), 'preview'=> route('mosque.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    }) 
                    ->addColumn('logo', function($row){
     
                        $logo = '<img src="'.($row->logo?asset('logo/'.$row->logo):(asset('assets/images/mosque.png'))).'" class="img-responsive image-logo" data-title="'.($row->name).'" width="100px">';
                         return $logo;
                    })
                    ->addColumn('photo', function($row){
     
                        $photo = '<img src="'.($row->photo?url('mosque_file/'.$row->photo):(assets('assets/images/mosque.png'))).'" class="img-responsive image-logo" data-title="'.($row->name).'" width="100px">';
                         return $photo;
                    })
                    ->rawColumns(['action','logo','photo'])
                    ->make(true);
        }
    }
 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Mosque::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMosque $request)
    {    
        try {
              
            $mosque = $this->_model::create($this->_serialize($request)); 
            if (request()->file('logo')) {                
                $this->validate($request, [
                    'name' => 'required',
                    'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]); 
                
                if (request()->file('logo')->isValid()) {
                    $file = request()->file('logo');
                    // image upload in storage/app/public/logo   
                    $info_logo = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/logo')));
                    if($mosque->logo && file_exists(storage_path('app/public/logo/'.$mosque->logo))){
                        unlink(storage_path('app/public/logo/'.$mosque->logo));
                    }
                    $mosque->logo = is_object($info_logo)?$info_logo->getFilename():$info_logo;
                    $mosque->save();
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                }
            } 

            if (request()->file('photo')) {                
                $this->validate($request, [
                    'name' => 'required',
                    'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]); 
                
                if (request()->file('photo')->isValid()) {
                    $file = request()->file('photo');
                    // image upload in storage/app/public/photo   
                    $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/mosque')));
                    if($mosque->photo && file_exists(storage_path('app/public/mosque/'.$mosque->photo))){
                        unlink(storage_path('app/public/mosque/'.$mosque->photo));
                    }
                    $mosque->photo = is_object($info)?$info->getFilename():$info;
                    $mosque->save();
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                }
            } 
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Tambah Data Error '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('mosque.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function show($mosque_id)
    {   $mosque = $this->_model::find($mosque_id);
        return view('Mosque::show', ['mosque'=> $mosque]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function preview($mosque_id)
    {   $mosque = $this->_model::find($mosque_id);
        return view('Mosque::preview', ['mosque'=> $mosque]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function edit($mosque_id)
    {
        $mosque = $this->_model::find($mosque_id);
        return view('Mosque::edit', ['mosque'=> $mosque]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMosque $request, $mosque_id)
    {     
        try {
            $mosque = $this->_model::find($mosque_id);
            if($mosque){  
                $old_logo = $mosque->logo;    
                $old_photo = $mosque->photo;    
                $mosque->update($this->_serialize($request));
                 
                if (request()->file('logo')) {                
                    $this->validate($request, [
                        'name' => 'required',
                        'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]); 
                    if (request()->file('logo')->isValid()) {
                        $file = request()->file('logo');
                        // image upload in storage/app/public/logo   
                        $info_logo = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/logo')));
                        if($old_logo && file_exists(storage_path('app/public/logo/'.$old_logo))){
                            unlink(storage_path('app/public/logo/'.$old_logo));
                        }
                        $mosque->logo = is_object($info_logo)?$info_logo->getFilename():$info_logo;
                        $mosque->save();
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                    }
                } 

                if (request()->file('photo')) {                
                    $this->validate($request, [
                        'name' => 'required',
                        'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]); 
                    
                    if (request()->file('photo')->isValid()) {
                        $file = request()->file('photo');
                        // image upload in storage/app/public/photo   
                        $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/mosque')));
                        if($old_photo && file_exists(storage_path('app/public/mosque/'.$old_photo))){
                            unlink(storage_path('app/public/mosque/'.$old_photo));
                        }
                        $mosque->photo = is_object($info)?$info->getFilename():$info; 
                        $mosque->save();
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                    }
                }else{
                    $mosque->photo = $old_photo;
                    $mosque->save();
                }
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => 'Tambah Mosque error => '. $e->getMessage()], 400); 
        }
        if (request()->ajax()) {
            return response()->json(['status' => 'success', 'message' => 'Data was updated.'], 200); 
        }
        return redirect(route('mosque.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Mosque  $mosque
     * @return \Illuminate\Http\Response
     */
    public function destroy($mosque_id)
    {   
        try { 
            $mosque = $this->_model::find($mosque_id); 
            if($mosque){
                $mosque->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('mosque.index')], 200);
    }
}
