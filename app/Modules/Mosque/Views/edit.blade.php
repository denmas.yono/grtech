{{ Form::model($mosque, ['method' => 'PUT','route' => ['mosque.update', $mosque->id],'class' => 'form-horizontal','enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Nama</label>
        {{ Form::text('name', $mosque->name, ['class' => 'form-control', 'placeholder' => 'Nama']) }}
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        {{ Form::email('email', $mosque->email, ['class' => 'form-control', 'placeholder' => 'Email']) }}
    </div>
    <div class="form-group">
        <label for="preview"></label>
        <div class="post-review">
            <img src="{{url('logo/'.$mosque->logo)}}" class="img-responsive" onerror="this.src='{{ assets('assets/images/mosque.png') }}'"
                width="100px">
        </div>
    </div>
    <div class="form-group">
        <label for="phone">Logo</label>
        <input type="file" class="form-control post-input" id="logo" name="logo" value="{{ old('logo') }}"
            placeholder="Upload Logo" data-target="post-review">
    </div>
    <div class="form-group">
        <label for="preview"></label>
        <div class="post-review2">
            <img src="{{url('mosque_file/'.$mosque->photo)}}" class="img-responsive" onerror="this.src='{{ assets('assets/images/mosque.png') }}'"
                width="100px">
        </div>
    </div>
    <div class="form-group">
        <label for="phone">Photo</label>
        <input type="file" class="form-control post-input" id="photo" name="photo" value="{{ old('logo') }}"
            placeholder="Upload Photo" data-target="post-review2">
    </div>
    <div class="form-group">
        <label for="website">Website</label>
        <input type="text" class="form-control" id="website" name="website" value="{{ $mosque->website }}"
            placeholder="Masukan website">
    </div>
    <div class="form-group">
        <label for="phone">No Telephone</label>
        {{ Form::text('phone', $mosque->phone, ['class' => 'form-control', 'placeholder' => 'Nomor Telephone']) }}
    </div>
    <div class="form-group">
        <label for="cellphone">Handphone</label>
        {{ Form::text('cellphone', $mosque->phone, ['class' => 'form-control', 'placeholder' => 'Nomor Handphone']) }}
    </div>
    <div class="form-group">
        <label for="address">Alamat</label>
        {{ Form::textarea('address_detail', $mosque->address_detail, ['class' => 'form-control', 'placeholder' => 'Alamat', 'rows'=> 3]) }}
    </div>
</div>
{{ Form::close() }}

@section('adminlte_js')
    @push('js')
        <script src="{{ asset('js/core.js') }}"></script>
    @endpush
@stop
