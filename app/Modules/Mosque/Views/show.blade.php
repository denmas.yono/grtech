@extends('adminlte::page') 
@section('content')

{{ Form::model($mosque, ['method' => 'PUT', 'route' => ['mosque.update', $mosque->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
    <div id="container">
        <div class="card-body">
            <div class="form-group">
                <label for="name">Nama</label>
                {{ Form::text('name', $mosque->name, ['class' => 'form-control', 'placeholder' => 'Nama']) }}
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                {{ Form::email('email', $mosque->email, ['class' => 'form-control', 'placeholder' => 'Email']) }}
            </div>
            <div class="form-group">
                <label for="preview"></label>
                <div class="post-review">
                    <img src="{{url('logo/'.$mosque->logo)}}" class="img-responsive" onerror="this.src='{{ assets('assets/images/mosque.png') }}'"
                        width="100px">
                </div>
            </div>
            <div class="form-group">
                <label for="phone">Logo</label>
                <input type="file" class="form-control post-input" id="logo" name="logo" value="{{ old('logo') }}"
                    data-target="post-review" placeholder="Upload Logo">
            </div>
            <div class="form-group">
                <label for="preview"></label>
                <div class="post-review2">
                    <img src="{{url('mosque_file/'.$mosque->photo)}}" class="img-responsive" onerror="this.src='{{ assets('assets/images/mosque.png') }}'"
                        width="100px">
                </div>
            </div>
            <div class="form-group">
                <label for="phone">Photo</label>
                <input type="file" class="form-control post-input" id="photo" name="photo" value="{{ old('logo') }}"
                    data-target="post-review2" placeholder="Upload Photo">
            </div>
            <div class="form-group">
                <label for="phone">No Telephone</label>
                {{ Form::text('phone', $mosque->phone, ['class' => 'form-control', 'placeholder' => 'Nomor Telephone']) }}
            </div>
            <div class="form-group">
                <label for="cellphone">Handphone</label>
                {{ Form::text('cellphone', $mosque->phone, ['class' => 'form-control', 'placeholder' => 'Nomor Handphone']) }}
            </div>
            {!! kembali(['url' => route('mosque.index'), 'title' => 'Kembali', 'style' => 'float:left']) !!}
            {!! submit(['title' => 'Simpan', 'style' => 'float:right']) !!}
        </div>
    </div>
    {{ Form::close() }}
@stop
