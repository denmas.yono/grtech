<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'mosque', 'namespace' => 'App\Modules\Mosque\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'mosque.index', 'uses' => 'MosqueController@index']);
    Route::get('/getListAjax', ['as' => 'mosque.getListAjax', 'uses' => 'MosqueController@getListAjax']);
    Route::get('/create', ['as' => 'mosque.create', 'uses' => 'MosqueController@create']);
    Route::post('/store', ['as' => 'mosque.store', 'uses' => 'MosqueController@store']);
    Route::get('/edit/{id}', ['as' => 'mosque.edit', 'uses' => 'MosqueController@edit']);
    Route::get('/show/{id}', ['as' => 'mosque.show', 'uses' => 'MosqueController@show']);
    Route::get('/preview/{id}', ['as' => 'mosque.preview', 'uses' => 'MosqueController@preview']);
    Route::put('/update/{id}', ['as' => 'mosque.update', 'uses' => 'MosqueController@update']);
    Route::delete('/destroy/{id}', ['as' => 'mosque.destroy', 'uses' => 'MosqueController@destroy']);
});