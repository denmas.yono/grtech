<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'event_cost', 'namespace' => 'App\Modules\EventCost\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'event_cost.index', 'uses' => 'EventCostController@index']);
    Route::get('/getListAjax', ['as' => 'event_cost.getListAjax', 'uses' => 'EventCostController@getListAjax']);
    Route::get('/create', ['as' => 'event_cost.create', 'uses' => 'EventCostController@create']);
    Route::get('/show/{id}', ['as' => 'event_cost.show', 'uses' => 'EventCostController@show']);    
    Route::get('/preview/{id}', ['as' => 'event_cost.preview', 'uses' => 'EventCostController@preview']);    
    Route::post('/store', ['as' => 'event_cost.store', 'uses' => 'EventCostController@store']);
    Route::get('/edit/{id}', ['as' => 'event_cost.edit', 'uses' => 'EventCostController@edit']);
    Route::put('/update/{id}', ['as' => 'event_cost.update', 'uses' => 'EventCostController@update']);
    Route::delete('/delete/{id}', ['as' => 'event_cost.destroy', 'uses' => 'EventCostController@destroy']);
});