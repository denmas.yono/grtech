<?php

namespace App\Modules\EventCost\Controllers;

use Models\EventCost as EventCost; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreEventCost;
       
class EventCostController extends MainController
{
    public function __construct() { 
        parent::__construct(new EventCost(), 'EventCost');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('EventCost::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $event_costs = $this->_model::with(['event'])->select('*');
            if( request()->has('event_id')){
                $event_costs->where('event_id', request()->input('event_id'));
            }
            return datatables()->of($event_costs)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('event_cost.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('event_cost.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('event_cost.destroy',$row->id), 'preview'=> route('event_cost.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $event = null;
        if(request()->has('event_id')){
            $event = \Models\Event::find(request()->input('event_id'));
        }
        return view('EventCost::create', ['event'=> $event]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventCost $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.'], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventCost  $EventCost
     * @return \Illuminate\Http\Response
     */
    public function show($event_cost_id)
    {
        $event_cost = $this->_model::find($event_cost_id);
        return view('EventCost::show', ['event_cost'=> $event_cost]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventCost  $EventCost
     * @return \Illuminate\Http\Response
     */
    public function preview($event_cost_id)
    {
        $event_cost = $this->_model::find($event_cost_id);
        return view('EventCost::preview', ['event_cost'=> $event_cost]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\EventCost  $EventCost
     * @return \Illuminate\Http\Response
     */
    public function edit($event_cost_id)
    {
        $event_cost = $this->_model::find($event_cost_id);
        $event = $event_cost->event;
        return view('EventCost::edit', ['event_cost'=> $event_cost, 'event'=> $event]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\EventCost  $EventCost
     * @return \Illuminate\Http\Response
     */
    public function update( $event_cost_id)
    {  
        try {
            $EventCost = $this->_model::find($event_cost_id);
            if($EventCost){                
                $EventCost->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\EventCost  $EventCost
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_cost_id)
    {
        try {
            $EventCost = $this->_model::find($event_cost_id);
            if($EventCost){
                $EventCost->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('event_cost.index')], 200);
    }
}
