{{ Form::open(['method' => 'POST','route'=> ['event_cost.store'], 'class'=> 'form-horizontal', 'enctype'=> 'multipart/form-data']) }}
    <div class="card-body"> 
        <div class="form-group">
            <label for="name">Nama Kegiatan</label>
            @if($event)
              {{$event->name}}
              {{Form::hidden('event_id', $event->id)}}
            @else 
            {{Form::select('event_id', \Models\Event::pluck('name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "type", "data-live-search"=> "true", 'data-style'=> 'btn-success'])}}
            @endif
        </div>
        <div class="form-group">
            <label for="parent">Keperluan</label> 
            {{Form::text('name', null, ['class'=> 'form-control','required'])}}
        </div> 
        <div class="form-group">
          <label for="amount">Jumlah</label> 
          {{Form::text('amount', null, ['class'=> 'form-control amount', 'required'])}}
        </div> 
        <div class="form-group">
          <label for="subtotal">Biaya</label> 
          {{Form::text('subtotal', null, ['class'=> 'form-control amount', 'required'])}}
        </div>
        <div class="form-group">
          <label for="description">Keterangan</label>
          {{Form::textarea('description', null, ["class"=> "form-control", 'rows'=> 3, "placeholder"=> "Isi Keterangan Keperluan"])}}
        </div>
    </div>
    <!-- /.card-body -->
{{Form::close()}} 