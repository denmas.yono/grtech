<?php

namespace App\Modules\ExpenditureCategory\Controllers;

use Models\ExpenditureCategory as expenditure_category; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreExpenditureCategory;
       
class ExpenditureCategoryController extends MainController
{
    public function __construct() { 
        parent::__construct(new expenditure_category(), 'expenditure_category');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('ExpenditureCategory::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $categories = $this->_model::select('*');
            return datatables()->of($categories)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('expenditure_category.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('expenditure_category.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('expenditure_category.destroy',$row->id), 'preview'=> route('expenditure_category.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ExpenditureCategory::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExpenditureCategory $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('expenditure_category.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\ExpenditureCategory  $expenditure_category
     * @return \Illuminate\Http\Response
     */
    public function show($category_id)
    {
        $expenditure_category = $this->_model::find($category_id);
        return view('ExpenditureCategory::show', ['expenditure_category'=> $expenditure_category]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\ExpenditureCategory  $expenditure_category
     * @return \Illuminate\Http\Response
     */
    public function preview($category_id)
    {
        $expenditure_category = $this->_model::find($category_id);
        return view('ExpenditureCategory::preview', ['expenditure_category'=> $expenditure_category]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\ExpenditureCategory  $expenditure_category
     * @return \Illuminate\Http\Response
     */
    public function edit($category_id)
    {
        $expenditure_category = $this->_model::find($category_id);
        return view('ExpenditureCategory::edit', ['expenditure_category'=> $expenditure_category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\ExpenditureCategory  $expenditure_category
     * @return \Illuminate\Http\Response
     */
    public function update( $category_id)
    {  
        try {
            $expenditure_category = $this->_model::find($category_id);
            if($expenditure_category){                
                $expenditure_category->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('expenditure_category.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\ExpenditureCategory  $expenditure_category
     * @return \Illuminate\Http\Response
     */
    public function destroy($category_id)
    {
        try {
            $expenditure_category = $this->_model::find($category_id);
            if($expenditure_category){
                $expenditure_category->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('expenditure_category.index')], 200);
    }
}
