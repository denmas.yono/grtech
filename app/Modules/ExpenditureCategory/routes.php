<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'expenditure_category', 'namespace' => 'App\Modules\ExpenditureCategory\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'expenditure_category.index', 'uses' => 'ExpenditureCategoryController@index']);
    Route::get('/getListAjax', ['as' => 'expenditure_category.getListAjax', 'uses' => 'ExpenditureCategoryController@getListAjax']);
    Route::get('/create', ['as' => 'expenditure_category.create', 'uses' => 'ExpenditureCategoryController@create']);
    Route::get('/show/{id}', ['as' => 'expenditure_category.show', 'uses' => 'ExpenditureCategoryController@show']);    
    Route::get('/preview/{id}', ['as' => 'expenditure_category.preview', 'uses' => 'ExpenditureCategoryController@preview']);    
    Route::post('/store', ['as' => 'expenditure_category.store', 'uses' => 'ExpenditureCategoryController@store']);
    Route::get('/edit/{id}', ['as' => 'expenditure_category.edit', 'uses' => 'ExpenditureCategoryController@edit']);
    Route::put('/update/{id}', ['as' => 'expenditure_category.update', 'uses' => 'ExpenditureCategoryController@update']);
    Route::delete('/delete/{id}', ['as' => 'expenditure_category.destroy', 'uses' => 'ExpenditureCategoryController@destroy']);
});