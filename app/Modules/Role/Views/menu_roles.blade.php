@extends('adminlte::page') 
@section('content')
    <div id="container">
        @include('Role::list_menu')
    </div>
    @push('js')
        <script>
            $(function() {
                loadMenuRoles();
                $("body").on('click', 'input[type=checkbox]', function(e){
                    let data = {
                        field:$(this).attr('data-field'),
                        value:$(this).attr('data-value'),
                        id:$(this).attr('data-id'),
                    };
                    $.post('{{url("menu_role/updateField")}}',data, function(res){
                         $("button.reload").click();
                    });
                });
            });

            function loadMenuRoles() {
                var selected = [];
                $("#menu_role").DataTable({
                    dom:"<'row'<'col-sm-3'l><'col-sm-4'B><'col-sm-5'f>>rtip",
                    responsive: true,
                    scrollY: 400,
                    responsive: true, 
                    processing: true,
                    serverSide: true,  
                    select: true,
                    "rowCallback": function( row, data ) {
                        if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                            $(row).addClass('selected');
                        }
                    },
                    "ajax": "{{ route('role.getMenuRoleListAjax', $role->id) }}",
                    "columns": [
                        // data:column name from server ,name:alias
                        {
                            data: 'DT_RowIndex',
                            name: 'id'
                        },
                        {
                            data: function(row, type){ 
                                if(row.menu){
                                return row.menu.name
                                }
                            },
                            name: 'menu.name'
                        }, 
                        {
                            data: function(row, type){
                                return '<div class="icheck-primary d-inline"><input type="checkbox" id="'+row.id+'index" '+((row.index)?"checked":null)+' data-value="'+row.index+'" data-field="index" data-id="'+row.id+'"><label for="'+row.id+'index"></label></div>';
                            },
                            name: 'index'
                        },
                        {
                            data: function(row, type){
                                return '<div class="icheck-primary d-inline"><input type="checkbox" id="'+row.id+'create" '+((row.create)?"checked":null)+' data-value="'+row.create+'" data-field="create" data-id="'+row.id+'"><label for="'+row.id+'create"></label></div>';
                            },
                            name: 'create'
                        },
                        {
                            data: function(row, type){
                                return '<div class="icheck-primary d-inline"><input type="checkbox" id="'+row.id+'edit" '+((row.edit)?"checked":null)+' data-value="'+row.edit+'" data-field="edit" data-id="'+row.id+'"><label for="'+row.id+'edit"></label></div>';
                            },
                            name: 'edit'
                        },
                        {
                            data: function(row, type){
                                return '<div class="icheck-primary d-inline"><input type="checkbox" id="'+row.id+'show" '+((row.show)?"checked":null)+' data-value="'+row.show+'" data-field="show" data-id="'+row.id+'"><label for="'+row.id+'show"></label></div>';
                            },
                            name: 'show'
                        },
                        {
                            data: function(row, type){
                                return '<div class="icheck-primary d-inline"><input type="checkbox" id="'+row.id+'print" '+((row.print)?"checked":null)+' data-value="'+row.print+'" data-field="print" data-id="'+row.id+'"><label for="'+row.id+'print"></label></div>';
                            },
                            name: 'print'
                        },
                        {
                            data: function(row, type){
                                return '<div class="icheck-primary d-inline"><input type="checkbox" id="'+row.id+'destroy" '+((row.destroy)?"checked":null)+' data-value="'+row.destroy+'" data-field="destroy" data-id="'+row.id+'"><label for="'+row.id+'destroy"></label></div>';
                            },
                            name: 'destroy'
                        },
                    ], 
                });
            }
        </script>
    @endpush
@stop
