{{ Form::model($asset_status, ['method' => 'POST', 'route' => ['asset_status.update', $asset_status->id], 'class' => 'form-horizontal']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Nama</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $asset_status->name }}"
            placeholder="Nama">
    </div> 
</div>
{{ Form::close() }}
