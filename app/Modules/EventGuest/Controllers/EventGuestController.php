<?php

namespace App\Modules\EventGuest\Controllers;

use Models\EventGuest as EventGuest; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreEventGuest;
       
class EventGuestController extends MainController
{
    public function __construct() { 
        parent::__construct(new EventGuest(), 'EventGuest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('EventGuest::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $event_guests = $this->_model::with(['event','people'])->select('*');
            if( request()->has('event_id')){
                $event_guests->where('event_id', request()->input('event_id'));
            }
            return datatables()->of($event_guests)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('event_guest.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('event_guest.show',$row->id), 'title'=> $row->name]);
                            $btn .= cetak(['url'=> route('event_guest.print',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('event_guest.destroy',$row->id), 'preview'=> route('event_guest.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $event_id = request()->input('event_id');
        return view('EventGuest::create',['event_id'=> $event_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventGuest $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('event_guest.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventGuest  $EventGuest
     * @return \Illuminate\Http\Response
     */
    public function show($event_guest_id)
    {
        $event_guest = $this->_model::find($event_guest_id);
        return view('EventGuest::show', ['event_guest'=> $event_guest]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\EventGuest  $EventGuest
     * @return \Illuminate\Http\Response
     */
    public function preview($event_guest_id)
    {
        $event_guest = $this->_model::find($event_guest_id);
        return view('EventGuest::preview', ['event_guest'=> $event_guest]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\EventGuest  $EventGuest
     * @return \Illuminate\Http\Response
     */
    public function edit($event_guest_id)
    {
        $event_guest = $this->_model::find($event_guest_id);
        return view('EventGuest::edit', ['event_guest'=> $event_guest]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\EventGuest  $EventGuest
     * @return \Illuminate\Http\Response
     */
    public function print($event_guest_id)
    {
        $event_guest = $this->_model::find($event_guest_id);
        return view('EventGuest::print', ['event_guest'=> $event_guest]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\EventGuest  $EventGuest
     * @return \Illuminate\Http\Response
     */
    public function update( $event_guest_id)
    {  
        try {
            $EventGuest = $this->_model::find($event_guest_id);
            if($EventGuest){                
                $EventGuest->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('event_guest.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\EventGuest  $EventGuest
     * @return \Illuminate\Http\Response
     */
    public function destroy($event_guest_id)
    {
        try {
            $EventGuest = $this->_model::find($event_guest_id);
            if($EventGuest){
                $EventGuest->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('event_guest.index')], 200);
    }
}
