
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Tamu Undangan</h3> 
                {!!create(['url'=> route('event_guest.create'), 'title'=> 'Tambah Tamu Undangan', 'style'=> "float: right;"]) !!}
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="event_guest" class="table table-bordered table-hover display standard" style="width: 100%" data-route="{{ route('event_guest.getListAjax') }}">
                  <thead>
                    <tr>
                      <th data-name="id">No</th> 
                      <th data-name="people.first_name">Nama</th>  
                      <th data-name="as">Sebagai</th>  
                      <th data-name="address">Alamat</th>  
                      <th data-name="description">Tujuan</th>  
                      <th data-name="action" nowrap="nowrap">Action</th> 
                    </tr>c
                  </thead>
                  <tbody>
                     
                  </tbody> 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content --> 