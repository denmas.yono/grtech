{{ Form::model($event_guest, ['method' => 'POST', 'route' => ['event_guest.update', $event_guest->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body"> 
  <div class="card-body">  
    <div class="form-group">
        <label for="parent">Nama</label> 
        {{Form::select('people_id', \Models\People::pluck('first_name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "type", "data-live-search"=> "true", 'data-style'=> 'btn-success'])}}
    </div> 
    <div class="form-group">
      <label for="parent">Sebagai</label> 
      {{Form::text('as',null, ["class" => "form-control selectpicker", "id"=> "type", "data-live-search"=> "true", 'data-style'=> 'btn-success'])}}
  </div>
  <div class="form-group">
    <label for="name">Alamat</label>
    {{Form::textarea('address', null, ["class"=> "form-control", 'rows'=> 3, "placeholder"=> "Isi Alamat"])}}
  </div>
    <div class="form-group">
      <label for="name">Keterangan</label>
      {{Form::textarea('description', null, ["class"=> "form-control", 'rows'=> 3, "placeholder"=> "Isi Keterangan"])}}
    </div>
</div> 
{{ Form::close() }}
