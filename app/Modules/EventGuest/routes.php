<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'event_guest', 'namespace' => 'App\Modules\EventGuest\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'event_guest.index', 'uses' => 'EventGuestController@index']);
    Route::get('/getListAjax', ['as' => 'event_guest.getListAjax', 'uses' => 'EventGuestController@getListAjax']);
    Route::get('/create', ['as' => 'event_guest.create', 'uses' => 'EventGuestController@create']);
    Route::get('/show/{id}', ['as' => 'event_guest.show', 'uses' => 'EventGuestController@show']);    
    Route::get('/print/{id}', ['as' => 'event_guest.print', 'uses' => 'EventGuestController@print']);    
    Route::get('/preview/{id}', ['as' => 'event_guest.preview', 'uses' => 'EventGuestController@preview']);    
    Route::post('/store', ['as' => 'event_guest.store', 'uses' => 'EventGuestController@store']);
    Route::get('/edit/{id}', ['as' => 'event_guest.edit', 'uses' => 'EventGuestController@edit']);
    Route::put('/update/{id}', ['as' => 'event_guest.update', 'uses' => 'EventGuestController@update']);
    Route::delete('/delete/{id}', ['as' => 'event_guest.destroy', 'uses' => 'EventGuestController@destroy']);
});