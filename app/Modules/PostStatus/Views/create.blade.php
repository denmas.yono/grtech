<div id="container"> 
    {{ Form::open(['method' => 'POST','route' => ['post_status.store'],'class' => 'form-horizontal inline','enctype' => 'multipart/form-data']) }}
    <div class="card-body"> 
        <div class="row"> 
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="code">kode</label>
                    {{ Form::text('code', old('code'), ['class' => 'form-control', 'id' => 'code', 'placeholder' => 'Kode']) }}
                </div> 
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="name">Nama</label>
                    {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nama Status']) }}
                </div> 
            </div>
        </div>
        
    </div>
    
    {{ Form::close() }}
    
</div>