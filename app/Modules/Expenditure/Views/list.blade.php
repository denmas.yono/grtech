 
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Pengeluaran</h3>
                {!!create(['url'=> route('expenditure.create'), 'title'=> 'Tambah Pengeluaran Kegiatan', 'style'=> "float: right;"]) !!}
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="expenditure" class="table table-bordered table-hover display standard" style="width: 100%" data-route="{{ route('expenditure.getListAjax') }}">
                  <thead>
                  <tr>
                    <th data-name="id">No</th> 
                    <th data-name="name">Nama</th>   
                    <th data-name="amount" data-format="money">Jumlah</th> 
                    <th data-name="date" data-format="date">Tanggal</th>  
                    <th data-name="expenditure_category.name">Kategori</th> 
                    <th data-name="description">Keterangan</th> 
                    <th data-name="action">Action</th>
                  </tr>
                  </thead>
                  <tbody>
                     
                  </tbody> 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->  