@extends('adminlte::page')  
@section('content') 
    <div id="container">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Detail Pengeluaran</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-one-expenditure-tab" data-toggle="pill"
                                            href="#custom-tabs-one-expenditure" role="tab" aria-controls="custom-tabs-one-expenditure"
                                            aria-selected="true">Pengeluaran</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-expenditure_type-tab" data-toggle="pill"
                                            href="#custom-tabs-one-expenditure_type" role="tab" aria-controls="custom-tabs-one-expenditure_type"
                                            aria-selected="true">Tipe Pengeluaran</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-one-expenditure_category-tab" data-toggle="pill"
                                            href="#custom-tabs-one-expenditure_category" role="tab" aria-controls="custom-tabs-one-expenditure_category"
                                            aria-selected="true">Kategori Pengeluaran</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-one-tabContent">
                                    <div class="tab-pane fade show active" id="custom-tabs-one-expenditure" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-expenditure-tab"> 
                                        @include('Expenditure::list')
                                    </div> 
                                    <div class="tab-pane fade show" id="custom-tabs-one-expenditure_type" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-expenditure_type-tab"> 
                                        @include('ExpenditureType::list')
                                    </div> 
                                    <div class="tab-pane fade show" id="custom-tabs-one-expenditure_category" role="tabpanel"
                                        aria-labelledby="custom-tabs-one-expenditure_category-tab"> 
                                        @include('ExpenditureCategory::list')
                                    </div> 
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
    </div>
    
@endsection
