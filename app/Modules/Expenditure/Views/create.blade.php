{{ Form::open(['method' => 'POST', 'route' => ['expenditure.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Nama Pengeluaran</label>
        {{ Form::text('name', old('name'), ['class' => 'form-control', 'id' => 'name',"placeholder"=>"Nama pengeluaran"]) }}
    </div>
    <div class="form-group">
        <label for="amount">Jumlah / Nominal Uang</label>
        {{ Form::text('amount', old('amount'), ['class' => 'form-control', 'id' => 'amount',"placeholder"=>"Nominal pengeluaran"]) }}
    </div>
    <div class="form-group">
        <label for="date">Tanggal</label>
        {{ Form::date('date', old('date'), ['class' => 'form-control', 'id' => 'date',"placeholder"=>"Tanggal pengeluaran"]) }}
    </div>
    <div class="form-group">
        <label for="expenditure_category_id">Kategori</label> 
        {{ Form::select('expenditure_category_id', \Models\ExpenditureCategory::pluck('name', 'id')->all(),null, ['class' => 'form-control selectpicker']) }}
    </div>
    <div class="form-group">
        <label for="preview"></label>
        <div class="post-review">
            <img src="" class="img-responsive" onerror="this.src='{{ asset('assets/images/mosque.png') }}'"
                width="100px">
        </div>
    </div>
    <div class="form-group">
        <label for="file">Nota</label>
        <input type="file" class="form-control post-input" id="nota" name="nota" value="{{ old('nota') }}"
            placeholder="Upload Nota" data-target="post-review2">
    </div>
    <div class="form-group">
        <label for="description">Keterangan</label>
        {{ Form::textarea('description', old('description'), ['class' => 'form-control', 'id' => 'description',"placeholder"=>"Nama pengeluaran", 'rows'=> '3']) }}
    </div>
</div>
{{ Form::close() }}
