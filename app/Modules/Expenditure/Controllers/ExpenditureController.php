<?php

namespace App\Modules\Expenditure\Controllers;

use Models\Expenditure as expenditure; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreExpenditure;
       
class ExpenditureController extends MainController
{
    public function __construct() { 
        parent::__construct(new expenditure(), 'expenditure');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Expenditure::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $expenditures = $this->_model::with('expenditure_category')->select('*');
            return datatables()->of($expenditures)
                    ->addIndexColumn() 
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('expenditure.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= hapus(['url'=> route('expenditure.destroy',$row->id), 'preview'=> route('expenditure.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;

                        }
                    })   
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Expenditure::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreExpenditure $request)
    {   
        try { 
            $expenditure = $this->_model::create($this->_serialize($request));
            if (request()->file('photo')) {                
                $this->validate($request, [ 
                    'nota' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]); 
                
                if (request()->file('nota')->isValid()) {
                    $file = request()->file('nota');
                    // image upload in storage/app/public/nota   
                    $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/nota')));
                    if($expenditure->nota && file_exists(storage_path('app/public/nota/'.$expenditure->nota))){
                        unlink(storage_path('app/public/nota/'.$expenditure->nota));
                    }
                    $expenditure->nota = $info->getFilename(); 
                    $expenditure->save();
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                }
            }
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Add data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('expenditure.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Expenditure  $expenditure
     * @return \Illuminate\Http\Response
     */
    public function show($expenditure_id)
    {
        $expenditure = $this->_model::find($expenditure_id);
        return view('Expenditure::show', ['expenditure'=> $expenditure]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Expenditure  $expenditure
     * @return \Illuminate\Http\Response
     */
    public function preview($expenditure_id)
    {
        $expenditure = $this->_model::find($expenditure_id);
        return view('Expenditure::preview', ['expenditure'=> $expenditure]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Expenditure  $expenditure
     * @return \Illuminate\Http\Response
     */
    public function edit($expenditure_id)
    {
        $expenditure = $this->_model::find($expenditure_id);
        return view('Expenditure::edit', ['expenditure'=> $expenditure]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Expenditure  $expenditure
     * @return \Illuminate\Http\Response
     */
    public function update( $expenditure_id)
    {  
        try {
            $expenditure = $this->_model::find($expenditure_id);
            if($expenditure){                
                $expenditure->update($this->_serialize($request));
                if (request()->file('photo')) {                
                    $this->validate($request, [ 
                        'nota' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]); 
                    
                    if (request()->file('nota')->isValid()) {
                        $file = request()->file('nota');
                        // image upload in storage/app/public/nota   
                        $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/nota')));
                        if($expenditure->nota && file_exists(storage_path('app/public/nota/'.$expenditure->nota))){
                            unlink(storage_path('app/public/nota/'.$expenditure->nota));
                        }
                        $expenditure->nota = $info->getFilename(); 
                        $expenditure->save();
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                    }
                }
                
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('expenditure.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Expenditure  $expenditure
     * @return \Illuminate\Http\Response
     */
    public function destroy($expenditure_id)
    {
        try {
            $expenditure = $this->_model::find($expenditure_id);
            if($expenditure){
                $expenditure->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('expenditure.index')], 200);
    }
}
