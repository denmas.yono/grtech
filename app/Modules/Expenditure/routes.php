<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'expenditure', 'namespace' => 'App\Modules\Expenditure\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'expenditure.index', 'uses' => 'ExpenditureController@index']);
    Route::get('/getListAjax', ['as' => 'expenditure.getListAjax', 'uses' => 'ExpenditureController@getListAjax']);
    Route::get('/create', ['as' => 'expenditure.create', 'uses' => 'ExpenditureController@create']);
    Route::get('/show/{id}', ['as' => 'expenditure.show', 'uses' => 'ExpenditureController@show']);    
    Route::get('/preview/{id}', ['as' => 'expenditure.preview', 'uses' => 'ExpenditureController@preview']);    
    Route::post('/store', ['as' => 'expenditure.store', 'uses' => 'ExpenditureController@store']);
    Route::get('/edit/{id}', ['as' => 'expenditure.edit', 'uses' => 'ExpenditureController@edit']);
    Route::put('/update/{id}', ['as' => 'expenditure.update', 'uses' => 'ExpenditureController@update']);
    Route::delete('/delete/{id}', ['as' => 'expenditure.destroy', 'uses' => 'ExpenditureController@destroy']);
});