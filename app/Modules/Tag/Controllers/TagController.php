<?php

namespace App\Modules\Tag\Controllers;

use Models\Tag as tag; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreTag;
       
class TagController extends MainController
{
    public function __construct() { 
        parent::__construct(new tag(), 'tag');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Tag::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $group_menus = $this->_model::select('*');
            return datatables()->of($group_menus)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('tag.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('tag.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('tag.destroy',$row->id), 'preview'=> route('tag.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })  
                    ->addColumn('parent', function($row){  
                        return $row->parent?$row->parent->name:"";
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Tag::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTag $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('tag.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show($group_menu_id)
    {
        $tag = $this->_model::find($group_menu_id);
        return view('Tag::show', ['tag'=> $tag]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function preview($group_menu_id)
    {
        $tag = $this->_model::find($group_menu_id);
        return view('Tag::preview', ['tag'=> $tag]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit($group_menu_id)
    {
        $tag = $this->_model::find($group_menu_id);
        return view('Tag::edit', ['tag'=> $tag]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update( $group_menu_id)
    {  
        try {
            $tag = $this->_model::find($group_menu_id);
            if($tag){                
                $tag->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('tag.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy($group_menu_id)
    {
        try {
            $tag = $this->_model::find($group_menu_id);
            if($tag){
                $tag->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('tag.index')], 200);
    }
}
