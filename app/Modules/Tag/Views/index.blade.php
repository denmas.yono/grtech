 <div id="container">
     @include('Tag::list')
 </div>
 <script>
     $(function() {
         loadTags();
     });

     function loadTags() {
        var selected = [];
         $("#tag").DataTable({
            dom:"<'row'<'col-sm-3'l><'col-sm-4'B><'col-sm-5'f>>rtip",
            responsive: true,
            scrollY: 400,
            responsive: true, 
            processing: true,
            serverSide: true,  
            select: true,
            "rowCallback": function( row, data ) {
                if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                    $(row).addClass('selected');
                }
            },
             "ajax": "{{ route('tag.getListAjax') }}",
             "columns": [
                 // data:column name from server ,name:alias
                 {
                     data: 'DT_RowIndex',
                     name: 'id'
                 },
                 {
                     data: 'name',
                     name: 'name'
                 }, 
                 {
                     data: 'parent',
                     name: 'parent'
                 }, 
                 {
                     data: 'action',
                     name: 'action',
                     orderable: false,
                     searchable: false
                 },
             ]
         });
     }
 </script>
