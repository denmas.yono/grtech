 {{ Form::open(['method' => 'POST', 'route' => ['tag.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
 <div class="card-body">
     <div class="form-group">
         <label for="name">Nama Grup Menu</label>
         <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}"
             placeholder="Nama Grup Menu">
     </div>
     <div class="form-group">
         <label for="company_id">Group Menu</label>
         {{Form::select('group_menu_id', \Models\Tag::pluck('name', 'id')->all(),null, ["class" => "form-control selectpicker", "id"=> "group_menu_id", "placeholder"=>"--Pilih Parent Group Menu--"])}}                  
     </div>
 </div>
 {{ Form::close() }}
