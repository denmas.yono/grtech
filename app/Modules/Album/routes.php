<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'album', 'namespace' => 'App\Modules\Album\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'album.index', 'uses' => 'AlbumController@index']);
    Route::get('/getListAjax', ['as' => 'album.getListAjax', 'uses' => 'AlbumController@getListAjax']);
    Route::get('/create', ['as' => 'album.create', 'uses' => 'AlbumController@create']);
    Route::get('/show/{id}', ['as' => 'album.show', 'uses' => 'AlbumController@show']);    
    Route::get('/preview/{id}', ['as' => 'album.preview', 'uses' => 'AlbumController@preview']);    
    Route::post('/store', ['as' => 'album.store', 'uses' => 'AlbumController@store']);
    Route::get('/edit/{id}', ['as' => 'album.edit', 'uses' => 'AlbumController@edit']);
    Route::put('/update/{id}', ['as' => 'album.update', 'uses' => 'AlbumController@update']);
    Route::delete('/delete/{id}', ['as' => 'album.destroy', 'uses' => 'AlbumController@destroy']);
    Route::get('/list_galleries/{id}', ['as' => 'list_galleries.show', 'uses' => 'AlbumController@list_galleries']); 
});