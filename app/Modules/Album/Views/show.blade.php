@extends('adminlte::page')
@push('css')
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/ekko-lightbox/ekko-lightbox.css') }}">
    
@endpush
@section('content')
    <div id="container">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12"> 
                        <div class="card-header">
                            <h3 class="card-title">Daftar Gallery</h3> 
                            {!! kembali(['url' => '/management/gallery', 'style' => 'float:right;margin-right: 5px;margin-left: 5px;']) !!}
                          </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="filtr-item " data-category="1" data-sort="white sample">
                                        <a href="{{ asset('albums/'.$album->cover) }}"
                                            data-toggle="lightbox"
                                            data-title="{{$album->title}}">
                                            <img src="{{ asset('albums/'.$album->cover) }}" class="img-fluid mb-4" alt="{{$album->title}}" />
                                        </a>
                                    </div> 
                                </div>
                                <div class="col-md-8"> 
                                    {{$album->title}}<br>
                                    {{DateToTimeIndo($album->created_at)}}<br>
                                    <br>
                                    <br>
                                    {!!create(['url'=> route('gallery.create')."?album_id=".$album->id, 'text'=> 'Tambah Photo']) !!}   
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12"> 
                        <div class="card-body">
                            <div>
                                {{-- <div class="btn-group w-100 mb-2">
                                    <a class="btn btn-info active" href="javascript:void(0)" data-filter="all"> All items </a>
                                    <a class="btn btn-info" href="javascript:void(0)" data-filter="1"> (WHITE) </a>
                                    <a class="btn btn-info" href="javascript:void(0)" data-filter="2"> (BLACK) </a>
                                    <a class="btn btn-info" href="javascript:void(0)" data-filter="3"> (COLORED) </a>
                                    <a class="btn btn-info" href="javascript:void(0)" data-filter="4"> (COLORED, BLACK) </a>
                                </div> --}} 
                                <div class="mb-2 mb-12">
                                    <a class="btn btn-secondary" href="javascript:void(0)"
                                        data-shuffle> Shuffle items </a> 
                                    <div class="float-right">
                                        <select class="custom-select" style="width: auto;"
                                            data-sortOrder>
                                            <option value="index"> Sort by Position
                                            </option>
                                            <option value="sortData"> Sort by Custom Data
                                            </option>
                                        </select>
                                        <div class="btn-group">
                                            <a class="btn btn-default"
                                                href="javascript:void(0)" data-sortAsc>
                                                Ascending </a>
                                            <a class="btn btn-default"
                                                href="javascript:void(0)" data-sortDesc>
                                                Descending </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div> 
                                <div class="filter-container p-0 row list-galleries">
                                    @if($galleries->count() > 0)
                                        @foreach ($galleries as $gallery) 
                                            <div class="filtr-item col-sm-2" data-category="{{$gallery->id}}"
                                                data-sort="white sample">
                                                <a href="{{ asset('galleries/'.$gallery->image) }}"
                                                    data-toggle="lightbox" data-title="{{$gallery->name}}">
                                                    <img src="{{ asset('galleries/'.$gallery->image) }}" class="img-fluid mb-2" alt="{{$gallery->name}}" />
                                                </a>
                                            </div>  
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
    </div>

    @push('js')
        <script src="{{ asset('plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
        <script src="{{ asset('plugins/filterizr/jquery.filterizr.min.js') }}"></script>
        <script src="{{ asset('js/demo.js') }}"></script>
        <script>
            $(function() {
                $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                    event.preventDefault();
                    $(this).ekkoLightbox({
                        alwaysShowClose: true
                    });
                });

                $(document).on('click', 'button.submit-button', function() {
                    setTimeout(() => {
                        //getData('{{url('album/list_galleries', $album->id)}}', $("div.list-galleries"));                                            
                        window.location.reload();
                    }, 3000); 
                });

                $('.filter-container').filterizr({
                    gutterPixels: 3
                });
                $('.btn[data-filter]').on('click', function() {
                    $('.btn[data-filter]').removeClass('active');
                    $(this).addClass('active');
                });
            })
        </script>
    @endpush
@stop
