{{ Form::open(['method' => 'POST','route' => ['album.store'],'class' => 'form-horizontal','enctype' => 'multipart/form-data']) }}
    <div class="card-body"> 
        <div class="form-group">
            <label for="title">Title</label>
            {{ Form::text('title', old('title'), ['class' => 'form-control', 'id' => 'title', 'placeholder' => 'Judul']) }}
        </div> 
        <div class="form-group">
            <label for="post_status_id">Status</label>
            {{ Form::select('post_status_id', \Models\PostStatus::pluck('name','id')->all(), null, ['class' => 'form-control','id' => 'post_status_id','placeholder' => '--Pilih Status Publish--']) }}                    
        </div>
        <div class="form-group">
            <label for="publish_date">Tanggal Publish</label>
            {{ Form::date('publish_date', null, ['class' => 'form-control datepicker','id' => 'publish_date','placeholder' => 'Tanggal Publish', 'data-language'=> 'en', 'data-multiple-tables'=> 3, 'data-multiple-tables-separator'=> ',', 'data-position'=> 'top left' ]) }}
        </div>  
        <div class="form-group">
            <label for="cover">Gambar</label><br>
            {{ Form::file('cover', null, ['class' => 'form-control','id' => 'cover','placeholder' => 'Upload Gambar']) }}
        </div>
        <div class="form-group">
            <label for="template_id">Template</label>
            {{ Form::select('template_id', \Models\Template::pluck('name','id')->all(), null, ['class' => 'form-control','id' => 'template_id','placeholder' => '--Pilih Template--']) }}                    
        </div>
        <div class="form-group">
            <label for="description">Keterangan</label>
            {{ Form::textarea('description', old('description'), ['class' => 'form-control text-editor', 'id' => 'description', 'placeholder' => 'Isi Deskripsi Album', 'rows'=> "3"]) }}
        </div> 


    </div>
    
    {{ Form::close() }}