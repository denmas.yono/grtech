@if ($galleries->count() > 0)
    @foreach ($galleries as $gallery)
        <div class="filtr-item col-sm-2" data-category="1" data-sort="white sample">
            <a href="{{ asset('galleries/' . $gallery->image) }}" data-toggle="lightbox" data-title="{{ $gallery->name }}">
                <img src="{{ asset('galleries/' . $gallery->image) }}" class="img-fluid mb-2" alt="{{ $gallery->name }}" />
            </a>
        </div>
    @endforeach
@endif
