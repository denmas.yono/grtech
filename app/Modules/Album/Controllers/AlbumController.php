<?php

namespace App\Modules\Album\Controllers;

use Models\Album as album; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 
use Lib\File; 

use App\Http\Requests\StoreAlbum;
       
class AlbumController extends MainController
{
    public function __construct() { 
        parent::__construct(new album(), 'album');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Album::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $galleries = $this->_model::with(['post_status'])->select(['*']);
            return datatables()->of($galleries)
                    ->addIndexColumn() 
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('album.edit',$row->id), 'title'=> $row->title]); 
                            $btn .= show(['url'=> route('album.show',$row->id), 'title'=> $row->title]);
                            $btn .= hapus(['url'=> route('album.destroy',$row->id), 'preview'=> route('album.preview',$row->id), 'title'=> $row->title]);
                            $btn .= '</div>';       
                         return $btn; 
                        }
                    })->addColumn('status', function($row){ 
                        if($row){
                            return '<span style="background-color:'.($row->post_status?$row->post_status->bg_color:"red").'; padding:5px">'.$row->post_status?$row->post_status->name:"Draft".'</span>';
                        }
                    })->addColumn('cover', function($row){ 
                        if($row){ 
                            if($row->cover && file_exists(storage_path('app/public/albums/'.$row->cover))){
                                return '<img class="img-responsive image" style="padding:5px" src="'.asset('albums/'.$row->cover).'" onerror="this.src='.asset('assets/images/mushola.png').'" width="100px" height="100px">';
                            }
                            return '<img class="img-responsive image" style="padding:5px" src="'.asset('assets/images/mushola.png').'"  width="100px" height="100px">';
                        }
                    })
                    ->rawColumns(['action','content', 'status','cover'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Album::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAlbum $request)
    {   
        try { 
            $album = $this->_model::create($this->_serialize($request));
            $album->slug = str_replace(" ","-",strtolower($album->title));
            $album->save();
            if (request()->file('cover')) {                
                $this->validate($request, [ 
                    'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]); 
                
                if (request()->file('cover')->isValid()) {
                    $file = request()->file('cover');
                    // cover upload in storage/app/public/cover   
                    $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/albums')));
                    if($album->cover && file_exists(storage_path('app/public/albums/'.$album->cover))){
                        unlink(storage_path('app/public/albums/'.$album->cover));
                    }
                    $album->cover = is_object($info)?$info->getFilename():$info; 
                    $album->save();
                } else {
                    return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                }
            } 
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('album.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function show($album_id)
    {
        $album = $this->_model::find($album_id);
        $galleries = \Models\Gallery::where('album_id',$album_id)->get();
        return view('Album::show', ['album'=> $album, 'galleries'=> $galleries]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function preview($album_id)
    {
        $album = $this->_model::find($album_id);
        return view('Album::preview', ['album'=> $album]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function edit($album_id)
    {
        $album = $this->_model::find($album_id);
        return view('Album::edit', ['album'=> $album]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function update( $album_id)
    {  
        try {
            
            $album = $this->_model::find($album_id);
            if($album){                
                $album->update($this->_serialize(request()));
                $album->slug = str_replace(" ","-",strtolower($album->title));
                $album->save();
                if (request()->file('cover')) {                
                    $this->validate(request(), [ 
                        'cover' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    ]); 
                    
                    if (request()->file('cover')->isValid()) {
                        $file = request()->file('cover');
                        // cover upload in storage/app/public/cover   
                        $info = File::storeLocalFile($file, File::createLocalDirectory(storage_path('app/public/albums')));
                        if($album->cover && file_exists(storage_path('app/public/albums/'.$album->cover))){
                            unlink(storage_path('app/public/albums/'.$album->cover));
                        }
                        $album->cover = is_object($info)?$info->getFilename():$info; 
                        $album->save();
                    } else {
                        return response()->json(['status' => 'error', 'message' => 'Image not allowed to upload.'], 200);
                    }
                } 

            }
        } catch (\Exception $e) {
            dump($e->getTraceAsString());
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('album.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function destroy($album_id)
    {
        try {
            $album = $this->_model::find($album_id);
            if($album){
                $album->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('album.index')], 200);
    } 

    /**
     * Display the specified resource.
     *
     * @param  \Models\Album  $album
     * @return \Illuminate\Http\Response
     */
    public function list_galleries($album_id)
    {
        $album = $this->_model::find($album_id);
        $galleries = \Models\Gallery::where('album_id',$album_id)->get();
        return view('Album::list_galleries', ['album'=> $album, 'galleries'=> $galleries]);
    }
}
