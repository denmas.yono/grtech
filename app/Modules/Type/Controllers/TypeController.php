<?php

namespace App\Modules\Type\Controllers;

use Models\Type as type; 
use Illuminate\Http\Request;
use App\Http\Controllers\MainController; 

use App\Http\Requests\StoreType;
       
class TypeController extends MainController
{
    public function __construct() { 
        parent::__construct(new type(), 'type');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('Type::index');
    }

    public function getListAjax(){
        if (request()->ajax()) {
            $types = $this->_model::select('*');
            return datatables()->of($types)
                    ->addIndexColumn() 
                    ->addColumn('action', function($row){ 
                        if($row){
                            $btn = '<div class="justify-content-between">'; 
                            $btn .= edit(['url'=> route('type.edit',$row->id), 'title'=> $row->name]); 
                            $btn .= show(['url'=> route('type.show',$row->id), 'title'=> $row->name]);
                            $btn .= hapus(['url'=> route('type.destroy',$row->id), 'preview'=> route('type.preview',$row->id), 'title'=> $row->name]);
                            $btn .= '</div>';       
                         return $btn;
    
                        }
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Type::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreType $request)
    {   
        try { 
            $this->_model::create($this->_serialize($request));
        } catch (\Exception $e) { 
            return response()->json(['status' => 'error', 'message' => 'Tambah data error => '. $e->getMessage()], 400); 
        }
        return response()->json(['status' => 'success', 'message' => 'Tambah Data Berhasil.', 'redirectTo'=> route('type.index')], 200); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show($type_id)
    {
        $type = $this->_model::find($type_id);
        return view('Type::show', ['type'=> $type]);
    }
 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit($type_id)
    {
        $type = $this->_model::find($type_id);
        return view('Type::edit', ['type'=> $type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update( $type_id)
    {  
        try {
            $type = $this->_model::find($type_id);
            if($type){                
                $type->update($this->_serialize(request()));
            }
        } catch (\Exception $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Update Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Update Data Berhasil.', 'redirectTo'=> route('type.index')], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy($type_id)
    {
        try {
            $type = $this->_model::find($type_id);
            if($type){
                $type->delete(); 
            }
        } catch (\Throwable $e) {
            return response()->json(['status'=> 'error', 'message'=> 'Data Error '.$e->getMessage()], 400);
        }
        return response()->json(['status'=> 'success', 'message'=> 'Hapus Data Berhasil.', 'redirectTo'=> route('type.index')], 200);
    }
}
