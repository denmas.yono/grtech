{{ Form::open(['method' => 'POST', 'route' => ['type.store'], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Tipe</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Tipe">
    </div>
</div>
{{ Form::close() }}
