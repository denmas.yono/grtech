{{ Form::model($type, ['method' => 'POST', 'route' => ['type.update', $type->id], 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) }}
<div class="card-body">
    <div class="form-group">
        <label for="name">Tipe</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $type->name }}" placeholder="Tipe">
    </div>
</div>

{{ Form::close() }}
