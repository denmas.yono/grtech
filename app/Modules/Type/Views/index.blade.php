@extends('adminlte::page') 
@section('content')
    <div id="container">
        @include('Type::list')
    </div>
@section('adminlte_js')
    <script>
        $(function() {
            loadTypes();
        });

        function loadTypes() {
            var selected = [];
            $("#type").DataTable({
                dom:"<'row'<'col-sm-3'l><'col-sm-4'B><'col-sm-5'f>>rtip",
                responsive: true,
                scrollY: 400,
                responsive: true, 
                processing: true,
                serverSide: true,  
                select: true,
                "rowCallback": function( row, data ) {
                    if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                        $(row).addClass('selected');
                    }
                },
                "ajax": "{{ route('type.getListAjax') }}",
                "columns": [
                    // data:column name from server ,name:alias
                    {
                        data: 'DT_RowIndex',
                        name: 'id'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },

                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        }
    </script>
@stop
@stop
