<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'type', 'namespace' => 'App\Modules\Type\Controllers', 'middleware' => ['web','admin']], function () {
    Route::get('/', ['as' => 'type.index', 'uses' => 'TypeController@index']);
    Route::get('/getListAjax', ['as' => 'type.getListAjax', 'uses' => 'TypeController@getListAjax']);
    Route::get('/create', ['as' => 'type.create', 'uses' => 'TypeController@create']);
    Route::get('/show/{id}', ['as' => 'type.show', 'uses' => 'TypeController@show']);    
    Route::get('/preview/{id}', ['as' => 'type.preview', 'uses' => 'TypeController@preview']);    
    Route::post('/store', ['as' => 'type.store', 'uses' => 'TypeController@store']);
    Route::get('/edit/{id}', ['as' => 'type.edit', 'uses' => 'TypeController@edit']);
    Route::put('/update/{id}', ['as' => 'type.update', 'uses' => 'TypeController@update']);
    Route::delete('/delete/{id}', ['as' => 'type.destroy', 'uses' => 'TypeController@destroy']);
});