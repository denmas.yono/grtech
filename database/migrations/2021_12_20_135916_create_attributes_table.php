<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('attributes')){
            Schema::create('attributes', function (Blueprint $table) {
                $table->id();
                $table->foreignId('menu_id')->references('id')->on('menus');
                $table->string('key');
                $table->string('name');
                $table->integer('is_active');
                $table->timestamps();
                $table->softDeletes($column = 'deleted_at', $precision = 0);
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
