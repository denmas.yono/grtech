<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('charities')){

            Schema::create('charities', function (Blueprint $table) {
                $table->id();
                $table->string('name'); 
                $table->string('cellphone')->nullable(); 
                $table->string('email')->nullable(); 
                $table->string('address')->nullable(); 
                $table->string('age')->nullable(); 
                $table->enum('sex',['P','W'])->nullable();  
                $table->timestamps();
                $table->softDeletes($column = 'deleted_at', $precision = 0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charities');
    }
}
