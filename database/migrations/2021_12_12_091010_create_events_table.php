<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('events')){

            Schema::create('events', function (Blueprint $table) {
                $table->id();
                $table->string('name'); 
                $table->string('description'); 
                $table->dateTime('date_start'); 
                $table->dateTime('date_end'); 
                $table->foreignId('event_type_id')->references('id')->on('event_types');
                $table->timestamps();
                $table->softDeletes($column = 'deleted_at', $precision = 0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
