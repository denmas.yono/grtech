<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenefactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('benefactors')){

            Schema::create('benefactors', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->foreignId('address_id')->references('id')->on('address');
                $table->string('cellphone')->nullable();;
                $table->enum('sex',['P', 'L']);
                $table->timestamps();
                $table->softDeletes($column = 'deleted_at', $precision = 0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefactors');
    }
}
