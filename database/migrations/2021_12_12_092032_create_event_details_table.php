<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('event_details')){

            Schema::create('event_details', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->dateTime('date_start');
                $table->dateTime('date_end');
                $table->foreignId('rt_id')->references('id')->on('rt');
                $table->foreignId('rw_id')->references('id')->on('rw');
                $table->string('cellphone')->nullable();;
                $table->enum('sex',['P', 'L']);
                $table->timestamps();
                $table->softDeletes($column = 'deleted_at', $precision = 0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_details');
    }
}
