<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncomeCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('income_categories')){

            Schema::create('income_categories', function (Blueprint $table) {
                $table->id();
                $table->string('name')->unique();
                $table->text('description',0)->nullable();
                $table->foreignId('type_id')->references('id')->on('types');
                $table->softDeletes($column = 'deleted_at', $precision = 0); 
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('income_categorie');
    }
}
