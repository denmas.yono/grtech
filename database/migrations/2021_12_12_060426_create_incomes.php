<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncomes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('incomes')){

            Schema::create('incomes', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->foreignId('type_id');
                $table->foreignId('category_id');
                $table->text('description');
                $table->foreignId('post_status_id');
                $table->integer('year');
                $table->integer('month');
                $table->date('publish_date');
                $table->timestamps();
                $table->softDeletes($column = 'deleted_at', $precision = 0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incomes');
    }
}
