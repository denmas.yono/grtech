<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMosquesTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('mosques')){

            Schema::create('mosques', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->string('email');
                $table->string('cellphone');
                $table->string('phone');
                $table->string('address_detail');
                $table->string('website');
                $table->string('photo');
                $table->string('logo');
                $table->foreignId('rt_id')->references('id')->on('rt');
                $table->foreignId('rw_id')->references('id')->on('rw');
                $table->foreignId('subdistrict_id')->references('id')->on('subdistricts');
                $table->foreignId('district_id')->references('id')->on('districts');
                $table->foreignId('city_id')->references('id')->on('cities');
                $table->foreignId('region_id')->references('id')->on('regions');
                $table->foreignId('country_id')->references('id')->on('countries');
                $table->softDeletes($column = 'deleted_at', $precision = 0); 
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mosques');
    }
}
