<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('comments')) {
            Schema::create('comments', function (Blueprint $table) {
                $table->id();
                $table->timestamps();
                $table->string('comment');
                $table->unsignedBigInteger('student_id');
                $table->unsignedBigInteger('teacher_id');
                $table->unsignedBigInteger('subject_id');
                $table->unsignedBigInteger('structure_id');
                $table->foreign('student_id')->references('id')->on('students');
                $table->foreign('teacher_id')->references('id')->on('teachers');
                $table->foreign('subject_id')->references('id')->on('subjects');
                $table->foreign('structure_id')->references('id')->on('structures');
                $table->softDeletes($column = 'deleted_at', $precision = 0);
            });
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
