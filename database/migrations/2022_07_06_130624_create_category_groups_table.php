<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('category_groups')){
            Schema::create('category_groups', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->string('description');
                $table->softDeletes($column = 'deleted_at', $precision = 0);
                $table->timestamps();
            });

        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_category_groups');
    }
}
