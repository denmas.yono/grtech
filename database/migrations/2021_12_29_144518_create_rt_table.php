<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('rt')){
            
            Schema::create('rt', function (Blueprint $table) {
                $table->id();
                $table->string('name', 255); 
                $table->foreignId('rw_id')->references('id')->on('rw');
                $table->softDeletes($column = 'deleted_at', $precision = 0); 
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rt');
    }
}
