<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('people')){
            Schema::create('people', function (Blueprint $table) {
                $table->id();
                $table->string('name');
                $table->string('first_name');
                $table->string('last_name');
                $table->foreignId('company_id')->index();
                $table->string('email')->unique();
                $table->string('phone')->nullable();
                $table->string('cellphone')->nullable();
                $table->foreignId('user_id')->nullable();
                $table->timestamps();
                $table->softDeletes($column = 'deleted_at', $precision = 0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
